% Created 2024-08-10 sab 11:58
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\author{Nicolò Plebani}
\date{\today}
\title{Physics Notebook}
\begin{document}

\maketitle
\tableofcontents

\newpage
\section{Glossario}
\label{sec:orgb673394}

\begin{itemize}
\item MRU, moto rettilineo uniforme. È quel moto senza accelerazione.
\item Impulso, cambio istantaneo della velocià (velocity).
\item Velocità relativa \(v_{rel} = v_{a} - v_{b}\), differenza di velocità tra due oggetti.

\newpage
\end{itemize}
\section{Simulazione del movimento}
\label{sec:org3a678d0}
\subsection{Accelerazione costante}
\label{sec:org958d49e}

Se un corpo si muove con accelerazione costante, ciò significa che la sua velocità cambia costantemente nel tempo.
Ergo, quando l’accelerazione di un corpo è costante, significa che la sua velocità aumenta o diminuisce linearmente rispetto al tempo.

Ad esempio, se un corpo si muove in avanti con un’accelerazione costante di \(\frac{2m}{s^2}\) , ciò significa che la sua velocità aumenterà di \(\frac{2m}{s}\) ogni secondo. Quindi se parte da fermo, dopo un secondo la sua velocità sarà \(\frac{2m}{s}\), dopo due secondi \(\frac{4m}{s}\), dopo tre secondi \(\frac{6m}{s}\), ecc.

Va notato che l’accelerazione non solo cambia l’entità della velocità, ma può anche variare la direzione della velocità. Movimenti come il movimento circolare uniforme (MCU) mantengono il modulo di velocità costante durante tutto il movimento ma la sua direzione varia, effettuando così virate a velocità costante.

Quando l’accelerazione del movimento è costante, la posizione di un corpo è uguale alla posizione iniziale più il prodotto della velocità iniziale per il tempo trascorso più metà dell’accelerazione per il quadrato del tempo trascorso.

Pertanto, la formula per calcolare la posizione del corpo che descrive un movimento ad accelerazione costante è la seguente:

\begin{equation}
p = p_{0} + v_{0}(t-t_{0}) + \frac{a(t-t_{0})^{2}}{2}
\end{equation}

\begin{itemize}
\item \(p\) è la posizione del corpo.
\item \(p_0{0}\) è la posizione iniziale del corpo.
\item \(v_{0}\) è la velocità iniziale del corpo.
\item \(t\) è l’istante di tempo durante il quale viene calcolata la posizione del corpo.
\item \(t_{0}\) è il momento iniziale.
\item \(p\) è la posizione del corpo.
\item \(a\) è l’accelerazione del corpo.
\end{itemize}

Se l’accelerazione è costante, si calcola dividendo la variazione della velocità (\(\Delta v\)) per la variazione del tempo (\(\Delta t\)). Quindi la formula per l’accelerazione costante è:

\begin{equation}
a = \frac{\Delta v}{\Delta t} = \frac{v_{f} - v_{i}}{t_{f} - t_{i}}
\end{equation}

\begin{itemize}
\item \(a\) è l’accelerazione.
\item \(\Delta v\) è l’incremento di velocità.
\item \(\Delta t\) è un incremento di tempo.
\item \(v_{f}\) è la velocità finale.
\item \(v_{i}\) è la velocità iniziale.
\item \(t_{f}\) è il momento finale.
\item \(t_{i}\) è il momento iniziale.
\end{itemize}
\subsection{Velocità costante}
\label{sec:orgc76ca51}

Se un corpo si muove a velocità costante significa che si muove in linea retta e percorre la stessa distanza in due intervalli di tempo uguali.
Pertanto, un corpo che ha un movimento a velocità costante descrive un movimento rettilineo uniforme (MRU).

Inoltre, se la velocità di un corpo è costante, ciò significa che la sua accelerazione è nulla.
L’accelerazione di un corpo in movimento indica la variazione della sua velocità, quindi se la velocità di un corpo non varia significa che la sua accelerazione sarà pari a 0.

La posizione del corpo la cui velocità è costante è equivalente alla posizione iniziale più il prodotto della velocità e dell’incremento temporale. Pertanto la formula per calcolare la posizione di un corpo a velocità costante è:

\begin{equation}
p = p_{0} + v (t - t_{0})
\end{equation}

\begin{itemize}
\item \(p\) è la posizione del corpo.
\item \(p_{0}\) è la posizione iniziale del corpo.
\item \(v\) è la velocità del corpo.
\item \(t\) è l’istante di tempo durante il quale viene calcolata la posizione del corpo.
\item \(t_{0}\) è il momento iniziale.
\end{itemize}

Quando la velocità di un corpo è costante, si calcola dividendo lo spostamento (\(\Delta p\)) per l’intervallo di tempo trascorso (\(\Delta t\)). Quindi, la velocità è uguale alla differenza tra la posizione finale e quella iniziale divisa per la differenza tra l’istante finale e quello iniziale è:

\begin{equation}
v = \frac{\Delta p}{\Delta t} = \frac{p_{f} - p_{i}}{t_{f} - t_{i}}
\end{equation}

\begin{itemize}
\item \(v\) è la velocità.
\item \(\Delta p\) è l’offset.
\item \(\Delta t\) è la variazione temporale.
\item \(p_{f}\) è la posizione finale.
\item \(p_{i}\) è la posizione di partenza.
\item \(t_{f}\) è il momento finale.
\item \(t_{i}\) è il momento iniziale.
\end{itemize}

Nel moto a velocità costante l’accelerazione è zero. Quindi, se un corpo si muove a velocità costante per tutto il percorso, la sua accelerazione sarà sempre zero per tutto il percorso.
\section{Principi della dinamica}
\label{sec:org77b58b1}

\subsection{Il principio di inerzia o primo principio della dinamica}
\label{sec:org6a974de}
Se su un corpo non agiscono forze o agisce un sistema di forze in equilibrio, il corpo persevera nel suo stato di quiete o di moto rettilineo uniforme.
Per “sistema di forze in equilibrio” si intende un insieme di forze, grandezze vettoriali, la cui somma vettoriale sia nulla.
\subsection{La legge fondamentale della dinamica o secondo principio della dinamica}
\label{sec:org7c50a93}
Se su un corpo agisce una forza o un sistema di forze, la forza risultante applicata al corpo possiede direzione e verso della sua accelerazione e, in modulo,
è direttamente proporzionale al modulo la sua accelerazione. La costante di proporzionalità tra queste due grandezze è la massa (detta appunto inerziale),
grandezza specifica di ciascun corpo. Questa legge può essere enunciata mediante l’equazione:

\begin{equation}
\overrightarrow{F} = m \overrightarrow{a}
\end{equation}

\begin{itemize}
\item \(\overrightarrow{F}\) è la risutlante delle forze agenti sul corpo.
\item \(m\) la massa dello stesso.
\item \(\overrightarrow{a}\) l’accelerazione cui è soggetto (il prodotto è quello tra uno scalare e un vettore).
\end{itemize}
\subsection{Il principio di azione-reazione o terzo principio della dinamica}
\label{sec:org2262758}

Se due corpi interagiscono tra loro, si sviluppano due forze, dette comunemente azione e reazione: come grandezze vettoriali sono uguali in modulo e direzione, ma opposte in verso.
\section{Forze}
\label{sec:orgf0923cf}

Una forza è:
\begin{itemize}
\item Ciò che causa lo stato di moto su un corpo libero di muoversi in stato di quiete.
\item Ciò che causa il cambiamento di traiettoria o di velocità del moto, o lo stato di quiete.
\item Ciò che causa una deformazione su un corpo non libero di muoversi.
\end{itemize}

La cinematica comprende solo accelerazione e velocità. La dinamica oltre all'accelerazione e velocità comprende anche l'effetto della massa e delle forze.
\subsection{Peso}
\label{sec:org543a0bd}

\subsubsection{Teoria}
\label{sec:org2646c68}

La forza peso dipende dalla costante gravitazionale:

\begin{equation}
g \simeq  9,81 \frac{m}{s^{2}}
\end{equation}

Ergo la forza peso sarà:

\begin{equation}
W = m g
\end{equation}
\subsubsection{Pratica}
\label{sec:orgfed7065}

Per tradurre quanto affermato nel paragrafo precedente, si passa al formato vettoriale.

\begin{equation}
\overrightarrow{W} = \overrightarrow{\nu} g
\end{equation}

dove \(\overrightarrow{\nu}\) è il versore che punta verso il basso.
Supponendo che il piano cartesiano della finestra abbia ordinata che cresca verso il basso, dunque \(\overrightarrow{\nu} = (0, 1)\)

\begin{verbatim}
float g{body.mass() * 9.8f};
gmath::vec2f weight{gmath::xf{0.0f}, gmath:yf{1.0f}};
weight *= g * PIXELS_PER_METER;
\end{verbatim}
\subsection{Resitenza fluidodinamica}
\label{sec:org0a7bca8}

\subsubsection{Teoria}
\label{sec:org769f602}

Innanzitutto, il coefficiente della resistenza aerodinamica comprende la resistenza di attrito e quella di forma. Per un profilo aerodinamico, invece, include anche gli effetti della resistenza indotta.

Tale coefficiente è definito come segue:

\begin{equation}
C_{d} = \frac{D}{q_{0} \cdot A}
\end{equation}

\begin{itemize}
\item \(C_{d}\) è il coefficiente di resistenza fluidodinamica.
\item \(D\) è la resistenza aerodinamica.
\item \(q_{0}\) è la pressione del fluido;
\item \(A\) è l’area di riferimento.
\end{itemize}

La resistenza aerodinamica, invece, può essere calcolata utilizzando la formula seguente:

\begin{equation}
F_{d} = \frac{1}{2} \rho v^{2} C_{d} A
\end{equation}

\begin{itemize}
\item \(F_{d}\)
\item \(\rho\) è la densità del fluido.
\item \(v^{2}\) è la velocità dell'oggetto che attraversa il fluido.
\item \(A\) è l'area dell'oggetto in contatto con il fluido.
\item \(C_{d}\) è il coefficiente di resistenza fluidodinamica.
\end{itemize}

Imperativo ricordare che essendo una resistenza deve essere applicata in maniera opposta alla direzione in cui l'oggetto si sta muovendo.
\subsubsection{Pratica}
\label{sec:orgfe533b0}

Possiamo riassumere \(\frac{1}{2} \rho C_{d} A\) in un unica costante \(k\) per semplificare il calcolo.

È necessario, anche qui, passare alla forma vettoriale. La formula è la seguente.

\begin{equation}
\overrightarrow{F_{d}} = k \space \left\| v \right\|^{2} \space (-\hat{\upsilon})
\end{equation}

\begin{itemize}
\item \(k\) costante.
\item \(\left\| v \right\|^{2}\) il modulo del vettore elevato al quadrato.
\item \((-\hat{\upsilon})\) versore opposto.
\end{itemize}

La funzione richiesta per effettuare il calcolo della resistenza fluidodinamica è la sottostante.

\begin{verbatim}
constexpr auto
generate_drag_force(const gmath::vec2f &velocity,
                    const float dragCoefficient) noexcept -> gmath::vec2f {

  auto magnitudeSquared{velocity.magnitude_squared()};

  // Guard!
  if (magnitudeSquared <= 0.0f)
    return {};

  // calculates the drag direction (inverse of velocity unit vector)
  auto dragDirection{velocity.normalized() * -1.0f};

  // calculate the drag magnitude (k * |v|^2)
  auto dragMagnitude{dragCoefficient * magnitudeSquared};

  // generate the final drag force with direction and magnitude
  return dragDirection * dragMagnitude;
}
\end{verbatim}
\subsection{Forza di attrazione gravitazionale}
\label{sec:orge70d08f}

\subsubsection{Teoria}
\label{sec:orga0651e0}

In fisica, la legge di gravitazione universale afferma che nell'Universo due corpi si attraggono con una forza direttamente proporzionale \(G\)
al prodotto delle loro masse e inversamente proporzionale al quadrato della loro distanza \(r\).

Ogni punto materiale attrae ogni altro singolo punto materiale con una forza che punta lungo la linea di intersezione di entrambi i punti.
La forza è proporzionale al prodotto delle due masse e inversamente proporzionale al quadrato della distanza fra loro:

\begin{equation}
F_{g} = G \frac{m_{1}m_{2}}{r^2}
\end{equation}

\begin{itemize}
\item \(F_{g}\) è l'intensità della forza tra le masse.
\item \(G\) è la costante di gravitazione universale.
\item \(m_{1}\) è la prima massa.
\item \(m_{2}\) è la seconda massa.
\item \(r\) è la distanza tra i centri delle masse.
\end{itemize}
\subsubsection{Pratica}
\label{sec:org3612a07}

Per passare alla stesura del codice è doveroso passare al formato vettoriale.

\begin{equation}
\overrightarrow{F_{g_{12}}} = -G \frac{m_{1}m_{2}}{\left\| r_{12} \right\|^2} \space \hat{r_{12}}
\end{equation}

\begin{itemize}
\item \(\overrightarrow{F_{g_{12}}}\) è la forza applicata sull'oggetto 2 dovuta all'oggetto 1.
\item \(G\) è la costante di gravitazione universale.
\item \(m1\) e \(m2\) sono rispettivamente le masse degli oggetti 1 e 2.
\item \(\left\| r_{1} - r_{2} \right\|\) è la distanza tra gli oggetti 1 e 2.
\item \(\hat{r_{12}}\) è il versore dall'oggetto 1 al 2.
\end{itemize}

Si può inoltre vedere che \(\overrightarrow{F_{g_{12}}} = -\overrightarrow{F_{g_{21}}}\) (entrambi i corpi si attraggono).

Avremo dunque.

\begin{verbatim}
constexpr auto
generate_gravitational_force(const rigid_body &a, const rigid_body &b,
                             const float gravitationalCoefficient =
                                 0.00000000006674f) noexcept -> gmath::vec2f {
  auto d{a.position() - b.position()};
  auto distanceSquared{d.magnitude_squared()};
  auto attractionDirection{d.normalized()};
  auto attractionMagnitude{gravitationalCoefficient * (a.mass() * b.mass()) /
                           distanceSquared};
  return attractionDirection * attractionMagnitude;
}
\end{verbatim}
\subsection{Forza elastica}
\label{sec:org4da0231}

\subsubsection{Teoria}
\label{sec:org9b56256}

La legge di Hooke è la più semplice relazione costitutiva di comportamento dei materiali elastici.
Essa è formulata dicendo che un corpo elastico subisce una deformazione direttamente proporzionale allo sforzo a esso applicato. La costante di proporzionalità dipende dalla natura del materiale stesso.

\begin{equation}
F_{s} = -k_{E} \space \Delta_{l} \space \hat{x}
\end{equation}

\begin{itemize}
\item \(F_{s}\) forza con cui la molla reagisce alla sollecitazione.
\item \(k_{E}\) costante elastica longitudinale della molla.
\item \(\Delta_{l}\) allungamento della molla.
\item \(\hat{x}\) l'asse dell'allungamento.
\end{itemize}
\subsubsection{Pratica}
\label{sec:org13ad8fa}

Passiamo al formato vettoriale.

\begin{equation}
\overrightarrow{F_{s}} = -k_{E} \space \Delta_{l} \space \hat{d}
\end{equation}

\begin{itemize}
\item \(\hat{d}\) il versore della forza elastica.
\end{itemize}

Il codice di seguito.

\begin{verbatim}
constexpr auto
generate_spring_force(const rigid_body &body, const vec2f &anchor,
                      const float restLength,
                      const float springCoefficient) noexcept -> vec2f {

  // Calculates the distance between the anchor and the object
  const auto d{body.position() - anchor};

  // Find the spring displacement considering the rest lenght
  auto displacement{d.magnitude() - restLength};

  // Calculate the direction and the magnitude of the spring force
  auto springDirection{d.normalized()};
  auto springMagnitude{-springCoefficient * displacement};

  return springDirection * springMagnitude;
}
\end{verbatim}
\section{Rigid-Body}
\label{sec:orgda91f28}

\section{Collisioni}
\label{sec:orgdd0aa61}
\subsection{Quantità di moto}
\label{sec:org608563e}

In meccanica classica, la quantità di moto di un oggetto è una grandezza vettoriale definita come il prodotto della massa dell'oggetto per la sua velocità.
Talvolta il vettore quantità di moto viene denominato momento lineare, per distinguerlo dal momento angolare. Tuttavia, a rigore questa quantità non rappresenta il momento di alcun vettore. Generalmente viene indicato con la lettera p o con la lettera q.

La formula è la seguente.

\begin{equation}
\overrightarrow{P} = m \overrightarrow{v}
\end{equation}

Deduciamo dunque che la la quantità di moto è:
\begin{itemize}
\item Una misura della massa in movimento.
\item La tendenza di un corpo di rimanere in movimento
\end{itemize}

Secondo la legge della conservazione della quantità di moto, la quantità di moto totale di un sistema deve sempre essere costante.
Quando due oggetti collidiono, impartiscono una parte della propria quantità di moto su tutti gli altri oggetti (in collisione), riducendo la prorpia.

Per esempio supponendo di avere due corpi \(a\) e \(b\), per quanto affermato sopra:

\begin{equation}
m_{a} v_{a} + m_{b} v_{b} = m_{a}v'_{a} + m_{b} v'_{b}
\end{equation}

Ovviamente non considero il cambio di massa dei corpi che potrebbe avvenire durante la collisione.

Si immagini il biliardo, la quantità di moto di una palla viene trasmessa alle altre tramite collisione, cedendo parte della propria e mantenendo tutto il sistema costante.
\subsection{Impulsi}
\label{sec:orgb755aad}

Il teorema dell'impulso afferma che, per il secondo principio della dinamica, in un sistema dinamico l'impulso corrisponde alla variazione della quantità di moto in un intervallo temporale.
L'impulso permette di registrare istantaneamente un cambio della velocità di un oggetto, istantaneo implica che la durata del frame non influisce sul valore finale.
Con impulso, nella meccanica classica, si indica una grandezza vettoriale, misurata in newton per secondi, definita come l'integrale di una forza rispetto al tempo:

\begin{equation}
\Delta \mathbf {p} :=\int _{t_{0}}^{t_{1}}\mathbf {F} \,\mathrm {d} t
\end{equation}

Se la forza è costante:

\begin{equation}
\Delta \mathbf {p} =\mathbf {F} \Delta t
\end{equation}

L'introduzione del concetto di impulso permette di enunciare il teorema dell'impulso, utilizzato in particolare nel campo degli urti, della diffusione e per lo studio delle forze impulsive.
Grazie alla legge di conservazione della quantità di moto si può dedurre che in un sistema isolato l'impulso totale è nullo.

Cerchiamo di riportare ora tutta questa teoria lato codice.
Effettuando alcune trasformazioni trovo.

\begin{equation}
\Delta \mathbf {p} =\mathbf {F} \Delta t = mv' - mv
\end{equation}

\begin{equation}
\mathbf {F} \Delta t = ma\Delta t = m \frac{\Delta v}{\Delta t} \Delta t
\end{equation}

\begin{equation}
\mathbf {F} \Delta t = m\Delta v = J
\end{equation}

\begin{equation}
\Delta v = \frac{J}{m}
\end{equation}

Chiarendo meglio, l'impulso è un cambio istantaneo della velocità, che è inversamente proporzionale alla massa dell'oggetto.
Sapendo che la quantità di moto \(P = mv\) e che l'impulso \(J\) è il cambiamento della quantità di moto \(J = \Delta P = m \Delta v\) trovo che il cambiamento
della velocità è pari a \(\Delta v = \frac{J}{m}\).

\begin{verbatim}
///< CODICE METODO IMPULSO
\end{verbatim}

\medskip

Come calcolare \(J\) generato calla collisione di due corpi?

\bigskip

Siccome la l'impulso viene generato dalla velocità con la quale i due oggetti collidono, dobbiamo utilizzare la velocità relativa \(v_{rel}\) tra i due oggetti.

Secondo la collisione perfettamente elastica abbiamo che la nuova velocià relativa \(v'_{rel}\) sarà:

\begin{equation}
v'_{rel} \cdot \hat{n} = -\epsilon(v_{rel} \cdot \hat{n})
\end{equation}

\begin{itemize}
\item \(v'_{rel}\) la velocità relativa dopo la collisione.
\item \(\hat{n}\) il versore della collisione.
\item \(\epsilon\) il coefficiente di restituzione (l'elasticità dell'urto valore [0, 1]).
\item \(v'_{rel}\) la velocità relativa prima della collisione.
\end{itemize}

Ora per quanto affermato in precedenza possiamo scrivere che:

\begin{equation}
\Delta P_{a} = m_{a}v'_{a} - m_{a}v_{a} = J\hat{n}
\end{equation}

\begin{equation}
v'_{a} = v_{a} + \frac{J\hat{n}}{m_{a}}
\end{equation}

E che:

\begin{equation}
\Delta P_{b} = m_{b}v'_{b} - m_{b}v_{b} = -J\hat{n}
\end{equation}

\begin{equation}
v'_{b} = v_{b} - \frac{J\hat{n}}{m_{b}}
\end{equation}

Affermiamo dunque:

\begin{equation}
v'_{rel} \cdot \hat{n} = (v'_{a} - v'_{b}) \cdot \hat{n}
\end{equation}

Dunque:

\begin{equation}
v'_{rel}  = (v'_{a} - v'_{b}) = (v_{a} + \frac{J\hat{n}}{m_{a}}) - (v_{b} + \frac{J\hat{n}}{m_{b}}) = (v_{a} - v_{b}) + (\frac{J\hat{n}}{m_{a}} + \frac{J\hat{n}}{m_{b}})
\end{equation}

Ora dobbiamo isolare \(J\):

\begin{equation}
v'_{rel} \cdot \hat{n} = (v_{a} - v_{b}) \cdot \hat{n} + (\frac{J\hat{n}}{m_{a}} + \frac{J\hat{n}}{m_{b}}) \cdot \hat{n}
\end{equation}


\begin{equation}
v'_{rel} \cdot \hat{n} = v_{rel} \cdot \hat{n} + (\frac{J\hat{n}}{m_{a}} + \frac{J\hat{n}}{m_{b}}) \cdot \hat{n}
\end{equation}

\begin{equation}
-\epsilon(v'_{rel} \cdot \hat{n}) = (v_{rel} \cdot \hat{n}) + J\hat{n}(\frac{1}{m_{a}} + \frac{1}{m_{b}}) \cdot \hat{n}
\end{equation}


\begin{equation}
J = \frac{-\epsilon(v_{rel} \cdot \hat{n}) - (v_{rel} \cdot \hat{n})}{(\frac{1}{m_{a}} + \frac{1}{m_{b}}) (\hat{n} \cdot\hat{n})}
\end{equation}

\begin{equation}
J = \frac{(1+\epsilon)(v_{rel} \cdot \hat{n})}{(\frac{1}{m_{a}} + \frac{1}{m_{b}}) (\hat{n} \cdot\hat{n})}
\end{equation}

Semplificando ancora e ricordandoci che \(\hat{n}\) è un versore abbiamo:

\begin{equation}
J = \frac{(1+\epsilon)(v_{rel} \cdot \hat{n})}{\frac{1}{m_{a}} + \frac{1}{m_{b}}}
\end{equation}
\end{document}
