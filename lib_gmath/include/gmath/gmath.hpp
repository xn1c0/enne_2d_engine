#ifndef LIB_PHYSX_INCLUDE_PHYSX_PHYSX_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_PHYSX_HPP_

/**
 * @author Nicolò Plebani
 * @file gmath.hpp
 */

#include <gmath/vec2.hpp>

#endif // LIB_PHYSX_INCLUDE_PHYSX_PHYSX_HPP_
