#ifndef LIB_GMATH_INCLUDE_GMATH_PRIMITIVES_HPP_
#define LIB_GMATH_INCLUDE_GMATH_PRIMITIVES_HPP_

#include <st/st.hpp>

/**
 * @author Nicolò Plebani
 * @file primitives.hpp
 */

namespace gmath {

using xi = st::type<int, struct xf_tag, st::traits::arithmetic::complete>;

using yi = st::type<int, struct yf_tag, st::traits::arithmetic::complete>;

using xf = st::type<float, struct xf_tag, st::traits::arithmetic::complete>;

using yf = st::type<float, struct yf_tag, st::traits::arithmetic::complete>;

} // namespace gmath

#endif // LIB_GMATH_INCLUDE_GMATH_PRIMITIVES_HPP_
