#ifndef LIB_GMATH_INCLUDE_GMATH_TRAITS_HPP_
#define LIB_GMATH_INCLUDE_GMATH_TRAITS_HPP_
/**
 * @author Nicolò Plebani
 * @file traits.hpp
 */

#include <type_traits>

namespace gmath {

/**
 * @brief Checks if type T value is a number.
 * @tparam T The type of the value.
 * @note True if type T value is a number.
 */
template <typename T>
inline constexpr bool is_number =
    not std::is_same_v<T, bool> and
    (std::is_integral_v<T> or std::is_floating_point_v<T>);

/**
 * @brief Specify the requirment on template arguments.
 * @tparam T The type that must be numeric.
 */
template <typename T>
concept is_arithmetic = is_number<T>;

} // namespace gmath

#endif // LIB_GMATH_INCLUDE_GMATH_TRAITS_HPP_
