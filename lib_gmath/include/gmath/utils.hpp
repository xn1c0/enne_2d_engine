#ifndef LIB_GMATH_INCLUDE_GMATH_UTILS_HPP_
#define LIB_GMATH_INCLUDE_GMATH_UTILS_HPP_

#include "gmath/traits.hpp"
#include <cmath>

namespace gmath {

/**
 * @brief Checks if two value are approximately equals.
 * @tparam T The type of the arithmetic value.
 * @param a The first vec2 to compare.
 * @param b The second vec2 to compare.
 * @param epsilon The machine epsilon
 * @return True if the two arithmetic values are approximately equals.
 * @see https://en.cppreference.com/w/cpp/types/numeric_limits/epsilon
 */
template <typename T>
  requires is_arithmetic<T>
constexpr auto is_equal_approx(
    T a, T b, T epsilon = std::numeric_limits<T>::epsilon()) noexcept -> bool {
  // Check if the numbers are really close
  T diff{std::abs(a - b)};
  if (diff <= epsilon)
    return true;
  // Otherwise fall back to Knuth's algorithm
  return (diff <= (std::max(std::abs(a), std::abs(b)) * epsilon));
}

} // namespace gmath

#endif // LIB_GMATH_INCLUDE_GMATH_UTILS_HPP_
