#ifndef LIB_GMATH_INCLUDE_GMATH_VEC2_HPP_
#define LIB_GMATH_INCLUDE_GMATH_VEC2_HPP_

#include <cmath>
#include <format>

#include "gmath/primitives.hpp"
#include "gmath/traits.hpp"
#include "gmath/utils.hpp"

/**
 * @author Nicolò Plebani
 * @file vec2.hpp
 */

namespace gmath {

template <typename T>
  requires is_arithmetic<T>
class vec2;

/**
 * @brief Checks if two vec2 are approximately equals.
 * @tparam T The type of the vec2.
 * @param a The first vec2 to compare.
 * @param b The second vec2 to compare.
 * @return True if the two vec2 are approximately equals else false.
 */
template <typename T>
constexpr auto is_equal_approx(vec2<T> a, vec2<T> b) -> bool {
  return is_equal_approx(a.x(), b.x()) && is_equal_approx(a.y(), b.y());
}

using vec2i = vec2<int>;
using vec2f = vec2<float>;

template <typename T>
  requires is_arithmetic<T>
class vec2 {
public:
  inline constexpr static bool integral = std::is_integral_v<T>;
  using value_t = std::conditional_t<integral, int, float>;
  using x_t = std::conditional_t<integral, xi, xf>;
  using y_t = std::conditional_t<integral, yi, yf>;

private:
  value_t mX;
  value_t mY;

public:
  [[nodiscard]] constexpr auto x() const & -> const value_t & { return mX; }
  [[nodiscard]] constexpr auto x() & -> value_t & { return mX; }
  [[nodiscard]] constexpr auto x() && -> value_t && { return std::move(mX); }

  [[nodiscard]] constexpr auto y() const & -> const value_t & { return mY; }
  [[nodiscard]] constexpr auto y() & -> value_t & { return mY; }
  [[nodiscard]] constexpr auto y() && -> value_t && { return std::move(mY); }

  /**
   * @brief Default constrcutor.
   * @note trivial and non-throwing.
   */
  constexpr vec2() : mX{}, mY{} {}

  /**
   * @brief Secondary constrcutor.
   * @param x The cartesian coordinate x.
   * @param y The cartesian coordinate y.
   */
  constexpr vec2(const x_t &x, const y_t &y) noexcept
      : mX{x.value()}, mY{y.value()} {}

  /**
   * @brief Secondary constructor.
   * Constructs the vec2 from another type of vec2. A call to this
   * constructor will fail to compile if U is not convertible to T.
   * @param other The vec2 to convert.
   */
  template <typename U>
  constexpr explicit vec2(const vec2<U> &other) noexcept
      : mX{static_cast<value_t>(other.x())},
        mY{static_cast<value_t>(other.y())} {}

  /**
   * @brief Operator "three way".
   */
  [[nodiscard]] constexpr auto
  operator<=>(const vec2 &rhs) const noexcept = default;

  /**
   * @brief Computes the length of the vector (floating-point).
   * @return The length of the vector.
   * @note std::sqrt is not constexpr until C++26 !!!
   */
  [[nodiscard]] constexpr auto magnitude(void) const noexcept -> float {
    return std::sqrt(magnitude_squared());
  }

  /**
   * @brief Computes the length squared of the vector (floating-point).
   * Suitable for comparisons, more efficient than length().
   * @return The length squared of the vector.
   */
  [[nodiscard]] constexpr auto
  magnitude_squared(void) const noexcept -> value_t {
    return dot_product(*this);
  }

  /**
   * @brief Computes the normalized vector.
   * A vector normalized vector is the vector with same direction but with
   * length equals to 1.
   * @return The normalized vector if magnitude is not zero else return *this
   */
  [[nodiscard]] constexpr auto normalized(void) const noexcept -> vec2 {
    vec2 tmp = *this;
    value_t m = magnitude();
    if (m != value_t{0})
      return tmp / magnitude();
    else
      return tmp;
  }

  /**
   * @brief Computes the perpendicular vector (normal).
   * This vector rotated by +90 degrees, (x,y) becomes (-y,x).
   * Example vec(18, 7) is transformed to (7, -18).
   * @return The perpendicular vector.
   */
  [[nodiscard]] constexpr auto perpendicular() const noexcept -> vec2 {
    return vec2{x_t{y()}, y_t{-x()}};
  }

  /**
   * @brief Computes the dot product of this vector and the other vector.
   * @param other The other vector.
   * @return The dot product.
   */
  [[nodiscard]] constexpr auto
  dot_product(const vec2 &other) const noexcept -> float {
    return x() * other.x() + y() * other.y();
  }

  /**
   * @brief Z component of the cross product of two 2D vectors.
   * In R^2 vector spaces there is no cross product operation.
   * This method operates as an hack, in a way that it only returns the z
   * component of the cross product vector because the x and y components are
   * always zero.
   * @param other The other vector.
   * @return The z component of the cross product vector.
   */
  [[nodiscard]] constexpr auto
  cross_product(const vec2 &other) const noexcept -> float {
    return x() * other.y() - y() * other.x();
  }

  /**
   * @brief Unary operator plus (promotion).
   * @return This vec2.
   */
  [[nodiscard]] constexpr auto operator+() const noexcept -> vec2 {
    return *this;
  }

  /**
   * @brief Unary operator minus (negation).
   * @return This vec2 with opposite values.
   */
  [[nodiscard]] constexpr auto operator-() const noexcept -> vec2 {
    return {x_t{-x()}, y_t(-y())};
  }

  /**
   * @brief Addition operator.
   * @param rhs The right hand side of the operator.
   * @return The vec2 resulted from the addition.
   */
  [[nodiscard]] constexpr auto
  operator+(const vec2 &rhs) const noexcept -> vec2 {
    return {x_t{x() + rhs.x()}, y_t{y() + rhs.y()}};
  }

  /**
   * @brief Addition and assignment operator.
   * @param rhs The right hand side of the operator.
   * @return The reference of lhs.
   */
  constexpr auto operator+=(const vec2 &rhs) noexcept -> vec2 & {
    x() += rhs.x();
    y() += rhs.y();
    return *this;
  }

  /**
   * @brief Subtraction operator.
   * @param rhs The right hand side of the operator.
   * @return The vec2 resulted from the substraction.
   */
  [[nodiscard]] constexpr auto
  operator-(const vec2 &rhs) const noexcept -> vec2 {
    return {x_t{x() - rhs.x()}, y_t{y() - rhs.y()}};
  }

  /**
   * @brief Subtracttion and assignment operator.
   * @param rhs The right hand side of the operator.
   * @return The reference to self.
   */
  constexpr auto operator-=(const vec2 &rhs) noexcept -> vec2 & {
    x() -= rhs.x();
    y() -= rhs.y();
    return *this;
  }

  /**
   * @brief Multiplication operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The vec2 resulted from the multiplication.
   */
  [[nodiscard]] constexpr auto
  operator*(const value_t rhs) const noexcept -> vec2 {
    return {x_t{x() * rhs}, y_t{y() * rhs}};
  }

  /**
   * @brief Multiplication operator by a scalar.
   * @param lhs The left hand side of the operator.
   * @param rhs The right hand side of the operator.
   * @return The vec2 resulted from the multiplication.
   */
  [[nodiscard]] friend constexpr auto
  operator*(const value_t lhs, const vec2 &rhs) noexcept -> vec2 {
    return rhs * lhs;
  }

  /**
   * @brief Multiplication and assignment operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The reference to self.
   */
  constexpr auto operator*=(const value_t rhs) noexcept -> vec2 & {
    x() *= rhs;
    y() *= rhs;
    return *this;
  }

  /**
   * @brief Division operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The vec2 resulted from the division.
   */
  [[nodiscard]] constexpr auto
  operator/(const value_t rhs) const noexcept -> vec2 {
    return {x_t{x() / rhs}, y_t{y() / rhs}};
  }

  /**
   * @brief Division and assignment operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The reference to self.
   */
  constexpr auto operator/=(const value_t rhs) noexcept -> vec2 & {
    x() /= rhs;
    y() /= rhs;
    return *this;
  }
};

/**
 * @brief Formats the vec2 into a readable std::string.
 * @tparam T The value type of the vec2.
 * @param vec2 The vec2 to be represented in a std::string.
 * @return The std::string representing the vec2.
 */
template <typename T>
[[nodiscard]] constexpr std::string to_string(const vec2<T> &vec2) noexcept {
  return std::format("[x: {}, y: {}]", vec2.x(), vec2.y());
}

/**
 * @brief Ostream operator.
 * @tparam T The value type of the vec2.
 * @param os The output stream.
 * @param vec2 The vec2 to be printed in the ostream.
 * @return The reference to os.
 */
template <typename T>
std::ostream &operator<<(std::ostream &os, const vec2<T> &vec2) noexcept {
  os << to_string(vec2);
  return os;
}

} // namespace gmath

#endif // LIB_GMATH_INCLUDE_GMATH_VEC2_HPP_
