#include <doctest/doctest.h>
#include <gmath/gmath.hpp>

using namespace gmath;

TEST_SUITE("Functional") {
  TEST_CASE("Basic usage") {
    vec2i b{xi{12}, yi{3}};
    REQUIRE(b.x() == 12);
    REQUIRE(b.y() == 3);

    vec2f a{xf{6.3f}, yf{10.9f}};
    REQUIRE(a.x() == 6.3f);
    REQUIRE(a.y() == 10.9f);
  } // TEST_CASE("Basic usage")

  TEST_CASE("Getters and setters") {
    SUBCASE("Run-time") {
      vec2i a{};
      REQUIRE(a == vec2i{xi{0}, yi{0}});
      a.x() = 32;
      REQUIRE(++a.x() == 33);
      a.y() = 1;
      REQUIRE(a.y()++ == 1);
    } // SUBCASE("Run-time")
      //
    SUBCASE("Compile-time") {
      constexpr vec2i a{xi{2}, yi{3}};
      static_assert(a.x() == 2, "vec2 values access is not constexpr");
      static_assert(++(vec2i{}.operator+=(a)).x() == 3,
                    "vec2 values access is not constexpr");
    } // SUBCASE("Compile-time")
  }

  TEST_CASE("Conversion constructor") {
    SUBCASE("Run-time") {
      vec2i a{vec2f{xf{5.6f}, yf{4.3f}}};
      REQUIRE(a == vec2i{xi{5}, yi{4}});
    } // SUBCASE("Run-time")
    SUBCASE("Compile-time") {
      constexpr vec2i a{vec2f{xf{5.6f}, yf{4.3f}}};
      static_assert(a == vec2i{xi{5}, yi{4}},
                    "vec2 conversion is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Conversion constructor")

} // TEST_SUITE("Functional")

TEST_SUITE("Math") {

  TEST_CASE("Relational") {
    SUBCASE("Run-time") {
      REQUIRE(vec2i{xi{10}, yi{4}} == vec2i{xi{10}, yi{4}});
      REQUIRE_FALSE(vec2i{xi{10}, yi{4}} == vec2i{xi{10}, yi{0}});
      REQUIRE(vec2i{xi{10}, yi{4}} != vec2i{xi{10}, yi{1}});
      REQUIRE_FALSE(vec2i{xi{10}, yi{4}} != vec2i{xi{10}, yi{4}});
      REQUIRE(vec2i{xi{10}, yi{4}} < vec2i{xi{10}, yi{6}});
      REQUIRE_FALSE(vec2i{xi{10}, yi{4}} < vec2i{xi{10}, yi{4}});
      REQUIRE(vec2i{xi{10}, yi{4}} <= vec2i{xi{10}, yi{4}});
      REQUIRE_FALSE(vec2i{xi{10}, yi{4}} <= vec2i{xi{10}, yi{3}});
      REQUIRE(vec2i{xi{10}, yi{4}} > vec2i{xi{9}, yi{1}});
      REQUIRE_FALSE(vec2i{xi{10}, yi{4}} > vec2i{xi{10}, yi{8}});
      REQUIRE(vec2i{xi{10}, yi{4}} >= vec2i{xi{10}, yi{1}});
      REQUIRE_FALSE(vec2i{xi{10}, yi{4}} >= vec2i{xi{10}, yi{7}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      static_assert(vec2i{xi{5}, yi{3}} == vec2i{xi{5}, yi{3}},
                    "vec2 comparison is not constexpr");
      static_assert(!(vec2i{xi{5}, yi{3}} == vec2i{xi{4}, yi{3}}),
                    "vec2 comparison is not constexpr");
      static_assert(vec2i{xi{5}, yi{3}} != vec2i{xi{4}, yi{3}},
                    "vec2 comparison is not constexpr");
      static_assert(!(vec2i{xi{5}, yi{3}} != vec2i{xi{5}, yi{3}}),
                    "vec2 comparison is not constexpr");
      static_assert(vec2i{xi{5}, yi{3}} < vec2i{xi{5}, yi{4}},
                    "vec2 comparison is not constexpr");
      static_assert(!(vec2i{xi{5}, yi{3}} < vec2i{xi{5}, yi{2}}),
                    "vec2 is not constexpr");
      static_assert(vec2i{xi{5}, yi{3}} <= vec2i{xi{5}, yi{3}},
                    "vec2 comparison is not constexpr");
      static_assert(!(vec2i{xi{5}, yi{3}} <= vec2i{xi{5}, yi{1}}),
                    "vec2 is not constexpr");
      static_assert(vec2i{xi{6}, yi{3}} > vec2i{xi{5}, yi{4}},
                    "vec2 comparison is not constexpr");
      static_assert(!(vec2i{xi{5}, yi{3}} > vec2i{xi{5}, yi{9}}),
                    "vec2 comparison is not constexpr");
      static_assert(vec2i{xi{5}, yi{3}} >= vec2i{xi{5}, yi{2}},
                    "vec2 comparison is not constexpr");
      static_assert(!(vec2i{xi{5}, yi{3}} >= vec2i{xi{10}, yi{10}}),
                    "vec2 comparison is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Relational")

  TEST_CASE("Addition") {
    SUBCASE("Run-time") {
      vec2i a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      REQUIRE(+a == vec2i{xi{10}, yi{4}});
      REQUIRE(a + b == vec2i{xi{17}, yi{6}});
      a += b;
      REQUIRE(a == vec2i{xi{17}, yi{6}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr vec2i a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      static_assert(+a == vec2i{xi{10}, yi{4}},
                    "vec2 addition is not constexpr");
      static_assert(a + b == vec2i{xi{17}, yi{6}},
                    "vec2 addition is not constexpr");
      static_assert(vec2i{xi{1}, yi{2}}.operator+=(a) == vec2i{xi{11}, yi{6}},
                    "vec2 addition is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Addition")

  TEST_CASE("Subtraction") {
    SUBCASE("Run-time") {
      vec2i a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      REQUIRE(-a == vec2i{xi{-10}, yi{-4}});
      REQUIRE(a - b == vec2i{xi{3}, yi{2}});
      REQUIRE(b - a == vec2i{xi{-3}, yi{-2}});
      a -= b;
      REQUIRE(a == vec2i{xi{3}, yi{2}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr vec2i a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      static_assert(-a == vec2i{xi{-10}, yi{-4}},
                    "vec2 subtraction is not constexpr");
      static_assert(a - b == vec2i{xi{3}, yi{2}},
                    "vec2 subtraction is not constexpr");
      static_assert(b - a == -vec2i{xi{3}, yi{2}},
                    "vec2 subtraction is not constexpr");
      static_assert(vec2i{xi{1}, yi{2}}.operator-=(a) == vec2i{xi{-9}, yi{-2}},
                    "vec2 subtraction is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Subtraction")

  TEST_CASE("Multiplication by a scalar") {
    SUBCASE("Run-time") {
      vec2i a{xi{10}, yi{4}};
      REQUIRE(a * 3 == vec2i{xi{30}, yi{12}});
      REQUIRE(3 * a == vec2i{xi{30}, yi{12}});
      a *= 3;
      REQUIRE(a == vec2i{xi{30}, yi{12}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr vec2i a{xi{10}, yi{4}};
      static_assert(a * 0 == vec2i{}, "vec2 multiplication is not constexpr");
      static_assert(0 * a == vec2i{}, "vec2 multiplication is not constexpr");
      static_assert(vec2i{xi{1}, yi{2}}.operator*=(2) == vec2i{xi{2}, yi{4}},
                    "vec2 multiplication is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Multiplication by a scalar")

  TEST_CASE("Division by a scalar") {
    SUBCASE("Run-time") {
      vec2i a{xi{10}, yi{4}};
      REQUIRE(a / 2 == vec2i{xi{5}, yi{2}});
      a /= 2;
      REQUIRE(a == vec2i{xi{5}, yi{2}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr vec2i a{xi{10}, yi{4}};
      static_assert(a / 4 == vec2i{xi{2}, yi{1}},
                    "vec2 multiplication is not constexpr");
      static_assert(vec2i{xi{2}, yi{2}}.operator/=(2) == vec2i{xi{1}, yi{1}},
                    "vec2 multiplication is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Division by a scalar")

  TEST_SUITE("Operations") {

    TEST_CASE("Magnitude") {
      SUBCASE("Run-time") {
        vec2f a{xf{7}, yf{3}};
        REQUIRE(is_equal_approx(a.magnitude(), 7.615773105f));
      } // SUBCASE("Run-time")
      SUBCASE("Compile-time") {
        // CAN'T UNTIL C++26
      } // SUBCASE("Compile-time")
    } // TEST_CASE("Magnitude")

    TEST_CASE("Magnitude squared") {
      SUBCASE("Run-time") {
        vec2f a{xf{4}, yf{2}};
        REQUIRE(is_equal_approx(a.magnitude_squared(), 20.f));
      } // SUBCASE("Run-time")
      SUBCASE("Compile-time") {
        constexpr vec2f a{xf{8}, yf{1}};
        static_assert(is_equal_approx(a.magnitude_squared(), 65.f),
                      "magnitude_squared is not constexpr");
      } // SUBCASE("Compile-time")
    } // TEST_CASE("Magnitude squared")

    TEST_CASE("Normalized") {
      SUBCASE("Run-time") {
        vec2f a{xf{7}, yf{9}};
        REQUIRE(is_equal_approx(a.normalized().x(), 0.6139406f));
        REQUIRE(is_equal_approx(a.normalized().y(), 0.7893522f));
        REQUIRE(is_equal_approx(a.normalized().magnitude(), 1.f));
      } // SUBCASE("Run-time")
      SUBCASE("Compile-time") {
        // CAN'T UNTIL C++26
      } // SUBCASE("Compile-time")
    } // TEST_CASE("Normalized")

    TEST_CASE("Perpendicular") {
      SUBCASE("Run-time") {
        vec2f a{xf{6}, yf{12}};
        REQUIRE(a.perpendicular() == vec2f{xf{12}, yf{-6}});
      } // SUBCASE("Run-time")
      SUBCASE("Compile-time") {
        constexpr vec2f a{xf{7}, yf{2}};
        static_assert(a.perpendicular() == vec2f{xf{2}, yf{-7}},
                      "perpendicular is not constexpr");
      } // SUBCASE("Compile-time")
    } // TEST_CASE("Perpendicular")

    TEST_CASE("Dot product") {
      SUBCASE("Run-time") {
        vec2f a{xf{5}, yf{14}};
        vec2f b{xf{12}, yf{7}};
        REQUIRE(a.dot_product(b) == 158.f);
        REQUIRE(b.dot_product(a) == 158.f);
      } // SUBCASE("Run-time")
      SUBCASE("Compile-time") {
        constexpr vec2f a{xf{4}, yf{4}};
        constexpr vec2f b{xf{10}, yf{6}};
        static_assert(a.dot_product(b) == 64.f, "dot_product is not constexpr");
        static_assert(b.dot_product(a) == 64.f, "dot_product is not constexpr");
      } // SUBCASE("Compile-time")
    } // TEST_CASE("Dot product")

    TEST_CASE("Cross product") {
      SUBCASE("Run-time") {
        vec2f a{xf{5}, yf{14}};
        vec2f b{xf{12}, yf{7}};
        REQUIRE(a.cross_product(b) == -133.f);
        REQUIRE(b.cross_product(a) == 133.f);
      } // SUBCASE("Run-time")
      SUBCASE("Compile-time") {
        constexpr vec2f a{xf{4}, yf{4}};
        constexpr vec2f b{xf{10}, yf{6}};
        static_assert(a.cross_product(b) == -16.f,
                      "cross_product is not constexpr");
        static_assert(b.cross_product(a) == 16.f,
                      "cross_product is not constexpr");
      } // SUBCASE("Compile-time")
    } // TEST_CASE("Cross product")

  } // TEST_SUITE("Operations")
}
