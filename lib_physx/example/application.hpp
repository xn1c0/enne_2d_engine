#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <algorithm>
#include <cmath>
#include <gmath/vec2.hpp>
#include <numeric>
#include <optional>
#include <physx/bodies.hpp>
#include <sdlpp/sdlpp.hpp>
#include <vector>

#include "physx/bodies/rigid_body.hpp"
#include "physx/core/collision_data.hpp"
#include "physx/core/collision_detection.hpp"
#include "sdlpp/events/event.hpp"
#include "sdlpp/events/mouse_event.hpp"
#include "stopwatch.hpp"

constexpr auto to_sdlpp_point(const gmath::vec2f v) noexcept -> sdlpp::pointf {
  return {sdlpp::xf{static_cast<float>(v.x())},
          sdlpp::yf(static_cast<float>(v.y()))};
}

constexpr auto to_sdlpp_point(const gmath::vec2i v) noexcept -> sdlpp::pointf {
  return {sdlpp::xf{static_cast<float>(v.x())},
          sdlpp::yf(static_cast<float>(v.y()))};
}

enum class time_step_type { fixed = 1, variable = 2 };

class application {
private:
  sdlpp::sdl mSDL;
  sdlpp::window mWindow;
  sdlpp::renderer mRenderer;

  bool mIsRunning;

  std::vector<physx::rigid_body> mBodies;

  stopwatch::stopwatch mStopWatch;
  std::uint64_t mInitTime;
  std::uint64_t mLastFrameTime;
  float mTargetFrameRate;
  std::vector<float> mFrameTimes;
  std::uint64_t mElapsedRenderTime;
  time_step_type mTimeStep;

  bool mIsMouseButtonLeftPressed{false};

  const float PIXELS_PER_METER{10.0f};

  gmath::vec2f mPushForce; ///< The force directing the particle.

  sdlpp::recti liquid{
      sdlpp::pointi{sdlpp::xi{}, sdlpp::yi{mWindow.drawable_size().h() - 200}},
      sdlpp::rsi{sdlpp::wi{mWindow.drawable_size().w()},
                 sdlpp::hi{mWindow.drawable_size().h() - 200}}};

public:
  float currentFPS;

  application()
      : mSDL{sdlpp::init_flags::everithing},
        mWindow{"sample window", sdlpp::window::position_centered,
                sdlpp::rsi{sdlpp::wi{800}, sdlpp::hi{600}},
                sdlpp::window_flags::resizable},
        mRenderer{mWindow, -1,
                  sdlpp::renderer_flags::accelerated |
                      sdlpp::renderer_flags::present_vsync},
        mIsRunning{true}, mTimeStep{time_step_type::fixed},
        mTargetFrameRate{1000.0f / 60.0f} {
    mStopWatch.start();
    setup();
  }

  constexpr bool is_running() const { return mIsRunning; }

  void setup() {
    // mBodies.emplace_back(physx::rigid_body{
    //     physx::circle{physx::radius{25.0f}},
    //     gmath::vec2f{gmath::xf{100.f}, gmath::yf{100.f}},
    //     physx::mass{10.f}});

    // mBodies.emplace_back(physx::rigid_body{
    //     physx::box{physx::width{200.f}, physx::height{100.f}},
    //     gmath::vec2f{gmath::xf{300.f}, gmath::yf{400.f}}, 10.f});

    mBodies.emplace_back(physx::rigid_body{
        physx::circle{physx::radius{60.0f}},
        gmath::vec2f{gmath::xf{300.f}, gmath::yf{300.f}}, physx::mass{0.f}});
  }

  void run() {
    while (mIsRunning) {

      input();
      if (mTimeStep == time_step_type::fixed) {
        if (mStopWatch.elapsed() >= mTargetFrameRate) {
          update(mStopWatch.elapsed());
          render(mStopWatch.elapsed());

          mFrameTimes.push_back(static_cast<float>(mStopWatch.elapsed()));

          if (mFrameTimes.size() >= 100) {
            std::swap(mFrameTimes.front(), mFrameTimes.back());
            mFrameTimes.pop_back();
          }

          currentFPS = std::round(
              1000.0f /
              ((std::accumulate(mFrameTimes.begin(), mFrameTimes.end(), 0)) *
               1.f / mFrameTimes.size()));

          mStopWatch.start();
        }
      } else if (mTimeStep == time_step_type::variable) {

        std::uint64_t currentFrameTime = mStopWatch.elapsed();
        std::uint64_t elapsed = currentFrameTime - mLastFrameTime;

        mLastFrameTime = currentFrameTime;

        update(elapsed);
        render(elapsed);

        mStopWatch.pause();

        mFrameTimes.push_back(static_cast<float>(elapsed));

        if (mFrameTimes.size() >= 100) {
          std::swap(mFrameTimes.front(), mFrameTimes.back());
          mFrameTimes.pop_back();
        }

        currentFPS = std::round(
            1000.0f /
            ((std::accumulate(mFrameTimes.begin(), mFrameTimes.end(), 0)) *
             1.f / mFrameTimes.size()));

        mStopWatch.start();
      }
    }
  }

  void input() {
    sdlpp::event_handler handler;
    while (handler.poll()) {
      if (handler.is(sdlpp::event_type::quit)) {
        mIsRunning = false;
      } else if (handler.is(sdlpp::event_type::mouse_button_down)) {
        const auto &e{handler.get<sdlpp::mouse_button_event>()};
        mBodies.emplace_back(physx::rigid_body{
            physx::circle{physx::radius{20.0f}},
            gmath::vec2f{gmath::xf{static_cast<float>(e.position().x())},
                         gmath::yf{static_cast<float>(e.position().y())}},
            physx::mass{10.f}});
      }
    }
  }

  void update(std::uint64_t elapsed) {
    const auto dt = float{elapsed / 1000.0f};

    for (auto ita{mBodies.begin()}; ita != mBodies.end(); ++ita) {

      const gmath::vec2f weight{
          gmath::xf{0.0f}, gmath::yf{ita->mass() * 9.8f * PIXELS_PER_METER}};

      const gmath::vec2f wind{gmath::xf{10.f * PIXELS_PER_METER}, gmath::yf{}};

      ita->apply_force(weight);
      ita->apply_force(wind);
      // ita->apply_torque(1000.f);
      ita->update(dt);

      // Check collisions and contact
      for (auto itb{std::next(ita, 1)}; itb != mBodies.end(); ++itb) {
        auto cd{physx::cd::get_collision_data(*ita, *itb)};
        if (cd.has_value())
          cd.value().resolve_collision();
      }

      if (ita->shape().is<physx::circle>()) {
        auto &circleShape{ita->shape().get<physx::circle>()};
        if (ita->position().x() - circleShape.radius() <= 0.0f) {
          ita->position().x() = circleShape.radius();
          ita->velocity().x() *= 0.9f;
        } else if (ita->position().x() + circleShape.radius() >=
                   mWindow.size().w()) {
          ita->position().x() = mWindow.size().w() - circleShape.radius();
          ita->velocity().x() *= 0.9f;
        }
        if (ita->position().y() - circleShape.radius() <= 0.0f) {
          ita->position().y() = circleShape.radius();
          ita->velocity().y() *= 0.9f;
        } else if (ita->position().y() + circleShape.radius() >=
                   mWindow.size().h()) {
          ita->position().y() = mWindow.size().h() - circleShape.radius();
          ita->velocity().y() *= 0.9f;
        }
      }
    }
  }

  void render(std::uint64_t elapsed) {
    mRenderer.set_draw_colour(sdlpp::colours::black);
    mRenderer.clear();

    auto doRender = [&](physx::rigid_body &body) {
      if (body.shape().is<physx::circle>()) {
        auto &circleShape{body.shape().get<physx::circle>()};
        mRenderer.draw_circle(to_sdlpp_point(body.position()),
                              circleShape.radius(), sdlpp::colours::white);
        mRenderer.draw_line(
            to_sdlpp_point(body.position()),
            sdlpp::pointf{
                sdlpp::xf{body.position().x() +
                          cos(body.rotation()) * circleShape.radius()},
                sdlpp::yf{body.position().y() +
                          sin(body.rotation()) * circleShape.radius()}});
      } else {
        auto &boxShape{body.shape().get<physx::box>()};
        std::vector<sdlpp::pointf> points;
        auto doConvert = [&](const gmath::vec2f &v) {
          points.emplace_back(
              sdlpp::pointf{sdlpp::xf{v.x()}, sdlpp::yf{v.y()}});
        };
        std::for_each(boxShape.world_vertices().begin(),
                      boxShape.world_vertices().end(), doConvert);

        points.emplace_back(points[0]);

        mRenderer.draw_lines(points, sdlpp::colours::white);
        mRenderer.draw_point(to_sdlpp_point(body.position()));
      }
    };

    std::for_each(mBodies.begin(), mBodies.end(), doRender);

    mRenderer.present();
  }
};

#endif // APPLICATION_H_
