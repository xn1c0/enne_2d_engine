#ifndef APPLICATION_1_H_
#define APPLICATION_1_H_

#include <cassert>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <numeric>
#include <physx/particle.hpp>
#include <sdlpp/sdlpp.hpp>
#include <unistd.h>
#include <utility>
#include <vector>

#include "gmath/primitives.hpp"
#include "gmath/vec2.hpp"
#include "physx/forces.hpp"
#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/events/event.hpp"
#include "sdlpp/events/misc_event.hpp"
#include "sdlpp/events/mouse_event.hpp"
#include "sdlpp/input/keyboard.hpp"
#include "sdlpp/video/colour.hpp"
#include "stopwatch.hpp"

enum class time_step_type { fixed = 1, variable = 2 };

class application {
private:
  sdlpp::sdl mSDL;
  sdlpp::window mWindow;
  sdlpp::renderer mRenderer;

  bool mIsRunning;

  std::vector<physx::particle> mParticles;
  physx::particle p0;

  stopwatch::stopwatch mStopWatch;
  std::uint64_t mInitTime;
  std::uint64_t mLastFrameTime;
  float mTargetFrameRate;
  std::vector<float> mFrameTimes;
  std::uint64_t mElapsedRenderTime;
  time_step_type mTimeStep;

  const float PIXEL_PER_METER{10.0f};

  gmath::vec2f mPushForce; ///< The force directing the particle.

  sdlpp::recti liquid{
      sdlpp::pointi{sdlpp::xi{}, sdlpp::yi{mWindow.drawable_size().h() - 200}},
      sdlpp::rsi{sdlpp::wi{mWindow.drawable_size().w()},
                 sdlpp::hi{mWindow.drawable_size().h() - 200}}};

public:
  float currentFPS;

  application()
      : mSDL{sdlpp::init_flags::everithing},
        mWindow{"sample window", sdlpp::window::position_centered,
                sdlpp::rsi{sdlpp::wi{800}, sdlpp::hi{600}},
                sdlpp::window_flags::resizable},
        mRenderer{mWindow, -1, sdlpp::renderer_flags::accelerated},
        mIsRunning{true} {
    mStopWatch.start();
    setup();
  }

  constexpr bool is_running() const { return mIsRunning; }

  void setup() {

    auto p1 = physx::particle{gmath::vec2f{gmath::xf{300.f}, gmath::yf{100.f}},
                              100.f};
    p1.radius() = 10.0f;
    p0 = p1;
    // mParticles.push_back(p1);

    // auto p2 = physx::particle{
    //     gmath::vec2f{gmath::fx{100.f}, gmath::fy{100.f}}, 10.f};
    // p2.radius = 20.0f;
    // mParticles.push_back(p2);

    mTimeStep = time_step_type::fixed;
    mTargetFrameRate = 1000.0f / 60.0f;
  }

  void run() {

    while (mIsRunning) {

      input();
      if (mTimeStep == time_step_type::fixed) {
        if (mStopWatch.elapsed() >= mTargetFrameRate) {
          update(mStopWatch.elapsed());
          render(mStopWatch.elapsed());

          mFrameTimes.push_back(static_cast<float>(mStopWatch.elapsed()));

          if (mFrameTimes.size() >= 100) {
            std::swap(mFrameTimes.front(), mFrameTimes.back());
            mFrameTimes.pop_back();
          }

          // std::cout << mFrameTimes.size() << std::endl;
          // std::cout << std::accumulate(mFrameTimes.begin(),
          // mFrameTimes.end(),
          //                              0)
          //           << std::endl;

          currentFPS = std::round(
              1000.0f /
              ((std::accumulate(mFrameTimes.begin(), mFrameTimes.end(), 0)) *
               1.f / mFrameTimes.size()));

          // std::cout << "AVG FPS = " << currentFPS << std::endl;

          mStopWatch.start();
        }
      } else if (mTimeStep == time_step_type::variable) {

        std::uint64_t currentFrameTime = mStopWatch.elapsed();
        std::uint64_t elapsed = currentFrameTime - mLastFrameTime;

        mLastFrameTime = currentFrameTime;

        update(elapsed);
        render(elapsed);

        mStopWatch.pause();

        mFrameTimes.push_back(static_cast<float>(elapsed));

        if (mFrameTimes.size() >= 100) {
          std::swap(mFrameTimes.front(), mFrameTimes.back());
          mFrameTimes.pop_back();
        }

        // Calculate the average frames per second
        currentFPS = std::round(
            1000.0f /
            ((std::accumulate(mFrameTimes.begin(), mFrameTimes.end(), 0)) *
             1.f / mFrameTimes.size()));

        std::cout << "AVG FPS = " << currentFPS << std::endl;

        mStopWatch.start();
      }
    }
  }

  void input() {
    sdlpp::event_handler handler;
    while (handler.poll()) {
      if (handler.is(sdlpp::event_type::quit)) {
        mIsRunning = false;
      }
      if (handler.is(sdlpp::event_type::key_down)) {
        // We received a keyboard event, so get the internal event
        const auto &keyboardEvent = handler.get<sdlpp::keyboard_event>();
        if (keyboardEvent.key() == sdlpp::key_codes::a)
          mPushForce.x() += -5.0f * PIXEL_PER_METER;
        if (keyboardEvent.key() == sdlpp::key_codes::d)
          mPushForce.x() += 5.0f * PIXEL_PER_METER;
      }
      if (handler.is(sdlpp::event_type::key_up)) {
        // We received a keyboard event, so get the internal event
        const auto &keyboardEvent = handler.get<sdlpp::keyboard_event>();
        if (keyboardEvent.key() == sdlpp::key_codes::a)
          mPushForce.x() = 0.0f;
        if (keyboardEvent.key() == sdlpp::key_codes::d)
          mPushForce.x() = 0.0f;
      }
      if (handler.is(sdlpp::event_type::mouse_button_down)) {
        const auto &mouseEvent = handler.get<sdlpp::mouse_button_event>();
        const physx::particle newParticle{
            gmath::vec2f{gmath::xf{static_cast<float>(mouseEvent.x())},
                         gmath::yf{static_cast<float>(mouseEvent.y())}},
            1.0f};
        mParticles.push_back(newParticle);
      }
    }
  }

  void update(std::uint64_t elapsed) {
    auto dt = float{elapsed / 1000.0f};

    // auto wind = gmath::vec2f{gmath::fx{2.0f * PIXEL_PER_METER},
    //                          gmath::fy{}};
    //
    auto weight = gmath::vec2f{gmath::xf{},
                               gmath::yf{p0.mass() * 9.8f * PIXEL_PER_METER}};
    // p.apply_force(wind);
    p0.apply_force(mPushForce);
    p0.apply_force(
        physx::forces::generate_friction_force(p0, 10.0f * PIXEL_PER_METER));
    p0.integrate(dt);

    // if (p.position().y() >= liquid.h())
    //   p.apply_force(physx::forces::generate_drag_force(p, 0.03f));

    const auto px = p0.position().x();
    const auto py = p0.position().y();
    const auto ws = mWindow.size();

    // assert(p.position().x() >= 0.0f && p.position().x() <= ws.w());
    //   assert(p.position().y() >= 0.0f && p.position().y() <= ws.h());

    if (px + p0.radius() >= ws.w()) {
      p0.velocity().x() *= -0.9f;
      p0.position().x() = ws.w() - p0.radius();
    } else if (px - p0.radius() <= 0.f) {
      p0.velocity().x() *= -0.9f;
      p0.position().x() = p0.radius();
    }

    if (py + p0.radius() >= ws.h()) {
      p0.velocity().y() *= -0.9f;
      p0.position().y() = ws.h() - p0.radius();
    } else if (py - p0.radius() <= 0.f) {
      p0.velocity().y() *= -0.9f;
      p0.position().y() = p0.radius();
    }

    for (auto &p : mParticles) {
      // p.apply_force(weight);
      auto attraction =
          physx::forces::generate_gravitational_force(p0, p, 1000.0f);
      p0.apply_force(-attraction);
      p.apply_force(attraction);

      p0.apply_force(
          physx::forces::generate_friction_force(p0, 40.0f * PIXEL_PER_METER));

      if (p.position().y() >= liquid.h())
        p.apply_force(physx::forces::generate_drag_force(p, 0.03f));

      p0.integrate(dt);
      p.integrate(dt);

      const auto px = p.position().x();
      const auto py = p.position().y();
      const auto ws = mWindow.size();

      // assert(p.position().x() >= 0.0f && p.position().x() <= ws.w());
      // assert(p.position().y() >= 0.0f && p.position().y() <= ws.h());

      if (px + p.radius() >= ws.w()) {
        p.velocity().x() *= -0.9f;
        p.position().x() = ws.w() - p.radius();
      } else if (px - p.radius() <= 0.f) {
        p.velocity().x() *= -0.9f;
        p.position().x() = p.radius();
      }

      if (py + p.radius() >= ws.h()) {
        p.velocity().y() *= -0.9f;
        p.position().y() = ws.h() - p.radius();
      } else if (py - p.radius() <= 0.f) {
        p.velocity().y() *= -0.9f;
        p.position().y() = p.radius();
      }
    }
  }

  void render(std::uint64_t elapsed) {
    mRenderer.set_draw_colour(sdlpp::colours::black);
    mRenderer.clear();

    mRenderer.draw_fill_rect(liquid, sdlpp::colours::dark_cyan);

    mRenderer.draw_circle(sdlpp::pointf{sdlpp::xf{p0.position().x()},
                                        sdlpp::yf{p0.position().y()}},
                          p0.radius(), sdlpp::colours::white);

    for (auto &p : mParticles) {
      mRenderer.draw_circle(sdlpp::pointf{sdlpp::xf{p.position().x()},
                                          sdlpp::yf{p.position().y()}},
                            p.radius(), sdlpp::colours::white);
    }
    mRenderer.present();
  }
};

#endif // APPLICATION_1_H_
