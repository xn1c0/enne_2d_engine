#include "application.hpp"
#include <exception>
#include <iostream>

auto main() -> int try {
  application app;
  app.run();
  return 0;
} catch (std::exception &e) {
  // If case of error, print it and exit with error
  std::cerr << e.what() << std::endl;
  return 1;
}
