#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <chrono>
#include <vector>

namespace stopwatch {

class stopwatch {

public:
  enum class time_format { nanoseconds, microseconds, milliseconds, seconds };

private:
  using clock = std::chrono::high_resolution_clock;
  using time_point_t = std::chrono::time_point<clock>;

  time_point_t mStartTime;
  time_point_t mPauseStartTime;
  std::vector<time_point_t> mLaps;

  static constexpr time_point_t mZeroTimePoint{clock::duration::zero()};

  static constexpr auto has_time(const time_point_t &t) noexcept -> bool {
    return t != mZeroTimePoint;
  }

  template <time_format fmt = time_format::milliseconds>
  static constexpr auto
  ticks(const time_point_t &startTime, const time_point_t &endTime,
        const time_point_t &pauseStartTime) noexcept -> std::uint64_t {

    decltype(startTime - endTime) duration;
    if (has_time(pauseStartTime)) {
      duration = pauseStartTime - startTime;
    } else if (has_time(startTime)) {
      duration = endTime - startTime;
    } else {
      duration = clock::duration::zero();
    }

    switch (fmt) {
    case time_format::nanoseconds:
      return std::chrono::duration_cast<std::chrono::nanoseconds>(duration)
          .count();
    case time_format::microseconds:
      return std::chrono::duration_cast<std::chrono::microseconds>(duration)
          .count();
    case time_format::milliseconds:
      return std::chrono::duration_cast<std::chrono::milliseconds>(duration)
          .count();
    case time_format::seconds:
      return std::chrono::duration_cast<std::chrono::seconds>(duration).count();
    }
  }

public:
  [[nodiscard]] auto is_paused() noexcept -> bool {
    return has_time(mPauseStartTime) || !has_time(mStartTime);
  }

public:
  auto start() noexcept -> void {
    const time_point_t now{clock::now()};
    if (mPauseStartTime != mZeroTimePoint) {
      mStartTime += (now - mPauseStartTime);
      mPauseStartTime = mZeroTimePoint;
    } else {
      mStartTime = now;
    }
  }

  void pause() noexcept {
    if (!is_paused()) {
      mPauseStartTime = clock::now();
    }
  }

  auto reset() noexcept -> void { *this = stopwatch{}; }

  template <time_format fmt = time_format::milliseconds>
  auto elapsed() noexcept -> std::uint64_t {
    const time_point_t endTime = std::chrono::high_resolution_clock::now();
    return ticks<fmt>(mStartTime, endTime, mPauseStartTime);
  }

  /*
  template <time_format fmt = time_format::milliseconds>
  auto lap() noexcept -> std::uint64_t {
  const time_point_t now{std::chrono::high_resolution_clock::now()};
  const time_point_t lastLap{mLaps.back()};
  mLaps.push_back(std::chrono::high_resolution_clock::now());
  return ticks<fmt>(lastLap, now);
  }
  */
};

} // namespace stopwatch

#endif // STOPWATCH_H_
