#ifndef LIB_PHYSX_INCLUDE_PHYSX_BODIES_RIGID_BODY_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_BODIES_RIGID_BODY_HPP_

#include "gmath/utils.hpp"
#include "gmath/vec2.hpp"
#include <physx/core/shape.hpp>
#include <physx/utils/primitives.hpp>

/**
 * @author Nicolò Plebani
 * @file rigid_body.hpp
 * @brief This file contains the rigid_body class.
 */

namespace physx {

class rigid_body {

private:
  class shape mShape; ///< The shape of the rigid_body.

  gmath::vec2f mPosition;     ///< The position of the body.
  gmath::vec2f mVelocity;     ///< The velocity of the body.
  gmath::vec2f mAcceleration; ///< The acceleration of the body.
  float mMass;                ///< The mass of the body.
  float mInverseMass;         ///< The inverse of the mass.
  gmath::vec2f mNetForce;     ///< The sum of all forces applied on the body.

  float mRotation;               ///< The rotation of the body.
  float mAngularVelocity;        ///< The angular velocity of the body.
  float mAngularAcceleration;    ///< The angular acceleration of the body.
  float mMomentOfInertia;        ///< The moment of inertia of the body.
  float mInverseMomentOfInertia; ///< The inverse of the moment of inertia.
  float mNetTorque; ///< The sum of all angular forces applied on the body.

  float mCoefficientOfResitution; ///< The elasticity of the body.

public:
  /// Getter and setters

  [[nodiscard]] constexpr auto shape() const & -> const class shape & {
    return mShape;
  }
  [[nodiscard]] constexpr auto shape() & -> class shape & { return mShape; }
  [[nodiscard]] constexpr auto shape() && -> class shape && {
    return std::move(mShape);
  }

  [[nodiscard]] constexpr auto position() const & -> const gmath::vec2f & {
    return mPosition;
  }

  [[nodiscard]] constexpr auto position() & -> gmath::vec2f & {
    return mPosition;
  }
  [[nodiscard]] constexpr auto position() && -> gmath::vec2f && {
    return std::move(mPosition);
  }

  [[nodiscard]] constexpr auto velocity() const & -> const gmath::vec2f & {
    return mVelocity;
  }
  [[nodiscard]] constexpr auto velocity() & -> gmath::vec2f & {
    return mVelocity;
  }
  [[nodiscard]] constexpr auto velocity() && -> gmath::vec2f && {
    return std::move(mVelocity);
  }

  [[nodiscard]] constexpr auto acceleration() const & -> const gmath::vec2f & {
    return mAcceleration;
  }
  [[nodiscard]] constexpr auto acceleration() & -> gmath::vec2f & {
    return mAcceleration;
  }
  [[nodiscard]] constexpr auto acceleration() && -> gmath::vec2f && {
    return std::move(mAcceleration);
  }

  [[nodiscard]] constexpr auto mass() const & -> const float & { return mMass; }
  [[nodiscard]] constexpr auto mass() & -> float & { return mMass; }
  [[nodiscard]] constexpr auto mass() && -> float && {
    return std::move(mMass);
  }

  [[nodiscard]] constexpr auto inverse_mass() const & -> const float & {
    return mInverseMass;
  }
  [[nodiscard]] constexpr auto inverse_mass() & -> float & {
    return mInverseMass;
  }
  [[nodiscard]] constexpr auto inverse_mass() && -> float && {
    return std::move(mInverseMass);
  }

  [[nodiscard]] constexpr auto net_force() const & -> const gmath::vec2f & {
    return mNetForce;
  }
  [[nodiscard]] constexpr auto net_force() & -> gmath::vec2f & {
    return mNetForce;
  }
  [[nodiscard]] constexpr auto net_force() && -> gmath::vec2f && {
    return std::move(mNetForce);
  }

  [[nodiscard]] constexpr auto rotation() const & -> const float & {
    return mRotation;
  }
  [[nodiscard]] constexpr auto rotation() & -> float & { return mRotation; }
  [[nodiscard]] constexpr auto rotation() && -> float && {
    return std::move(mRotation);
  }

  [[nodiscard]] constexpr auto angular_velocity() const & -> const float & {
    return mAngularVelocity;
  }
  [[nodiscard]] constexpr auto angular_velocity() & -> float & {
    return mAngularVelocity;
  }
  [[nodiscard]] constexpr auto angular_velocity() && -> float && {
    return std::move(mAngularVelocity);
  }

  [[nodiscard]] constexpr auto angular_acceleration() const & -> const float & {
    return mAngularAcceleration;
  }
  [[nodiscard]] constexpr auto angular_acceleration() & -> float & {
    return mAngularAcceleration;
  }
  [[nodiscard]] constexpr auto angular_acceleration() && -> float && {
    return std::move(mAngularAcceleration);
  }

  [[nodiscard]] constexpr auto moment_of_inertia() const & -> const float & {
    return mMomentOfInertia;
  }
  [[nodiscard]] constexpr auto moment_of_inertia() & -> float & {
    return mMomentOfInertia;
  }
  [[nodiscard]] constexpr auto moment_of_inertia() && -> float && {
    return std::move(mMomentOfInertia);
  }

  [[nodiscard]] constexpr auto inverse_moment_of_inertia() const & -> const
      float & {
    return mInverseMomentOfInertia;
  }
  [[nodiscard]] constexpr auto inverse_moment_of_inertia() & -> float & {
    return mInverseMomentOfInertia;
  }
  [[nodiscard]] constexpr auto inverse_moment_of_inertia() && -> float && {
    return std::move(mInverseMomentOfInertia);
  }

  [[nodiscard]] constexpr auto net_torque() const & -> const float & {
    return mNetTorque;
  }
  [[nodiscard]] constexpr auto net_torque() & -> float & { return mNetTorque; }
  [[nodiscard]] constexpr auto net_torque() && -> float && {
    return std::move(mNetTorque);
  }

  [[nodiscard]] constexpr auto coefficient_of_restitution() const & -> const
      float & {
    return mCoefficientOfResitution;
  }
  [[nodiscard]] constexpr auto coefficient_of_restitution() & -> float & {
    return mCoefficientOfResitution;
  }
  [[nodiscard]] constexpr auto coefficient_of_restitution() && -> float && {
    return std::move(mCoefficientOfResitution);
  }

public:
  /// Constructors

  constexpr rigid_body() noexcept
      : mShape{}, mPosition{}, mVelocity{}, mMass{}, mInverseMass{},
        mNetForce{}, mRotation{}, mAngularVelocity{}, mAngularAcceleration{},
        mMomentOfInertia{}, mInverseMomentOfInertia{}, mNetTorque{},
        mCoefficientOfResitution{1.0f} {}

  template <typename T>
  constexpr rigid_body(
      const T &shape, const gmath::vec2f &position, const physx::mass &mass,
      const elasticity &coefficientOfRestitution = elasticity{1.0f}) noexcept
      : mShape{shape}, mPosition{position}, mVelocity{}, mMass{mass.value()},
        mInverseMass{(mMass != 0.0f) ? 1.0f / mMass : 0.0f}, mNetForce{},
        mRotation{}, mAngularVelocity{}, mAngularAcceleration{},
        mMomentOfInertia{mShape.moment_of_inertia()},
        mInverseMomentOfInertia{(mMass != 0.0f) ? 1.0f / mMomentOfInertia
                                                : 0.0f},
        mNetTorque{},
        mCoefficientOfResitution{coefficientOfRestitution.value()} {}

public:
  /// Methods

  constexpr auto is_static() noexcept -> bool {
    return gmath::is_equal_approx(mInverseMass, 0.0f);
  }

  constexpr auto integrate(const float dt) noexcept -> void {

    if (is_static())
      return;

    // find acceleration based on the forces applied and the mass
    // using inverse_mass() to reduce computation of the division with the
    // mass().
    acceleration() = net_force() * inverse_mass();

    // Semi-implicit Euler
    // integrate the acceleration to find the new velocity
    velocity() += acceleration() * dt;

    // integrate the velocity to find position
    position() += velocity() * dt;

    clear_forces();
  }

  constexpr auto integrate_angular(const float dt) noexcept -> void {

    if (is_static())
      return;

    angular_acceleration() = net_torque() * inverse_moment_of_inertia();

    // integrate alpha to find the new omega
    angular_velocity() += angular_acceleration() * dt;

    // integrate omega to find the new theta
    rotation() += angular_velocity() * dt;

    clear_torque();
  }

  constexpr auto apply_force(const gmath::vec2f &force) noexcept -> void {
    net_force() += force;
  }

  constexpr auto apply_torque(const float torque) noexcept -> void {
    net_torque() += torque;
  }

  constexpr auto apply_impulse(const gmath::vec2f &impulse) -> void {
    if (is_static())
      return;
    velocity() += impulse * inverse_mass();
  }

  constexpr auto clear_forces() noexcept -> void { net_force() = {}; }

  constexpr auto clear_torque() noexcept -> void { net_torque() = {}; }

  auto update(const float dt) noexcept -> void {
    integrate(dt);
    integrate_angular(dt);
    shape().update_vertices(position(), rotation());
  }
};

} // namespace physx

#endif // LIB_PHYSX_INCLUDE_PHYSX_BODIES_RIGID_BODY_HPP_
