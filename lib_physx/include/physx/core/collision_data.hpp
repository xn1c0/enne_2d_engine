#ifndef LIB_PHYSX_INCLUDE_PHYSX_CORE_COLLISION_DATA_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_CORE_COLLISION_DATA_HPP_

#include "physx/bodies/rigid_body.hpp"
#include <algorithm>
#include <physx/bodies.hpp>

/**
 * @author Nicolò Plebani
 * @file collision_data.hpp
 */

namespace physx {

class collision_data {
private:
  rigid_body &mBodyA;
  rigid_body &mBodyB;
  gmath::vec2f mStart;
  gmath::vec2f mEnd;
  gmath::vec2f mDirection;
  float mDepth;

public:
  [[nodiscard]] constexpr auto start() const & -> const gmath::vec2f & {
    return mStart;
  }

  [[nodiscard]] constexpr auto start() && -> gmath::vec2f && {
    return std::move(mStart);
  }

  [[nodiscard]] constexpr auto end() const & -> const gmath::vec2f & {
    return mEnd;
  }

  [[nodiscard]] constexpr auto end() && -> gmath::vec2f && {
    return std::move(mEnd);
  }

  [[nodiscard]] constexpr auto direction() const & -> const gmath::vec2f & {
    return mDirection;
  }

  [[nodiscard]] constexpr auto direction() && -> gmath::vec2f && {
    return std::move(mDirection);
  }

  [[nodiscard]] constexpr auto depth() const & -> const float & {
    return mDepth;
  }

  [[nodiscard]] constexpr auto depth() && -> float && {
    return std::move(mDepth);
  }

  collision_data(rigid_body &bodyA, rigid_body &bodyB,
                 const gmath::vec2f &direction, const gmath::vec2f &start,
                 const gmath::vec2f end, const float depth) noexcept
      : mBodyA{bodyA}, mBodyB{bodyB}, mStart{start}, mEnd{end},
        mDirection{direction}, mDepth{depth} {}

  constexpr auto resolve_penetration() -> void {
    float da{depth() / (mBodyA.inverse_mass() + mBodyB.inverse_mass()) *
             mBodyA.inverse_mass()};
    float db{depth() / (mBodyA.inverse_mass() + mBodyB.inverse_mass()) *
             mBodyB.inverse_mass()};
    mBodyA.position() -= direction() * da;
    mBodyB.position() += direction() * db;
    return;
  }

  constexpr auto resolve_collision() -> void {
    // Apply positional correction using the projection method
    resolve_penetration();

    // Define elasticity
    auto e{std::min(mBodyA.coefficient_of_restitution(),
                    mBodyB.coefficient_of_restitution())};

    // Calculate the ralative velocity between two objects
    auto vRel{mBodyA.velocity() - mBodyB.velocity()};

    // Calculate the relative velocity along the direction of the collision
    // vector
    auto vRelDotDirection{vRel.dot_product(mDirection)};

    // Calulate the collision impulse
    auto impulseDirection{mDirection};
    auto impulseMagnitude{-(1 + e) * vRelDotDirection /
                          (mBodyA.inverse_mass() + mBodyB.inverse_mass())};

    auto impulse{impulseDirection * impulseMagnitude};

    mBodyA.apply_impulse(impulse);
    mBodyB.apply_impulse(-impulse);

    return;
  }
};

} // namespace physx

#endif // LIB_PHYSX_INCLUDE_PHYSX_CORE_COLLISION_DATA_HPP_
