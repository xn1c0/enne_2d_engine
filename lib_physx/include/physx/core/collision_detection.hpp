#ifndef LIB_PHYSX_INCLUDE_PHYSX_COLLISION_DETECTION_SHAPE_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_COLLISION_DETECTION_SHAPE_HPP_

#include "physx/utils/primitives.hpp"
#include <physx/bodies.hpp>
#include <physx/core/collision_data.hpp>

/**
 * @author Nicolò Plebani
 * @file collision_detection.hpp
 */

namespace physx::cd {

constexpr auto get_collision_data_circle_circle(rigid_body &a, rigid_body &b)
    -> maybe<collision_data> {

  const auto &circleA{a.shape().get<circle>()};
  const auto &circleB{b.shape().get<circle>()};

  // Distance vector from a to b
  const gmath::vec2f ab{b.position() - a.position()};

  const float radiusSum{circleA.radius() + circleB.radius()};

  // fast calc with everything squared and no square root involved
  if (!(ab.magnitude_squared() <= radiusSum * radiusSum))
    return nothing;

  const gmath::vec2f normalizedAB{ab.normalized()};
  const gmath::vec2f start{b.position() - normalizedAB * circleB.radius()};
  const gmath::vec2f end{a.position() + normalizedAB * circleA.radius()};
  const float depth{(end - start).magnitude()};
  return {{a, b, normalizedAB, start, end, depth}};
}

constexpr auto get_collision_data(rigid_body &a,
                                  rigid_body &b) -> maybe<collision_data> {
  const bool isCircleA{a.shape().is<circle>()};
  const bool isCircleB{b.shape().is<circle>()};

  if (isCircleA && isCircleB)
    return get_collision_data_circle_circle(a, b);

  return nothing;
}

} // namespace physx::cd

#endif // LIB_PHYSX_INCLUDE_PHYSX_UTILS_SHAPE_HPP_
