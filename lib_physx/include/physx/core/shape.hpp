#ifndef LIB_PHYSX_INCLUDE_PHYSX_CORE_SHAPE_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_CORE_SHAPE_HPP_

#include <cmath>
#include <physx/utils/primitives.hpp>
#include <physx/utils/traits.hpp>
#include <variant>
#include <vector>

/**
 * @author Nicolò Plebani
 * @file shape.hpp
 */

namespace physx {

class circle {
private:
  float mRadius;

public:
  constexpr circle() noexcept : mRadius{} {}
  constexpr circle(const radius &r) noexcept : mRadius{r.value()} {}

  [[nodiscard]] constexpr auto radius() const & -> const float & {
    return mRadius;
  }
  [[nodiscard]] constexpr auto radius() & -> float & { return mRadius; }
  [[nodiscard]] constexpr auto radius() && -> float && {
    return std::move(mRadius);
  }
};

class polygon {
private:
  using v_t = std::vector<gmath::vec2f>;
  std::vector<gmath::vec2f> mLocalVertices;
  std::vector<gmath::vec2f> mWorldVertices;

public:
  [[nodiscard]] constexpr auto local_vertices() const & -> const v_t & {
    return mLocalVertices;
  }

  [[nodiscard]] constexpr auto local_vertices() & -> v_t & {
    return mLocalVertices;
  }

  [[nodiscard]] constexpr auto world_vertices() const & -> const v_t & {
    return mWorldVertices;
  }

  [[nodiscard]] constexpr auto world_vertices() & -> v_t & {
    return mWorldVertices;
  }

  constexpr polygon() noexcept = default;
  constexpr polygon(const std::vector<gmath::vec2f> &vertices) noexcept
      : mLocalVertices{vertices}, mWorldVertices{vertices} {}
};

class box : public polygon {
private:
  float mWidth, mHeight;

public:
  [[nodiscard]] constexpr auto width() const & -> const float & {
    return mWidth;
  }
  [[nodiscard]] constexpr auto width() & -> float & { return mWidth; }
  [[nodiscard]] constexpr auto width() && -> float && {
    return std::move(mWidth);
  }

  [[nodiscard]] constexpr auto height() const & -> const float & {
    return mHeight;
  }
  [[nodiscard]] constexpr auto height() & -> float & { return mHeight; }
  [[nodiscard]] constexpr auto height() && -> float && {
    return std::move(mHeight);
  }

  constexpr box() noexcept : mWidth{}, mHeight{} {}
  constexpr box(const physx::width &w, const physx::height h) noexcept
      : polygon{{gmath::vec2f{gmath::xf{-w.value() / 2.0f},
                              gmath::yf{-h.value() / 2.0f}},
                 gmath::vec2f{gmath::xf{w.value() / 2.0f},
                              gmath::yf{-h.value() / 2.0f}},
                 gmath::vec2f{gmath::xf{w.value() / 2.0f},
                              gmath::yf{h.value() / 2.0f}},
                 gmath::vec2f{gmath::xf{-w.value() / 2.0f},
                              gmath::yf{h.value() / 2.0f}}}},
        mWidth{w.value()}, mHeight{h.value()} {}
};

class shape {
private:
  using data_t = std::variant<circle, polygon, box>;

  data_t mData;

public:
  shape() noexcept = default;

  template <typename T>
  constexpr shape(const T &value) noexcept : mData{value} {}

  template <typename T>
  [[nodiscard]] constexpr auto is() const noexcept -> bool {
    return std::holds_alternative<T>(mData);
  }

  template <typename T> [[nodiscard]] constexpr auto get() -> T & {
    return std::get<T>(mData);
  }

  template <typename T> [[nodiscard]] constexpr auto get() const -> const T & {
    return std::get<T>(mData);
  }

  template <typename T> [[nodiscard]] constexpr auto try_get() noexcept -> T * {
    return std::get_if<T>(&mData);
  }

  template <typename T>
  [[nodiscard]] constexpr auto try_get() const noexcept -> const T * {
    return std::get_if<T>(&mData);
  }

  [[nodiscard]] constexpr auto moment_of_inertia() const noexcept -> float {
    return std::visit(overload{[](const circle &arg) -> float {
                                 return 0.5f * (arg.radius() * arg.radius());
                               },
                               [](const polygon &arg) -> float { return {}; },
                               [](const box &arg) -> float {
                                 return 0.8333333333f *
                                        (arg.width() * arg.width() +
                                         arg.height() * arg.height());
                               }},
                      mData);
  }

  constexpr auto update_vertices(const gmath::vec2f &position,
                                 const float angle) -> void {
    auto doUpdate = [&position, angle](const std::vector<gmath::vec2f> &input,
                                       std::vector<gmath::vec2f> &output) {
      std::transform(
          input.begin(), input.end(), output.begin(),
          [&position, angle](const auto &v) {
            return gmath::vec2f{gmath::xf{static_cast<float>(
                                    cos(angle) * v.x() - sin(angle) * v.y())},
                                gmath::yf{static_cast<float>(
                                    sin(angle) * v.x() + cos(angle) * v.y())}} +
                   position;
          });
    };
    std::visit(overload{[](circle &arg) {},
                        [doUpdate](polygon &arg) {
                          doUpdate(arg.local_vertices(), arg.world_vertices());
                        },
                        [doUpdate](box &arg) {
                          doUpdate(arg.local_vertices(), arg.world_vertices());
                        }},
               mData);
  }
};

} // namespace physx

#endif // LIB_PHYSX_INCLUDE_PHYSX_UTILS_SHAPE_HPP_
