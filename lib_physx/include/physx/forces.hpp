#ifndef LIB_PHYSX_INCLUDE_PHYSX_FORCES_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_FORCES_HPP_

#include <physx/forces/drag_force.hpp>
#include <physx/forces/friction_force.hpp>
#include <physx/forces/gravity_force.hpp>
#include <physx/forces/spring_force.hpp>

#endif // LIB_PHYSX_INCLUDE_PHYSX_FORCES_HPP_
