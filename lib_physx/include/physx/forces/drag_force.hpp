#ifndef LIB_PHYSX_INCLUDE_PHYSX_FORCES_DRAG_FORCE_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_FORCES_DRAG_FORCE_HPP_

#include <physx/bodies.hpp>

/**
 * @author Nicolò Plebani
 * @file rigid_body.hpp
 * @brief This file contains the drag force implementation.
 */

namespace physx::forces {

constexpr auto
generate_drag_force(const gmath::vec2f &velocity,
                    const float dragCoefficient) noexcept -> gmath::vec2f {

  auto magnitudeSquared{velocity.magnitude_squared()};

  // Guard!
  if (magnitudeSquared <= 0.0f)
    return {};

  // calculates the drag direction (inverse of velocity unit vector)
  auto dragDirection{velocity.normalized() * -1.0f};

  // calculate the drag magnitude (k * |v|^2)
  auto dragMagnitude{dragCoefficient * magnitudeSquared};

  // generate the final drag force with direction and magnitude
  return dragDirection * dragMagnitude;
}

constexpr auto
generate_drag_force(const rigid_body &body,
                    const float dragCoefficient) noexcept -> gmath::vec2f {
  return generate_drag_force(body.velocity(), dragCoefficient);
}

constexpr auto
generate_drag_force(const rigid_body &body, const float fluidDensity,
                    const float dragCoefficient,
                    const float crosssectionalArea) noexcept -> gmath::vec2f {
  return generate_drag_force(body, 0.5f * fluidDensity * dragCoefficient *
                                       crosssectionalArea);
}

} // namespace physx::forces

#endif // LIB_PHYSX_INCLUDE_PHYSX_DRAG_FORCE_HPP_
