#ifndef LIB_PHYSX_INCLUDE_PHYSX_FORCES_FRICTION_FORCE_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_FORCES_FRICTION_FORCE_HPP_

#include <physx/bodies/rigid_body.hpp>

namespace physx::forces {

constexpr auto generate_friction_force(const rigid_body &body,
                                       const float frictionCoefficient) noexcept
    -> gmath::vec2f {
  return body.velocity().normalized() * -1.0f * frictionCoefficient;
}

constexpr auto
generate_friction_force(const rigid_body &body, const float frictionCoefficient,
                        const float normalForce) noexcept -> gmath::vec2f {
  return generate_friction_force(body, frictionCoefficient * normalForce);
}

} // namespace physx::forces

#endif // LIB_PHYSX_INCLUDE_PHYSX_FORCES_FRICTION_FORCE_HPP_
