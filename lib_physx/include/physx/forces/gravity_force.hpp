#ifndef LIB_PHYSX_INCLUDE_PHYSX_FORCES_GRAVITY_FORCE_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_FORCES_GRAVITY_FORCE_HPP_

#include <physx/bodies/rigid_body.hpp>

namespace physx::forces {

constexpr auto
generate_gravitational_force(const rigid_body &a, const rigid_body &b,
                             const float gravitationalCoefficient =
                                 0.00000000006674f) noexcept -> gmath::vec2f {
  auto d{a.position() - b.position()};
  auto distanceSquared{d.magnitude_squared()};
  auto attractionDirection{d.normalized()};
  auto attractionMagnitude{gravitationalCoefficient * (a.mass() * b.mass()) /
                           distanceSquared};
  return attractionDirection * attractionMagnitude;
}

} // namespace physx::forces

#endif // LIB_PHYSX_INCLUDE_PHYSX_FORCES_GRAVITY_FORCE_HPP_
