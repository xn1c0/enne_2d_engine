#ifndef LIB_PHYSX_INCLUDE_PHYSX_FORCES_SPRING_FORCE_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_FORCES_SPRING_FORCE_HPP_

#include <physx/bodies/rigid_body.hpp>

namespace physx::forces {

constexpr auto
generate_spring_force(const rigid_body &body, const vec2f &anchor,
                      const float restLength,
                      const float springCoefficient) noexcept -> vec2f {

  // Calculate the distance between the anchor and the object
  const auto d{body.position() - anchor};

  // Find the spring displacement considering the rest lenght
  auto displacement{d.magnitude() - restLength};

  // Calculate the direction and the magnitude of the spring force
  auto springDirection{d.normalized()};
  auto springMagnitude{-springCoefficient * displacement};

  return springDirection * springMagnitude;
}

} // namespace physx::forces

#endif // LIB_PHYSX_INCLUDE_PHYSX_FORCES_SPRING_FORCE_HPP_
