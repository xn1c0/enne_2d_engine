#ifndef LIB_PHYSX_INCLUDE_PHYSX_PHYSX_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_PHYSX_HPP_

/**
 * @author Nicolò Plebani
 * @file physx.hpp
 */

#include <physx/bodies.hpp>
#include <physx/forces.hpp>

#endif // LIB_PHYSX_INCLUDE_PHYSX_PHYSX_HPP_
