#ifndef LIB_PHYSX_INCLUDE_PHYSX_UTILS_PRIMITIVES_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_UTILS_PRIMITIVES_HPP_

#include <gmath/vec2.hpp>
#include <st/st.hpp>

/**
 * @author Nicolò Plebani
 * @file primitives.hpp
 */

namespace physx {

template <typename T> using maybe = std::optional<T>;

inline constexpr auto nothing = std::nullopt;

using width =
    st::type<float, struct width_tag, st::traits::arithmetic::complete>;

using height =
    st::type<float, struct height_tag, st::traits::arithmetic::complete>;

using radius =
    st::type<float, struct radius_tag, st::traits::arithmetic::complete>;

using mass = st::type<float, struct mass_tag, st::traits::arithmetic::complete>;

using elasticity =
    st::type<float, struct elasticity_tag, st::traits::arithmetic::complete>;

} // namespace physx

#endif // LIB_PHYSX_INCLUDE_PHYSX_UTILS_PRIMITIVES_HPP_
