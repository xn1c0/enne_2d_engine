#ifndef LIB_PHYSX_INCLUDE_PHYSX_UTILS_TRAITS_HPP_
#define LIB_PHYSX_INCLUDE_PHYSX_UTILS_TRAITS_HPP_

/**
 * @author Nicolò Plebani
 * @file traits.hpp
 */

namespace physx {

template <class... Ts> struct overload : Ts... {
  using Ts::operator()...;
};

} // namespace physx

#endif // LIB_PHYSX_INCLUDE_PHYSX_UTILS_TRAITS_HPP_
