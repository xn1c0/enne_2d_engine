#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/events/event.hpp"
#include "sdlpp/video/colour.hpp"
#include <sdlpp/sdlpp.hpp>
#include <vector>

auto main() -> int {

  std::vector<sdlpp::colour> colours = {
      sdlpp::colours::red, sdlpp::colours::green, sdlpp::colours::blue};

  std::optional<sdlpp::pointi> mousePos;

  sdlpp::sdl sdl{sdlpp::init_flags::everithing};
  sdlpp::window window{"sample window", sdlpp::window::position_centered,
                       sdlpp::rsi{sdlpp::wi{800}, sdlpp::hi{600}},
                       sdlpp::window_flags::resizable};
  sdlpp::renderer renderer{window, -1, sdlpp::renderer_flags::accelerated};

  window.show();

  int index = 0;
  bool running = true;
  while (running) {
    sdlpp::event_handler event;
    while (event.poll()) {
      if (event.is(sdlpp::event_type::quit)) {
        running = false;
      } else if (event.is(sdlpp::event_type::key_down)) {
        const auto &keyboardEvent = event.get<sdlpp::keyboard_event>();
        if (keyboardEvent.key() == sdlpp::key_codes::n) {
          index = (index + 1u) % colours.size();
        }
      } else if (const auto *buttonEvent =
                     event.try_get<sdlpp::mouse_button_event>()) {
        if (buttonEvent->pressed()) {
          mousePos = buttonEvent->position();
        }
      }
    }

    renderer.set_draw_colour(colours.at(index));
    renderer.clear();

    if (mousePos) {
      renderer.set_draw_colour(colours.at((index + 1u) % colours.size()));

      sdlpp::recti rect{sdlpp::pointi{mousePos.value()},
                        sdlpp::rsi{sdlpp::wi{6}, sdlpp::hi{6}}};
      rect.x() -= 3;
      rect.y() -= 3;

      renderer.draw_fill_rect(rect);
    }

    renderer.present();
  }

  window.hide();

  return 0;
}
