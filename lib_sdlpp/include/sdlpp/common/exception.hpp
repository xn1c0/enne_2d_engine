#ifndef LIB_SDLPP_INCLUDE_SDLPP_EXCEPTION_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_EXCEPTION_HPP_

#include <stdexcept>
#include <string>

/**
 * @author Nicolò Plebani
 * @file exception.hpp
 */

namespace sdlpp {

/**
 * @brief Exception class.
 * Manages SDL2 exceptions.
 */
class exception : public std::runtime_error {
private:
  /// Member data
  std::string
      function_; ///< The name of the SDL function which caused the exception.
  std::string error_; ///< The SDL error message.

public:
  /// Getter and setters.
  std::string function() const noexcept;
  std::string error() const noexcept;

private:
  /**
   * @brief Generates the exception error message.
   */
  static std::string make_what(const char *sdl_function, const char *sdl_error);

public:
  /**
   * @brief Secondary constructor
   * @param function The name of SDL where the error took place.
   */
  explicit exception(const char *function);
};

} // namespace sdlpp

#endif // SDLPP_INCLUDE_SDLPP_EXCEPTION_HPP_
