#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_LITERALS_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_LITERALS_HPP_

#include "sdlpp/common/primitives.hpp"

namespace sdlpp::literals {

namespace time {

[[nodiscard]] constexpr auto
operator""_ms(const unsigned long long ms) -> u64ms {
  return u64ms{static_cast<uint64>(ms)};
}

} // namespace time
} // namespace sdlpp::literals

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_LITERALS_HPP_
