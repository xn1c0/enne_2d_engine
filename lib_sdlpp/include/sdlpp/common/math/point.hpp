#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_POINT_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_POINT_HPP_

#include <SDL2/SDL_rect.h>

#include <compare>
#include <format>

#include "sdlpp/common/primitives.hpp"

/**
 * @author Nicolò Plebani
 * @file point.hpp
 */

namespace sdlpp {

/**
 * @brief Operator "three way" for SDL_Point.
 */
constexpr auto operator<=>(const SDL_Point &lhs,
                           const SDL_Point &rhs) -> std::strong_ordering {
  if (lhs.x < rhs.x or (lhs.x == rhs.x and lhs.y < rhs.y))
    return std::strong_ordering::less;
  if (lhs.x > rhs.x or (lhs.x == rhs.x and lhs.y > rhs.y))
    return std::strong_ordering::greater;
  return std::strong_ordering::equivalent;
}

/**
 * @brief Operator "is equal to" for SDL_Point.
 */
constexpr auto operator==(const SDL_Point &lhs, const SDL_Point &rhs) -> bool {
  return std::is_eq(lhs <=> rhs);
}

/**
 * @brief Operator "three way" for SDL_FPoint.
 */
constexpr auto operator<=>(const SDL_FPoint &lhs,
                           const SDL_FPoint &rhs) -> std::partial_ordering {
  if (lhs.x < rhs.x or (lhs.x == rhs.x and lhs.y < rhs.y))
    return std::partial_ordering::less;
  if (lhs.x > rhs.x or (lhs.x == rhs.x and lhs.y > rhs.y))
    return std::partial_ordering::greater;
  return std::partial_ordering::equivalent;
}

/**
 * @brief Operator "is equal to" for SDL_FPoint.
 */
constexpr auto operator==(const SDL_FPoint &lhs,
                          const SDL_FPoint &rhs) -> bool {
  return std::is_eq(lhs <=> rhs);
}

template <typename T> class point;

using pointi = point<int>;   ///< Wrapper for SDL_Point.
using pointf = point<float>; ///< Wrapper for SDL_FPoint.

/**
 * @brief Templated class point.
 * Serves as a wrapper for SDL point_t, and it provides useful functionality.
 */
template <typename T> class point {
public:
  inline constexpr static bool integral = std::is_integral_v<T>;
  using value_t = std::conditional_t<integral, int, float>;
  using point_t = std::conditional_t<integral, SDL_Point, SDL_FPoint>;
  using x_t = std::conditional_t<integral, xi, xf>;
  using y_t = std::conditional_t<integral, yi, yf>;

private:
  point_t mPoint; ///< The managed SDL point_t.

public:
  [[nodiscard]] constexpr auto x() const & -> const value_t & {
    return mPoint.x;
  }
  [[nodiscard]] constexpr auto x() & -> value_t & { return mPoint.x; }
  [[nodiscard]] constexpr auto x() && -> value_t && {
    return std::move(mPoint.x);
  }
  [[nodiscard]] constexpr auto y() const & -> const value_t & {
    return mPoint.y;
  }
  [[nodiscard]] constexpr auto y() & -> value_t & { return mPoint.y; }
  [[nodiscard]] constexpr auto y() && -> value_t && {
    return std::move(mPoint.y);
  }

  [[nodiscard]] constexpr auto operator()() const noexcept -> const point_t * {
    return &mPoint;
  }

  [[nodiscard]] constexpr auto operator()() noexcept -> point_t * {
    return &mPoint;
  }

  /**
   * @brief Default constrcutor.
   * @note trivial and non-throwing.
   */
  constexpr point() = default;

  /**
   * @brief Secondary constrcutor.
   * @param x The cartesian coordinate x.
   * @param y The cartesian coordinate y.
   */
  constexpr point(const x_t &x, const y_t &y) noexcept
      : mPoint{x.value(), y.value()} {}

  /**
   * @brief Secondary constructor.
   * Constructs the point from another type of point. A call to this
   * constructor will fail to compile if U is not convertible to T.
   * @param other The point to convert.
   */
  template <typename U>
  constexpr explicit point(const point<U> &other) noexcept
      : mPoint{static_cast<value_t>(other.x()),
               static_cast<value_t>(other.y())} {}

  /**
   * @brief Operator "three way".
   */
  [[nodiscard]] constexpr auto
  operator<=>(const point &rhs) const noexcept = default;

  /**
   * @brief Unary operator plus (promotion).
   * @return This point.
   */
  [[nodiscard]] constexpr auto operator+() const noexcept -> point {
    return *this;
  }

  /**
   * @brief Unary operator minus (negation).
   * @return This point with opposite values.
   */
  [[nodiscard]] constexpr auto operator-() const noexcept -> point {
    return {x_t{-x()}, y_t(-y())};
  }

  /**
   * @brief Addition operator.
   * @param rhs The right hand side of the operator.
   * @return The point resulted from the addition.
   */
  [[nodiscard]] constexpr auto
  operator+(const point &rhs) const noexcept -> point {
    return {x_t{x() + rhs.x()}, y_t{y() + rhs.y()}};
  }

  /**
   * @brief Addition and assignment operator.
   * @param rhs The right hand side of the operator.
   * @return The reference of lhs.
   */
  constexpr auto operator+=(const point &rhs) noexcept -> point & {
    x() += rhs.x();
    y() += rhs.y();
    return *this;
  }

  /**
   * @brief Subtraction operator.
   * @param rhs The right hand side of the operator.
   * @return The point resulted from the substraction.
   */
  [[nodiscard]] constexpr auto
  operator-(const point &rhs) const noexcept -> point {
    return {x_t{x() - rhs.x()}, y_t{y() - rhs.y()}};
  }

  /**
   * @brief Subtracttion and assignment operator.
   * @param rhs The right hand side of the operator.
   * @return The reference to self.
   */
  constexpr auto operator-=(const point &rhs) noexcept -> point & {
    x() -= rhs.x();
    y() -= rhs.y();
    return *this;
  }

  /**
   * @brief Multiplication operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The point resulted from the multiplication.
   */
  [[nodiscard]] constexpr auto
  operator*(const value_t rhs) const noexcept -> point {
    return {x_t{x() * rhs}, y_t{y() * rhs}};
  }

  /**
   * @brief Multiplication operator by a scalar.
   * @param lhs The left hand side of the operator.
   * @param rhs The right hand side of the operator.
   * @return The point resulted from the multiplication.
   */
  [[nodiscard]] friend constexpr auto
  operator*(const value_t lhs, const point &rhs) noexcept -> point {
    return rhs * lhs;
  }

  /**
   * @brief Multiplication and assignment operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The reference to self.
   */
  constexpr auto operator*=(const value_t rhs) noexcept -> point & {
    x() *= rhs;
    y() *= rhs;
    return *this;
  }

  /**
   * @brief Division operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The point resulted from the division.
   */
  [[nodiscard]] constexpr auto
  operator/(const value_t rhs) const noexcept -> point {
    return {x_t{x() / rhs}, y_t{y() / rhs}};
  }

  /**
   * @brief Division and assignment operator by a scalar.
   * @param rhs The right hand side of the operator.
   * @return The reference to self.
   */
  constexpr auto operator/=(const value_t rhs) noexcept -> point & {
    x() /= rhs;
    y() /= rhs;
    return *this;
  }
};

/**
 * @brief Formats the point into a readable std::string.
 * @tparam T The value type of the point.
 * @param point The point to be represented in a std::string.
 * @return The std::string representing the point.
 */
template <typename T>
[[nodiscard]] constexpr std::string to_string(const point<T> &point) noexcept {
  return std::format("[x: {}, y: {}]", point.x(), point.y());
}

/**
 * @brief Ostream operator.
 * @tparam T The value type of the point.
 * @param os The output stream.
 * @param point The point to be printed in the ostream.
 * @return The reference to os.
 */
template <typename T>
std::ostream &operator<<(std::ostream &os, const point<T> &point) noexcept {
  os << to_string(point);
  return os;
}

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_POINT_HPP_
