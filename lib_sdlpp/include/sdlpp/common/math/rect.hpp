#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_RECT_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_RECT_HPP_

#include <SDL2/SDL_rect.h>

#include <compare>
#include <format>
#include <ostream>
#include <utility>

#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"

/**
 * @author Nicolò Plebani
 * @file rect.hpp
 */

namespace sdlpp {

/**
 * @brief Operator "three way" for SDL_Rect.
 */
constexpr auto operator<=>(const SDL_Rect &lhs,
                           const SDL_Rect &rhs) -> std::strong_ordering {
  if (lhs.x < rhs.x or (lhs.x == rhs.x and lhs.y < rhs.y) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w < rhs.w) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w == rhs.w and lhs.h < rhs.h))
    return std::strong_ordering::less;
  if (lhs.x > rhs.x or (lhs.x == rhs.x and lhs.y > rhs.y) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w > rhs.w) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w == rhs.w and lhs.h > rhs.h))
    return std::strong_ordering::greater;
  return std::strong_ordering::equivalent;
}

/**
 * @brief Operator "is equal to" for SDL_Rect.
 */
constexpr auto operator==(const SDL_Rect &lhs, const SDL_Rect &rhs) -> bool {
  return std::is_eq(lhs <=> rhs);
}

/**
 * @brief Operator "three way" for SDL_FRect.
 */
constexpr auto operator<=>(const SDL_FRect &lhs,
                           const SDL_FRect &rhs) -> std::partial_ordering {
  if (lhs.x < rhs.x or (lhs.x == rhs.x and lhs.y < rhs.y) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w < rhs.w) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w == rhs.w and lhs.h < rhs.h))
    return std::partial_ordering::less;
  if (lhs.x > rhs.x or (lhs.x == rhs.x and lhs.y > rhs.y) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w > rhs.w) or
      (lhs.x == rhs.x and lhs.y == rhs.y and lhs.w == rhs.w and lhs.h > rhs.h))
    return std::partial_ordering::greater;
  return std::partial_ordering::equivalent;
}

/**
 * @brief Operator "is equal to" for SDL_FRect.
 */
constexpr auto operator==(const SDL_FRect &lhs, const SDL_FRect &rhs) -> bool {
  return std::is_eq(lhs <=> rhs);
}

template <typename T> class rect;

using recti = rect<int>;   ///< Integral rect (SDL_Rect).
using rectf = rect<float>; ///< Floating-point rect (SDL_FRect).

/**
 * @brief Templated class rect.
 * Serves as a wrapper for SDL rect_t, and it provides useful functionality.
 */
template <typename T> class rect {
public:
  inline constexpr static bool integral = std::is_integral_v<T>;
  using value_t = std::conditional_t<integral, int, float>;
  using rect_t = std::conditional_t<integral, SDL_Rect, SDL_FRect>;
  using point_t = std::conditional_t<integral, pointi, pointf>;
  using x_t = point_t::x_t;
  using y_t = point_t::y_t;
  using rect_size_t = std::conditional_t<integral, rsi, rsf>;
  using width_t = rect_size_t::width_t;
  using height_t = rect_size_t::height_t;

private:
  rect_t mRect; ///< The managed SDL rect_t.

public:
  [[nodiscard]] constexpr auto x() const & -> const value_t & {
    return mRect.x;
  }

  [[nodiscard]] constexpr auto x() & -> value_t & { return mRect.x; }

  [[nodiscard]] constexpr auto x() && -> value_t && {
    return std::move(mRect.x);
  }

  [[nodiscard]] constexpr auto y() const & -> const value_t & {
    return mRect.y;
  }
  [[nodiscard]] constexpr auto y() & -> value_t & { return mRect.y; }

  [[nodiscard]] constexpr auto y() && -> value_t && {
    return std::move(mRect.y);
  }

  [[nodiscard]] constexpr auto w() const & -> const value_t & {
    return mRect.w;
  }

  [[nodiscard]] constexpr auto w() & -> value_t & { return mRect.w; }

  [[nodiscard]] constexpr auto w() && -> value_t && {
    return std::move(mRect.w);
  }

  [[nodiscard]] constexpr auto h() const & -> const value_t & {
    return mRect.h;
  }

  [[nodiscard]] constexpr auto h() & -> value_t & { return mRect.h; }

  [[nodiscard]] constexpr auto h() && -> value_t && {
    return std::move(mRect.h);
  }

  [[nodiscard]] constexpr auto
  operator()(void) const noexcept -> const rect_t * {
    return &mRect;
  }

  [[nodiscard]] constexpr auto operator()(void) noexcept -> rect_t * {
    return &mRect;
  }

  /**
   * @brief Default constructor.
   */
  constexpr rect() = default;

  /**
   * @brief Secondary constructor.
   * Construct from existing SDL_Rect.
   * @param rect The SDL_Rect.
   */
  constexpr rect(const rect_t &rect) noexcept : mRect{rect} {}

  /**
   * @brief Secondary constructor.
   * Construct from specified arguments.
   * @param position The top left corner of the rectangle.
   * @param size The size of the rectangle.
   * height).
   */
  constexpr rect(const point_t &position, const rect_size_t &size) noexcept
      : mRect{position.x(), position.y(), size.w(), size.h()} {}

  /**
   * @brief Secondary constructor.
   * Constructs the rectangle from another type of rectangle. A call to this
   * constructor will fail to compile if U is not convertible to T.
   * @param other The other rectangle to convert.
   */
  template <typename U>
  constexpr explicit rect(const rect<U> &other) noexcept
      : mRect{static_cast<value_t>(other.x()), static_cast<value_t>(other.y()),
              static_cast<value_t>(other.w()),
              static_cast<value_t>(other.h())} {}

  /**
   * @brief Operator "three way".
   */
  [[nodiscard]] constexpr auto
  operator<=>(const rect &rhs) const noexcept = default;

  /**
   * @brief Gets the size of the rectangle.
   * @return The rect_size_t representing the dimension of the rectangle.
   */
  [[nodiscard]] constexpr auto size() const noexcept -> rect_size_t {
    return {width_t{w()}, height_t{h()}};
  }

  /**
   * @brief Gets the centeroid of the rectangle.
   * @return The point of the centroid.
   */
  [[nodiscard]] constexpr auto centroid() const noexcept -> pointf {
    xf cx{x() + w() / float{2}};
    yf cy{y() + h() / float{2}};
    return {cx, cy};
  }

  /**
   * @brief Gets the top left corner of the rectangle.
   * @return The point of the top left corner.
   */
  [[nodiscard]] constexpr auto top_left_corner() const noexcept -> point_t {
    x_t cx{x()};
    y_t cy{y()};
    return {cx, cy};
  }

  /**
   * @brief Gets the top right corner of the rectangle.
   * @return The point of the top right corner.
   */
  [[nodiscard]] constexpr auto top_right_corner() const noexcept -> point_t {
    x_t cx{x() + w()};
    y_t cy{y()};
    return {cx, cy};
  }

  /**
   * @brief Gets the bottom left corner of the rectangle.
   * @return The point of the bottom left corner.
   */
  [[nodiscard]] constexpr auto bottom_left_corner() const noexcept -> point_t {
    x_t cx{x()};
    y_t cy{y() + h()};
    return {cx, cy};
  }

  /**
   * @brief Gets the bottom right corner of the rectangle.
   * @return The point of the bottom right corner.
   */
  [[nodiscard]] constexpr auto bottom_right_corner() const noexcept -> point_t {
    x_t cx{x() + w()};
    y_t cy{y() + h()};
    return {cx, cy};
  }

  /**
   * @brief Checks if a point is inside the area of the rectangle
   * @param point The point to check.
   * @return True of the point is inside the rectangle.
   * @note SDL_Rect can't be rotated so the formula works.
   */
  [[nodiscard]] constexpr auto
  contains(const point_t &point) const noexcept -> bool {
    return point.x() >= x() and point.y() >= y() and
           point.x() <= bottom_right_corner().x() and
           point.y() <= bottom_left_corner().y();
  }

  /**
   * @brief Computes the interesction with another rectangle.
   * @param other The other rectangle.
   * @return True if the two rectangle intersect.
   * @note SDL_Rect can't be rotated so the formula works.
   */
  [[nodiscard]] constexpr auto
  intersects(const rect &other) const noexcept -> bool {
    auto isInRange = [](value_t value, value_t min, value_t max) -> bool {
      return (value >= min) and (value <= max);
    };
    bool xOverlap = isInRange(x(), other.x(), other.x() + other.w()) or
                    isInRange(other.x(), x(), x() + w());

    bool yOverlap = isInRange(y(), other.y(), other.y() + other.h()) or
                    isInRange(other.y(), y(), y() + h());
    return xOverlap and yOverlap;
  }

  /**
   * @brief Computes the interesction with another rectangle.
   * @param other The other rectangle.
   * @return The rectangle representing the intersection or nothing.
   */
  [[nodiscard]] constexpr auto
  intersection(const rect &other) const noexcept -> maybe<rect> {
    if (!intersects(other))
      return nothing;
    point_t topLeft{x_t{std::max(x(), other.x())},
                    y_t{std::max(y(), other.y())}};
    point_t bottomRight{x_t{std::min(bottom_right_corner().x(),
                                     other.bottom_right_corner().x())},
                        y_t{std::min(bottom_right_corner().y(),
                                     other.bottom_right_corner().y())}};
    return create_from_corners(topLeft, bottomRight);
  }
};

/**
 * @brief Creates a rectangle by the given center.
 * @tparam T The value type of the rectangle.
 * @param center The point, center of the rectangle.
 * @param size The size of the rectangle (Povalue_t indicating width and
 * height).
 * @return The created rectangle.
 */
template <typename T>
[[nodiscard]] constexpr auto
create_rect_from_center(const point<T> &center,
                        const rect_size<T> &size) noexcept -> rect<T> {
  return rect{center.x() - size.w() / T{2}, center.y() - size.h() / T{2},
              size.w(), size.h()};
}

/**
 * @brief Creates a rectangle by the given corners.
 * @tparam T The value type of the rectangle.
 * @param topLeft The point, top left corner of the rectangle.
 * @param bottomRight The point, bottom right corner of the rectangle.
 * @return The created rectangle.
 */
template <typename T>
[[nodiscard]] constexpr auto
create_from_corners(const point<T> &topLeft,
                    const point<T> &bottomRight) noexcept -> rect<T> {
  typename rect<T>::width_t w{bottomRight.x() - topLeft.y()};
  typename rect<T>::height_t h{bottomRight.y() - topLeft.y()};
  typename rect<T>::rect_size_t size{w, h};
  return rect<T>{topLeft, size};
}

/**
 * @brief Formats the rect into a readable std::string.
 * @tparam T The value type of the rectangle.
 * @param rect The rect to be represented in a std::string.gg
 * @return The std::string representing the rect.
 */
template <typename T>
[[nodiscard]] constexpr auto to_string(const rect<T> &rect) noexcept {
  return std::format("[x: {}, y: {}, w: {}, h: {}]", rect.x(), rect.y(),
                     rect.w(), rect.h());
}

/**
 * @brief Ostream operator.
 * @tparam T The value type of the rectangle.
 * @param os The output stream.
 * @param rect The rect to be printed in the ostream.
 * @return The reference to os.
 */
template <typename T>
auto operator<<(std::ostream &os,
                const rect<T> &rect) noexcept -> std::ostream & {
  os << to_string(rect);
  return os;
}

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_RECT_HPP_
