#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_RECT_SIZE_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_RECT_SIZE_HPP_

#include "sdlpp/common/primitives.hpp"

/**
 * @author Nicolò Plebani
 * @file rect_size.hpp
 */

namespace sdlpp {

template <typename T> class rect_size;

using rsi = rect_size<int>;   ///< Integer size.
using rsf = rect_size<float>; ///< Floating-point size.

/**
 * @brief Templated class rect_size.
 * Serves as a strong type.
 */
template <typename T> class rect_size {
public:
  inline constexpr static bool integral = std::is_integral_v<T>;
  using value_t = std::conditional_t<integral, int, float>;
  using width_t = std::conditional_t<integral, wi, wf>;
  using height_t = std::conditional_t<integral, hi, hf>;

private:
  value_t mWidth;
  value_t mHeight;

public:
  [[nodiscard]] constexpr auto w() const & -> const value_t & { return mWidth; }
  [[nodiscard]] constexpr auto w() & -> value_t & { return mWidth; }
  [[nodiscard]] constexpr auto w() && -> value_t && {
    return std::move(mWidth);
  }
  [[nodiscard]] constexpr auto h() const & -> const value_t & {
    return mHeight;
  }
  [[nodiscard]] constexpr auto h() & -> value_t & { return mHeight; }
  [[nodiscard]] constexpr auto h() && -> value_t && {
    return std::move(mHeight);
  }

  /**
   * @brief Default constrcutor.
   * @note trivial and non-throwing.
   */
  constexpr rect_size() = default;

  /**
   * @brief Secondary constrcutor.
   * @param width The width of the rect.
   * @param height The height of the rect.
   */
  constexpr rect_size(const width_t &width, const height_t &height) noexcept
      : mWidth{width.value()}, mHeight{height.value()} {}

  /**
   * @brief Operator "three way".
   */
  [[nodiscard]] constexpr auto
  operator<=>(const rect_size &rhs) const noexcept = default;

  /**
   * @brief Calculates the area of the rect.
   * @return The computed area.
   */
  [[nodiscard]] constexpr auto area() const noexcept -> value_t {
    return w() * h();
  }

  /**
   * @brief Checks if the rect has area.
   * @return True if rect has area else false.
   */
  [[nodiscard]] constexpr auto has_area() const noexcept -> bool {
    return area() != 0;
  }
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_MATH_RECT_SIZE_HPP_
