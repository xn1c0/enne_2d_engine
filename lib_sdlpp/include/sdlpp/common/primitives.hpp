#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_PRIMITIVES_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_PRIMITIVES_HPP_

#include "st/traits.hpp"
#include <SDL2/SDL_stdinc.h>

#include <chrono>
#include <cstddef>
#include <optional>
#include <ratio>
#include <st/st.hpp>

/**
 * @author Nicolò Plebani
 * @file primitives.hpp
 */

namespace sdlpp {

using usize = std::size_t;
using uint = unsigned int;
using ulonglong = unsigned long long int;

using uint8 = Uint8;
using uint16 = Uint16;
using uint32 = Uint32;
using uint64 = Uint64;

using int8 = Sint8;
using int16 = Sint16;
using int32 = Sint32;
using int64 = Sint64;

using unicode_t = uint16;
using unicode32_t = uint32;

template <typename T> using seconds = std::chrono::duration<T>;
template <typename T> using millis = std::chrono::duration<T, std::milli>;
template <typename T> using minutes = std::chrono::duration<T, std::ratio<60>>;

using u16ms = millis<uint16>;
using u32ms = millis<uint32>;
using u64ms = millis<uint64>;

template <typename T> using maybe = std::optional<T>;

inline constexpr auto nothing = std::nullopt;

using xi = st::type<int, struct xi_tag, st::traits::arithmetic::complete>;
using yi = st::type<int, struct yi_tag, st::traits::arithmetic::complete>;
using wi = st::type<int, struct ei_tag, st::traits::arithmetic::complete>;
using hi = st::type<int, struct hi_tag, st::traits::arithmetic::complete>;

using xf = st::type<float, struct xf_tag, st::traits::arithmetic::complete>;
using yf = st::type<float, struct yf_tag, st::traits::arithmetic::complete>;
using wf = st::type<float, struct wf_tag, st::traits::arithmetic::complete>;
using hf = st::type<float, struct hf_tag, st::traits::arithmetic::complete>;

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_PRIMITIVES_HPP_
