#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_SDL_OBJECT_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_SDL_OBJECT_HPP_

#include "sdlpp/common/utils.hpp"

namespace sdlpp {

/**
 * @brief Class sdl_object.
 */
class sdl_object : non_copyable {
protected:
  sdl_object() = default;
  ~sdl_object() = default;

  /**
   * @brief Queries the status of SDL library.
   * @param result The result of an SDL function call.
   * @param function The name of the SDL function called.
   * @throws sdlpp::Exception
   */
  void query_status(int result, const char *function) const;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_SDL_OBJECT_HPP_
