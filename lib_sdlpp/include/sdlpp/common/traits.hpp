#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_TRAITS_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_TRAITS_HPP_

/**
 * @author Nicolò Plebani
 * @file traits.hpp
 */

#include <iterator>
#include <type_traits>

namespace sdlpp {

/**
 * @brief Checks if type T value is a number.
 * @tparam T The type of the value.
 * @note True if type T value is a number.
 */
template <typename T>
inline constexpr bool is_number =
    not std::is_same_v<T, bool> and
    (std::is_integral_v<T> or std::is_floating_point_v<T>);

/**
 * @brief Specify the requirment on template arguments.
 * @tparam T The type that must be pointer.
 */
template <typename T>
concept is_pointer = std::is_pointer_v<T>;

/**
 * @brief Specify the requirment on template arguments.
 * @tparam T The type that must be enumeration.
 */
template <typename T>
concept is_enum = std::is_enum_v<T>;

/**
 * @brief Specify the requirment on template arguments.
 * @tparam T The type that must be implicitly convertible to.
 * @tparam ...Args The types to which T must be convertible to.
 * @note T must be implicitly convertible to all the types specified.
 */
template <typename T, typename... Args>
concept is_implicitly_convertible = (... && std::is_convertible_v<T, Args>);

/**
 * @brief Specify the requirment on template arguments.
 * @tparam T The type that must be numeric.
 */
template <typename T>
concept is_arithmetic = is_number<T>;

/**
 * @brief Specify the requirment on template arguments.
 * @tparam T The type that must be numeric for SDL.
 * @note SDL uses two types for drawing operations, int and float.
 */
template <typename T>
concept is_arithmetic_for_sdl = is_implicitly_convertible<T, int, float>;

/**
 * @brief Specify the requirment on template arguments.
 * @tparam T The type that must be conteiner.
 */
template <typename T>
concept is_container = requires(T t) {
  { std::size(t) } -> std::same_as<std::size_t>;
  { std::begin(t) } -> std::same_as<typename T::iterator>;
  { std::end(t) } -> std::same_as<typename T::iterator>;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_TRAITS_HPP_
