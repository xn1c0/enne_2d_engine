#ifndef LIB_SDLPP_INCLUDE_SDLPP_COMMON_UTILS_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_COMMON_UTILS_HPP_

#include <cmath>
#include <type_traits>

#include "sdlpp/common/traits.hpp"

/**
 * @author Nicolò Plebani
 * @file utils.hpp
 */

namespace sdlpp {

/**
 * @brief Converts an enum value to its underlying integral value.
 * @tparam E The enum type of the value.
 * @param value The value from which will be extracted the underlying type.
 * @return The underlying type.
 */
template <typename Enum>
  requires is_enum<Enum>
[[nodiscard]] constexpr std::underlying_type_t<Enum>
to_underlying(const Enum value) noexcept {
  return static_cast<std::underlying_type_t<Enum>>(value);
}

/**
 * @brief Gets the size of a container as a plain integer.
 * @param container The container type to compute.
 * @return The integral value, size of the container.
 */
template <typename Container>
  requires is_container<Container>
[[nodiscard]] constexpr int
container_size(const Container &container) noexcept {
  return static_cast<int>(container.size());
}

/**
 * @brief Checks if two numbers are approximately equals.
 * @tparam T Type of the numbers to compare.
 * @param a The first number to compare.
 * @param b The second number to compare.
 * @param epsilon The tolerance value.
 */
template <typename T>
  requires is_arithmetic<T>
constexpr bool is_equal_approx(
    T a, T b,
    T epsilon = static_cast<T>(std::numeric_limits<T>::epsilon())) noexcept {
  // Check if the numbers are really close
  double diff{std::abs(a - b)};
  if (diff <= epsilon)
    return true;
  // Otherwise fall back to Knuth's algorithm
  return (diff <= (std::max(std::abs(a), std::abs(b)) * epsilon));
}

/**
 * @brief Non copyable class
 * Ensures classes derived from class non_copyable cannot be copied.
 */
class non_copyable {
protected:
  non_copyable() = default;
  ~non_copyable() = default;
  non_copyable(const non_copyable &) = delete;
  non_copyable &operator=(const non_copyable &) = delete;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_COMMON_UTILS_HPP_
