#ifndef EVENT_H_
#define EVENT_H_

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_timer.h>

#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/utils.hpp"

namespace sdlpp {

enum class event_type : uint32 {
  first_event = SDL_FIRSTEVENT,
  last_event = SDL_LASTEVENT,
  poll_sentinel = SDL_POLLSENTINEL,
  quit = SDL_QUIT,
  app_terminating = SDL_APP_TERMINATING,
  app_low_memory = SDL_APP_LOWMEMORY,
  app_will_enter_background = SDL_APP_WILLENTERBACKGROUND,
  app_did_enter_background = SDL_APP_DIDENTERBACKGROUND,
  app_will_enter_foreground = SDL_APP_WILLENTERFOREGROUND,
  app_did_enter_foreground = SDL_APP_DIDENTERFOREGROUND,
  locale_changed = SDL_LOCALECHANGED,
  display = SDL_DISPLAYEVENT,
  window = SDL_WINDOWEVENT,
  system = SDL_SYSWMEVENT,
  key_down = SDL_KEYDOWN,
  key_up = SDL_KEYUP,
  text_editing = SDL_TEXTEDITING,
  text_editing_ext = SDL_TEXTEDITING_EXT,
  text_input = SDL_TEXTINPUT,
  keymap_changed = SDL_KEYMAPCHANGED,
  mouse_motion = SDL_MOUSEMOTION,
  mouse_button_down = SDL_MOUSEBUTTONDOWN,
  mouse_button_up = SDL_MOUSEBUTTONUP,
  mouse_wheel = SDL_MOUSEWHEEL,
  joy_axis_motion = SDL_JOYAXISMOTION,
  joy_ball_motion = SDL_JOYBALLMOTION,
  joy_hat_motion = SDL_JOYHATMOTION,
  joy_button_down = SDL_JOYBUTTONDOWN,
  joy_button_up = SDL_JOYBUTTONUP,
  joy_device_added = SDL_JOYDEVICEADDED,
  joy_device_removed = SDL_JOYDEVICEREMOVED,
  joy_battery_updated = SDL_JOYBATTERYUPDATED,
  controller_axis_motion = SDL_CONTROLLERAXISMOTION,
  controller_button_down = SDL_CONTROLLERBUTTONDOWN,
  controller_button_up = SDL_CONTROLLERBUTTONUP,
  controller_device_added = SDL_CONTROLLERDEVICEADDED,
  controller_device_removed = SDL_CONTROLLERDEVICEREMOVED,
  controller_device_remapped = SDL_CONTROLLERDEVICEREMAPPED,
  controller_touchpad_down = SDL_CONTROLLERTOUCHPADDOWN,
  controller_touchpad_motion = SDL_CONTROLLERTOUCHPADMOTION,
  controller_touchpad_up = SDL_CONTROLLERTOUCHPADUP,
  controller_sensor_update = SDL_CONTROLLERSENSORUPDATE,
  finger_down = SDL_FINGERDOWN,
  finger_up = SDL_FINGERUP,
  finger_motion = SDL_FINGERMOTION,
  dollar_gesture = SDL_DOLLARGESTURE,
  dollar_record = SDL_DOLLARRECORD,
  multi_gesture = SDL_MULTIGESTURE,
  clipboard_update = SDL_CLIPBOARDUPDATE,
  drop_file = SDL_DROPFILE,
  drop_text = SDL_DROPTEXT,
  drop_begin = SDL_DROPBEGIN,
  drop_complete = SDL_DROPCOMPLETE,
  audio_device_added = SDL_AUDIODEVICEADDED,
  audio_device_removed = SDL_AUDIODEVICEREMOVED,
  sensor_update = SDL_SENSORUPDATE,
  render_targets_reset = SDL_RENDER_TARGETS_RESET,
  render_device_reset = SDL_RENDER_DEVICE_RESET,
  user = SDL_USEREVENT
};

/**
 * @brief Class event.
 * Handles sdl events and provides a simple interface.
 * @tparam T The type of sdl event to manage.
 */
template <typename T> class event {
protected:
  T mEvent; ///< The managed sdl event.

public:
  /**
   * @brief Gets the sdl event.
   */
  [[nodiscard]] auto operator()() const noexcept -> const T & { return mEvent; }

  /**
   * @brief Secondary constructor.
   * Constructs from event_type.
   */
  explicit event(const event_type type) {
    mEvent.type = to_underlying(type);
    mEvent.timestamp = SDL_GetTicks();
  }

  /**
   * @brief Secondary constructor.
   * Constructs from the sdl event.
   */
  explicit event(const T &event) noexcept : mEvent{event} {}

  /**
   * @brief Secondary constructor.
   * Constructs from the sdl event.
   */
  explicit event(T &&event) noexcept : mEvent{std::move(event)} {}

  /**
   * @brief Gets the type of the event.
   * @return The event_type.
   */
  [[nodiscard]] auto type() const noexcept -> event_type {
    return event_type{mEvent.type};
  }

  /**
   * @brief Gets the timestamp (time of the creation of the event).
   * @return The time in form of millis.
   */
  [[nodiscard]] auto timestamp() const -> u32ms {
    return u32ms{mEvent.timestamp};
  }
};

} // namespace sdlpp

#endif // EVENT_H_
