#ifndef HANDLER_H_
#define HANDLER_H_

#include <SDL2/SDL_events.h>

#include "sdlpp/events/event.hpp"
#include "sdlpp/events/misc_event.hpp"
#include "sdlpp/events/mouse_event.hpp"
#include "sdlpp/events/window_event.hpp"

namespace sdlpp {

class event_handler final {
public:
  [[nodiscard]] auto operator()() const noexcept -> const SDL_Event * {
    return &mEvent;
  }

  auto poll() noexcept -> bool {
    SDL_Event event{};
    if (SDL_PollEvent(&event)) {
      store(event);
      return true;
    } else {
      reset_state();
      return false;
    }
  }

  template <typename T> [[nodiscard]] auto is() const noexcept -> bool {
    return std::holds_alternative<T>(mData);
  }

  [[nodiscard]] auto is(const event_type type) const noexcept -> bool {
    return mType == type;
  }

  template <typename T> [[nodiscard]] auto get() -> T & {
    return std::get<T>(mData);
  }

  template <typename T> [[nodiscard]] auto get() const -> const T & {
    return std::get<T>(mData);
  }

  template <typename T> [[nodiscard]] auto try_get() noexcept -> T * {
    return std::get_if<T>(&mData);
  }

  template <typename T>
  [[nodiscard]] auto try_get() const noexcept -> const T * {
    return std::get_if<T>(&mData);
  }

private:
  using data_type =
      std::variant<std::monostate, window_event, quit_event, keyboard_event,
                   mouse_button_event, mouse_motion_event, mouse_wheel_event>;

  SDL_Event mEvent{};
  event_type mType{event_type::last_event};
  data_type mData{};

  void reset_state() {
    mEvent = {};
    mType = event_type::last_event;
    mData.emplace<std::monostate>();
  }

  void store(const SDL_Event &event) noexcept {
    mEvent = event;
    mType = static_cast<event_type>(static_cast<SDL_EventType>(event.type));

    switch (mType) {

    case event_type::quit:
      mData.emplace<quit_event>(event.quit);
      break;

    case event_type::window:
      mData.emplace<window_event>(event.window);
      break;

    case event_type::key_up:
    case event_type::key_down:
      mData.emplace<keyboard_event>(event.key);
      break;

    case sdlpp::event_type::mouse_motion:
      mData.emplace<mouse_motion_event>(event.motion);
      break;

    case sdlpp::event_type::mouse_button_up:
    case sdlpp::event_type::mouse_button_down:
      mData.emplace<mouse_button_event>(event.button);
      break;

    case sdlpp::event_type::mouse_wheel:
      mData.emplace<mouse_wheel_event>(event.wheel);
      break;

    default:
      reset_state();
      break;
    }
  }
};

} // namespace sdlpp

#endif // HANDLER_H_
