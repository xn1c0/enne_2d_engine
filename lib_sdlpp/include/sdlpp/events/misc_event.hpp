#ifndef MISC_EVENT_H_
#define MISC_EVENT_H_

#include <SDL2/SDL_events.h>

#include "sdlpp/events/event.hpp"

#include "sdlpp/input/button_state.hpp"
#include "sdlpp/input/keyboard.hpp"

namespace sdlpp {
class quit_event final : public event<SDL_QuitEvent> {
public:
  quit_event() : event{event_type::quit} {}
  explicit quit_event(const SDL_QuitEvent &e) noexcept : event{e} {}
};

class keyboard_event final : public event<SDL_KeyboardEvent> {
public:
  keyboard_event() : event{event_type::key_down} {}

  explicit keyboard_event(const SDL_KeyboardEvent &e) noexcept : event{e} {}

  [[nodiscard]] auto is_active(const scan_code &code) const noexcept -> bool {
    return mEvent.keysym.scancode == code.get();
  }

  [[nodiscard]] auto is_active(const key_code &code) const noexcept -> bool {
    return static_cast<SDL_KeyCode>(mEvent.keysym.sym) == code();
  }

  [[nodiscard]] auto repeated() const noexcept -> bool { return mEvent.repeat; }

  [[nodiscard]] auto state() const noexcept -> button_state {
    return static_cast<button_state>(mEvent.state);
  }

  [[nodiscard]] auto released() const noexcept -> bool {
    return state() == button_state::released;
  }

  [[nodiscard]] auto pressed() const noexcept -> bool {
    return state() == button_state::pressed;
  }

  [[nodiscard]] auto scan() const noexcept -> scan_code {
    return mEvent.keysym.scancode;
  }

  [[nodiscard]] auto key() const noexcept -> key_code {
    return static_cast<SDL_KeyCode>(mEvent.keysym.sym);
  }

  [[nodiscard]] auto window_id() const noexcept -> uint32 {
    return mEvent.windowID;
  }
};

} // namespace sdlpp

#endif // MISC_EVENT_H_
