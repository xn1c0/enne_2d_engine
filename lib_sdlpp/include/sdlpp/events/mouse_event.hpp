#ifndef MOUSE_EVENT_H_
#define MOUSE_EVENT_H_

#include "sdlpp/common/primitives.hpp"
#include "sdlpp/events/event.hpp"
#include "sdlpp/input/button_state.hpp"
#include "sdlpp/input/mouse.hpp"

namespace sdlpp {

enum class mouse_wheel_direction : uint32 {
  normal = SDL_MOUSEWHEEL_NORMAL,
  flipped = SDL_MOUSEWHEEL_FLIPPED
};

class mouse_button_event final : public event<SDL_MouseButtonEvent> {
public:
  mouse_button_event() : event{event_type::mouse_button_down} {}

  explicit mouse_button_event(const SDL_MouseButtonEvent &e) noexcept
      : event{e} {}

  void set_window_id(const uint32 id) noexcept { mEvent.windowID = id; }

  void set_which(const uint32 which) noexcept { mEvent.which = which; }

  void set_button(const mouse_button button) noexcept {
    mEvent.button = to_underlying(button);
  }

  void set_state(const button_state state) noexcept {
    mEvent.state = to_underlying(state);
  }

  void set_clicks(const uint8 clicks) noexcept { mEvent.clicks = clicks; }

  void set_x(const int32 x) noexcept { mEvent.x = x; }
  void set_y(const int32 y) noexcept { mEvent.y = y; }

  [[nodiscard]] auto window_id() const noexcept -> uint32 {
    return mEvent.windowID;
  }

  [[nodiscard]] auto which() const noexcept -> uint32 { return mEvent.which; }

  [[nodiscard]] auto button() const noexcept -> mouse_button {
    return static_cast<mouse_button>(mEvent.button);
  }

  [[nodiscard]] auto state() const noexcept -> button_state {
    return static_cast<button_state>(mEvent.state);
  }

  [[nodiscard]] auto pressed() const noexcept -> bool {
    return state() == button_state::pressed;
  }

  [[nodiscard]] auto released() const noexcept -> bool {
    return state() == button_state::released;
  }

  [[nodiscard]] auto clicks() const noexcept -> uint8 { return mEvent.clicks; }

  [[nodiscard]] auto x() const noexcept -> int32 { return mEvent.x; }
  [[nodiscard]] auto y() const noexcept -> int32 { return mEvent.y; }

  [[nodiscard]] auto position() const noexcept -> pointi {
    return {xi{x()}, yi{y()}};
  }
};

class mouse_motion_event final : public event<SDL_MouseMotionEvent> {
public:
  mouse_motion_event() : event{event_type::mouse_motion} {}

  explicit mouse_motion_event(const SDL_MouseMotionEvent &e) noexcept
      : event{e} {}

  void set_window_id(const uint32 id) noexcept { mEvent.windowID = id; }

  void set_which(const uint32 which) noexcept { mEvent.which = which; }

  void set_state(const uint32 state) noexcept { mEvent.state = state; }

  void set_x(const int32 x) noexcept { mEvent.x = x; }
  void set_y(const int32 y) noexcept { mEvent.y = y; }

  void set_dx(const int32 dx) noexcept { mEvent.xrel = dx; }
  void set_dy(const int32 dy) noexcept { mEvent.yrel = dy; }

  [[nodiscard]] auto window_id() const noexcept -> uint32 {
    return mEvent.windowID;
  }

  [[nodiscard]] auto which() const noexcept -> uint32 { return mEvent.which; }

  [[nodiscard]] auto state() const noexcept -> uint32 { return mEvent.state; }

  [[nodiscard]] auto pressed(const mouse_button button) const noexcept -> bool {
    return mEvent.state & SDL_BUTTON(to_underlying(button));
  }

  [[nodiscard]] auto x() const noexcept -> int32 { return mEvent.x; }
  [[nodiscard]] auto y() const noexcept -> int32 { return mEvent.y; }

  [[nodiscard]] auto position() const noexcept -> pointi {
    return {xi{x()}, yi{y()}};
  }

  [[nodiscard]] auto dx() const noexcept -> int32 { return mEvent.xrel; }
  [[nodiscard]] auto dy() const noexcept -> int32 { return mEvent.yrel; }
};

class mouse_wheel_event final : public event<SDL_MouseWheelEvent> {
public:
  mouse_wheel_event() : event{event_type::mouse_wheel} {}

  explicit mouse_wheel_event(const SDL_MouseWheelEvent &e) noexcept
      : event{e} {}

  void set_window_id(const uint32 id) noexcept { mEvent.windowID = id; }

  void set_which(const uint32 which) noexcept { mEvent.which = which; }

  void set_x(const int32 x) noexcept { mEvent.x = x; }
  void set_y(const int32 y) noexcept { mEvent.y = y; }

  void set_precise_x(const float x) noexcept { mEvent.preciseX = x; }
  void set_precise_y(const float y) noexcept { mEvent.preciseY = y; }

  void set_direction(const mouse_wheel_direction direction) noexcept {
    mEvent.direction = to_underlying(direction);
  }

  [[nodiscard]] auto window_id() const noexcept -> uint32 {
    return mEvent.windowID;
  }

  [[nodiscard]] auto which() const noexcept -> uint32 { return mEvent.which; }

  [[nodiscard]] auto x() const noexcept -> int32 { return mEvent.x; }
  [[nodiscard]] auto y() const noexcept -> int32 { return mEvent.y; }

  [[nodiscard]] auto precise_x() const noexcept -> float {
    return mEvent.preciseX;
  }
  [[nodiscard]] auto precise_y() const noexcept -> float {
    return mEvent.preciseY;
  }

  void set_mouse_x(const int32 x) noexcept { mEvent.mouseX = x; }
  void set_mouse_y(const int32 y) noexcept { mEvent.mouseY = y; }

  [[nodiscard]] auto mouse_x() const noexcept -> int32 { return mEvent.mouseX; }
  [[nodiscard]] auto mouse_y() const noexcept -> int32 { return mEvent.mouseY; }

  [[nodiscard]] auto direction() const noexcept -> mouse_wheel_direction {
    return static_cast<mouse_wheel_direction>(mEvent.direction);
  }
};

} // namespace sdlpp

#endif // MOUSE_EVENT_H_
