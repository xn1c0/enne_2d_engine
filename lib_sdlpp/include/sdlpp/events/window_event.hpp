#ifndef WINDOW_EVENT_H_
#define WINDOW_EVENT_H_

#include "sdlpp/common/primitives.hpp"
#include "sdlpp/events/event.hpp"
#include <SDL2/SDL_events.h>

namespace sdlpp {

/**
 * @brief Enumeration for window event id.
 */
enum class window_event_id {
  none = SDL_WINDOWEVENT_NONE,
  shown = SDL_WINDOWEVENT_SHOWN,
  hidden = SDL_WINDOWEVENT_HIDDEN,
  exposed = SDL_WINDOWEVENT_EXPOSED,
  moved = SDL_WINDOWEVENT_MOVED,
  resized = SDL_WINDOWEVENT_RESIZED,
  size_changed = SDL_WINDOWEVENT_SIZE_CHANGED,
  minimized = SDL_WINDOWEVENT_MINIMIZED,
  maximized = SDL_WINDOWEVENT_MAXIMIZED,
  restored = SDL_WINDOWEVENT_RESTORED,
  enter = SDL_WINDOWEVENT_ENTER,
  leave = SDL_WINDOWEVENT_LEAVE,
  focus_gained = SDL_WINDOWEVENT_FOCUS_GAINED,
  focus_lost = SDL_WINDOWEVENT_FOCUS_LOST,
  close = SDL_WINDOWEVENT_CLOSE,
  take_focus = SDL_WINDOWEVENT_TAKE_FOCUS,
  display_changed = SDL_WINDOWEVENT_DISPLAY_CHANGED,
  icc_profile_changed = SDL_WINDOWEVENT_ICCPROF_CHANGED,
  hit_test = SDL_WINDOWEVENT_HIT_TEST
};

/**
 * @brief Class window_event.
 */
class window_event final : public event<SDL_WindowEvent> {
public:
  /**
   * @brief Default constructor.
   */
  window_event();
  /**
   * @brief Secondary constructor.
   * Constructs from SDL_WindowEvent.
   * @param e THe SDL_WindowEvent to construct from.
   */
  explicit window_event(const SDL_WindowEvent &e) noexcept;

  /**
   * @brief Gets the window event id.
   * @return The window_event_id.
   */
  [[nodiscard]] auto event_id() const noexcept -> window_event_id;

  /**
   * @brief Sets the window event id.
   * @param id The new window_event_id.
   */
  auto set_event_id(const window_event_id id) noexcept -> void;

  /**
   * @brief Gets the data1 field.
   * @return The data2 field
   */
  [[nodiscard]] auto data1() const noexcept -> int32;

  /**
   * @brief Sets the data1 field.
   * @param value The new value for field data1.
   */
  auto set_data1(const int32 value) noexcept -> void;

  /**
   * @brief Gets the data2 field.
   * @return The data2 field
   */
  [[nodiscard]] auto data2() const noexcept -> int32;

  /**
   * @brief Sets the data2 field.
   * @param value The new value for field data2.
   */
  void set_data2(const int32 value) noexcept;
};

} // namespace sdlpp

#endif // WINDOW_EVENT_H_
