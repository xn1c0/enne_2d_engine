#ifndef LIB_SDLPP_INCLUDE_SDLPP_INIT_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_INIT_HPP_

#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/sdl_object.hpp"

/**
 * @author Nicolò Plebani
 * @file init.hpp
 */

namespace sdlpp {

/**
 * @brief Init Flags.
 * This is redundant but it's important since SDL2 puts this
 * declaration in SDL.h (in SDL3 it is in SDL_init.h) so this is a workaround
 * for now.
 */
enum class init_flags : uint32 {
  timer = 0x00000001u,
  audio = 0x00000010u, ///< SDL_INIT_AUDIO implies SDL_INIT_EVENTS.
  video = 0x00000020u, //< SDL_INIT_VIDEO implies SDL_INIT_EVENTS.
  joystick =
      0x00000200u, ///< SDL_INIT_JOYSTICK implies SDL_INIT_EVENTS, should be
                   ///< initialized on the same thread as SDL_INIT_VIDEO on
                   ///< Windows if you don't set SDL_HINT_JOYSTICK_THREAD.
  haptic = 0x00001000u,
  gamecontroller =
      0x00002000u, ///< SDL_INIT_GAMECONTROLLER implies SDL_INIT_JOYSTICK.
  events = 0x00004000u,
  sensor = 0x00008000u,       ///< SDL_INIT_SENSOR implies SDL_INIT_EVENTS.
  no_parachute = 0x00100000u, ///< compatibility; this flag is ignored.
  everithing = timer | audio | video | events | joystick | haptic |
               gamecontroller | sensor
};

/**
 * @brief Bitwise operator |.
 * Each bit in a OR each bit in b.
 * @param lhs The left hand side operand.
 * @param rhs The right hand side operand.
 */
inline constexpr auto operator|(init_flags const &lhs,
                                init_flags const &rhs) noexcept -> init_flags {
  return static_cast<init_flags>(static_cast<uint32>(lhs) |
                                 static_cast<uint32>(rhs));
}

/**
 * @brief Bitwise operator &.
 * Each bit in a AND each bit in b.
 * @param lhs The left hand side operand.
 * @param rhs The right hand side operand.
 */
inline constexpr auto operator&(init_flags const &lhs,
                                init_flags const &rhs) noexcept -> init_flags {
  return static_cast<init_flags>(static_cast<uint32>(lhs) &
                                 static_cast<uint32>(rhs));
}

/**
 * @brief SDL class.
 * Takes care of SDL library core operations.
 */
class sdl : public sdl_object {
public:
  /**
   * @brief Secondary constrcutor.
   * Initialises SDL library.
   * @param flags The flags for SDL_Init (bitwise operations allowed).
   * @throws sdlpp::Exception
   */
  sdl(const init_flags flags);

  /**
   * @brief Destructor.
   */
  virtual ~sdl() noexcept;

  /**
   * @brief Queries SDL2 subsystems for initialisation.
   * Checks which SDL2 subsystems is initialised.
   * @param flags The flags for SDL_Init (bitwise operations allowed).
   * @throws sdlpp::Exception
   */
  init_flags is_initialized(init_flags flags) noexcept;

  /**
   * @brief Initialises additional SDL2 subsystems.
   * The flags are used to choose which subsytems to initialise.
   * @param flags The flags for SDL_Init (bitwise operations allowed).
   * @throws sdlpp::Exception
   */
  void init_subsystem(init_flags flags);

  /**
   * @brief De-initialises additional SDL2 subsystems.
   * The flags are used to choose which subsytems to de-initialise.
   * @param flags The flags for SDL_Init (bitwise operations allowed).
   * @throws sdlpp::Exception
   */
  void quit_subsystem(init_flags flags) noexcept;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_INIT_HPP_
