#ifndef LIB_SDLPP_INCLUDE_SDLPP_INPUT_BUTTON_STATE_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_INPUT_BUTTON_STATE_HPP_

#include <SDL2/SDL_events.h>

#include "sdlpp/common/primitives.hpp"
#include <cassert>

/**
 * @author Nicolò Plebani
 * @file button_state.hpp
 */

namespace sdlpp {

enum class button_state : uint8 {
  released = SDL_RELEASED,
  pressed = SDL_PRESSED
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_INPUT_BUTTON_STATE_HPP_
