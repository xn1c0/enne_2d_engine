#ifndef MOUSE_H_
#define MOUSE_H_

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_haptic.h>
#include <SDL2/SDL_mouse.h>

#include "sdlpp/common/exception.hpp"
#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/video/renderer.hpp"

namespace sdlpp {
enum class system_cursor {
  arrow = SDL_SYSTEM_CURSOR_ARROW,
  ibeam = SDL_SYSTEM_CURSOR_IBEAM,
  wait = SDL_SYSTEM_CURSOR_WAIT,
  crosshair = SDL_SYSTEM_CURSOR_CROSSHAIR,
  wait_arrow = SDL_SYSTEM_CURSOR_WAITARROW,
  size_nw_se = SDL_SYSTEM_CURSOR_SIZENWSE,
  size_ne_sw = SDL_SYSTEM_CURSOR_SIZENESW,
  size_we = SDL_SYSTEM_CURSOR_SIZEWE,
  size_ns = SDL_SYSTEM_CURSOR_SIZENS,
  size_all = SDL_SYSTEM_CURSOR_SIZEALL,
  no = SDL_SYSTEM_CURSOR_NO,
  hand = SDL_SYSTEM_CURSOR_HAND
};

enum class mouse_button : uint8 {
  left = SDL_BUTTON_LEFT,
  middle = SDL_BUTTON_MIDDLE,
  right = SDL_BUTTON_RIGHT,
  x1 = SDL_BUTTON_X1,
  x2 = SDL_BUTTON_X2
};

[[nodiscard]] constexpr auto system_cursor_count() noexcept -> int {
  return SDL_NUM_SYSTEM_CURSORS;
}

class mouse final {
public:
  mouse() noexcept = default;

  static auto warp(const pointi &pos) noexcept -> void {
    // CHECK SDL STATUS
    SDL_WarpMouseGlobal(pos.x(), pos.y());
  }

  static auto warp_in_window(const pointi &pos) noexcept -> void {
    SDL_WarpMouseInWindow(nullptr, pos.x(), pos.y());
  }

  static auto set_relative_mode(const bool enabled) noexcept -> void {
    // CHECK SDL STATUS
    SDL_SetRelativeMouseMode(enabled ? SDL_TRUE : SDL_FALSE);
  }

  [[nodiscard]] static auto is_relative_mode_enabled() noexcept -> bool {
    return SDL_GetRelativeMouseMode() == SDL_TRUE;
  }

  auto update(const renderer &renderer) noexcept -> void {
    mPreviousMask = mCurrentMask;
    mPreviousPosition = mCurrentPosition;

    int mx{};
    int my{};
    mCurrentMask = SDL_GetMouseState(&mx, &my);

    const auto logicalSize = renderer.logical_size();
    if (logicalSize.w() != 0 && logicalSize.h() != 0) {
      mCurrentPosition = pointi{renderer.to_logical(mx, my)};
    } else {
      // No logical size has been set for the renderer
      mCurrentPosition.x() = mx;
      mCurrentPosition.y() = my;
    }
  }

  [[nodiscard]] auto position() const noexcept -> pointi {
    return mCurrentPosition;
  }

  [[nodiscard]] auto x() const noexcept -> int { return mCurrentPosition.x(); }
  [[nodiscard]] auto y() const noexcept -> int { return mCurrentPosition.y(); }

  [[nodiscard]] auto is_left_pressed() const noexcept -> bool {
    return is_pressed(SDL_BUTTON_LMASK);
  }

  [[nodiscard]] auto is_middle_pressed() const noexcept -> bool {
    return is_pressed(SDL_BUTTON_MMASK);
  }

  [[nodiscard]] auto is_right_pressed() const noexcept -> bool {
    return is_pressed(SDL_BUTTON_RMASK);
  }

  [[nodiscard]] auto was_left_released() const noexcept -> bool {
    return was_released(SDL_BUTTON_LMASK);
  }

  [[nodiscard]] auto was_middle_released() const noexcept -> bool {
    return was_released(SDL_BUTTON_MMASK);
  }

  [[nodiscard]] auto was_right_released() const noexcept -> bool {
    return was_released(SDL_BUTTON_RMASK);
  }

  [[nodiscard]] auto was_moved() const noexcept -> bool {
    return (mCurrentPosition.x() != mPreviousPosition.x()) ||
           (mCurrentPosition.y() != mPreviousPosition.y());
  }

  [[nodiscard]] static auto position_relative_window() noexcept -> pointi {
    int x{}, y{};
    SDL_GetMouseState(&x, &y);
    return {xi{x}, yi{y}};
  }

  [[nodiscard]] static auto position_relative_desktop() noexcept -> pointi {
    int x{}, y{};
    SDL_GetGlobalMouseState(&x, &y);
    return {xi{x}, yi{y}};
  }

  [[nodiscard]] static auto delta() noexcept -> pointi {
    int x{}, y{};
    SDL_GetRelativeMouseState(&x, &y);
    return {xi{x}, yi{y}};
  }

  [[nodiscard]] static auto is_haptic() noexcept -> bool {
    return SDL_MouseIsHaptic() == SDL_TRUE;
  }

private:
  pointi mCurrentPosition;
  pointi mPreviousPosition;
  uint32 mCurrentMask{};
  uint32 mPreviousMask{};

  [[nodiscard]] auto is_pressed(const uint32 mask) const noexcept -> bool {
    return mCurrentMask & mask;
  }

  [[nodiscard]] auto was_released(const uint32 mask) const noexcept -> bool {
    return !(mCurrentMask & mask) && mPreviousMask & mask;
  }
};

class cursor final {
public:
  explicit cursor(const system_cursor cursor)
      : mCursor{SDL_CreateSystemCursor(static_cast<SDL_SystemCursor>(cursor))} {
    if (!mCursor) {
      throw exception{"SDL_CreateSystemCursor"};
    }
  }

  explicit cursor(SDL_Cursor *cursor) noexcept : mCursor{cursor} {}

  explicit cursor(const cursor &owner) noexcept : mCursor{owner()} {}

  void enable() noexcept { SDL_SetCursor(mCursor); }

  [[nodiscard]] auto is_enabled() const noexcept -> bool {
    return SDL_GetCursor() == (*this)();
  }

  static void reset() noexcept { SDL_SetCursor(SDL_GetDefaultCursor()); }

  static void force_redraw() noexcept { SDL_SetCursor(nullptr); }

  static void set_visible(const bool visible) noexcept {
    SDL_ShowCursor(visible ? SDL_ENABLE : SDL_DISABLE);
  }

  [[nodiscard]] static auto visible() noexcept -> bool {
    return SDL_ShowCursor(SDL_QUERY) == SDL_ENABLE;
  }

  [[nodiscard]] static auto get_default() noexcept -> cursor {
    return cursor{SDL_GetDefaultCursor()};
  }

  [[nodiscard]] static auto get_current() noexcept -> cursor {
    return cursor{SDL_GetCursor()};
  }

  [[nodiscard]] auto operator()() const noexcept -> SDL_Cursor * {
    return mCursor;
  }

private:
  SDL_Cursor *mCursor;
};

} // namespace sdlpp

#endif // MOUSE_H_
