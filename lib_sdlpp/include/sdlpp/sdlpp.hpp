#ifndef LIB_SDLPP_INCLUDE_SDLPP_SDLPP_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_SDLPP_HPP_

/**
 * @author Nicolò Plebani
 * @file sdlpp.hpp
 */

#include <sdlpp/init.hpp>

#include <sdlpp/video/blend.hpp>
#include <sdlpp/video/colour.hpp>
#include <sdlpp/video/pixel.hpp>
#include <sdlpp/video/renderer.hpp>
#include <sdlpp/video/renderer_info.hpp>
#include <sdlpp/video/surface.hpp>
#include <sdlpp/video/texture.hpp>
#include <sdlpp/video/window.hpp>

#include <sdlpp/events/event.hpp>
#include <sdlpp/events/handler.hpp>
#include <sdlpp/events/misc_event.hpp>
#include <sdlpp/events/window_event.hpp>

#include <sdlpp/common/exception.hpp>
#include <sdlpp/common/literals.hpp>
#include <sdlpp/common/primitives.hpp>

#include <sdlpp/common/math/point.hpp>
#include <sdlpp/common/math/rect.hpp>
#include <sdlpp/common/math/rect_size.hpp>

#endif // LIB_SDLPP_INCLUDE_SDLPP_SDLPP_HPP_
