#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_BLEND_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_BLEND_HPP_

#include "sdlpp/common/primitives.hpp"
#include <SDL2/SDL_blendmode.h>

namespace sdlpp {
/**
 * @brief Blend mode.
 */
enum class blend_mode : uint32 {
  none = SDL_BLENDMODE_NONE,
  blend = SDL_BLENDMODE_BLEND,
  add = SDL_BLENDMODE_ADD,
  mod = SDL_BLENDMODE_MOD,
  mul = SDL_BLENDMODE_MUL,
  invalid = SDL_BLENDMODE_INVALID
};

/**
 * @brief BLend factor.
 */
enum class blend_factor : uint32 {
  zero = SDL_BLENDFACTOR_ZERO,
  one = SDL_BLENDFACTOR_ONE,
  src_color = SDL_BLENDFACTOR_SRC_COLOR,
  one_minus_src_color = SDL_BLENDFACTOR_ONE_MINUS_SRC_COLOR,
  src_alpha = SDL_BLENDFACTOR_SRC_ALPHA,
  one_minus_src_alpha = SDL_BLENDFACTOR_ONE_MINUS_SRC_ALPHA,
  dst_color = SDL_BLENDFACTOR_DST_COLOR,
  one_minus_dst_color = SDL_BLENDFACTOR_ONE_MINUS_DST_COLOR,
  dst_alpha = SDL_BLENDFACTOR_DST_ALPHA,
  one_minus_dst_alpha = SDL_BLENDFACTOR_ONE_MINUS_DST_ALPHA
};

/**
 * @brief Blend operation.
 */
enum class blend_operation : uint32 {
  add = SDL_BLENDOPERATION_ADD,
  sub = SDL_BLENDOPERATION_SUBTRACT,
  reverse_sub = SDL_BLENDOPERATION_REV_SUBTRACT,
  min = SDL_BLENDOPERATION_MINIMUM,
  max = SDL_BLENDOPERATION_MAXIMUM
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_VIDEO_BLEND_HPP_
