#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_COLOUR_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_COLOUR_HPP_

#include <SDL2/SDL_pixels.h>

#include "sdlpp/common/primitives.hpp"
#include <string>

/**
 * @author Nicolò Plebani
 * @file colour.hpp
 */

namespace sdlpp {

/**
 * @brief Operator "is equal to" for SDL_Color.
 */
constexpr auto operator==(const SDL_Color &lhs, const SDL_Color &rhs) -> bool {
  return lhs.r == rhs.r and lhs.g == rhs.g and lhs.b == rhs.b and
         lhs.a == rhs.a;
}

/**
 * @brief Colour class.
 */
class colour {
public:
  using value_type = uint8;
  using colour_type = SDL_Color;

private:
  SDL_Colour mColour;

public:
  [[nodiscard]] constexpr auto r() const & -> const value_type & {
    return mColour.r;
  }
  [[nodiscard]] constexpr auto r() & -> value_type & { return mColour.r; }
  [[nodiscard]] constexpr auto r() && -> value_type && {
    return std::move(mColour.r);
  }
  [[nodiscard]] constexpr auto g() const & -> const value_type & {
    return mColour.g;
  }
  [[nodiscard]] constexpr auto g() & -> value_type & { return mColour.g; }
  [[nodiscard]] constexpr auto g() && -> value_type && {
    return std::move(mColour.g);
  }
  [[nodiscard]] constexpr auto b() const & -> const value_type & {
    return mColour.b;
  }
  [[nodiscard]] constexpr auto b() & -> value_type & { return mColour.b; }
  [[nodiscard]] constexpr auto b() && -> value_type && {
    return std::move(mColour.b);
  }
  [[nodiscard]] constexpr auto a() const & -> const value_type & {
    return mColour.a;
  }
  [[nodiscard]] constexpr auto a() & -> value_type & { return mColour.a; }
  [[nodiscard]] constexpr auto a() && -> value_type && {
    return std::move(mColour.a);
  }

  [[nodiscard]] constexpr auto
  operator()() const noexcept -> const colour_type * {
    return &mColour;
  }

  [[nodiscard]] constexpr auto operator()() noexcept -> colour_type * {
    return &mColour;
  }

  /**
   * @brief Default constructor.
   */
  constexpr colour() noexcept : mColour{0, 0, 0, 0xFF} {}

  /**
   * @brief Secondary constructor.
   * @param colour The SDL_Colour value.
   * @post The values are assigned to the respective members.
   */
  constexpr colour(const SDL_Colour &colour) noexcept : mColour{colour} {}

  /**
   * @brief Secondary constructor.
   * @param red The red colour value.
   * @param green The green colour value.
   * @param blue The blue colour value.
   * @param alpha The alpha value.
   * @post The values are assigned to the respective members.
   */
  constexpr colour(const value_type red, const value_type green,
                   const value_type blue,
                   const value_type alpha = 0xFF) noexcept
      : mColour{red, green, blue, alpha} {}

  constexpr auto operator==(const colour &other) const -> bool = default;
};

/**
 * @brief Formats the colour into a readable std::string.
 * @param colour The colour to compute.
 * @return The std::string representing the colour.
 */
[[nodiscard]] constexpr std::string to_string(const colour &colour) noexcept {
  auto s = [](colour::value_type value) -> std::string {
    return std::to_string(value);
  };
  std::string tmp = "[r: " + s(colour.r()) + ", g: " + s(colour.g()) +
                    ", b: " + s(colour.b()) + ", a: " + s(colour.a()) + "]";
  return tmp;
}

/**
 * @brief Ostream operator.
 * @param os The output stream.
 * @param colour The colour to be printed in the ostream.
 * @return The reference to os.
 */
std::ostream &operator<<(std::ostream &os, const colour &colour) noexcept;

namespace colours {

inline constexpr colour transparent{0, 0, 0, 0};
inline constexpr colour white{0xFF, 0xFF, 0xFF};
inline constexpr colour black{0, 0, 0};
inline constexpr colour alice_blue{0xF0, 0xF8, 0xFF};
inline constexpr colour antique_white{0xFA, 0xEB, 0xD7};
inline constexpr colour aqua{0, 0xFF, 0xFF};
inline constexpr colour aquamarine{0x7F, 0xFF, 0xD4};
inline constexpr colour azure{0xF0, 0xFF, 0xFF};
inline constexpr colour beige{0xF5, 0xF5, 0xDC};
inline constexpr colour bisque{0xFF, 0xE4, 0xC4};
inline constexpr colour blanched_almond{0xFF, 0xEB, 0xCD};
inline constexpr colour blue{0, 0, 0xFF};
inline constexpr colour blue_violet{0x8A, 0x2B, 0xE2};
inline constexpr colour brown{0xA5, 0x2A, 0x2A};
inline constexpr colour burly_wood{0xDE, 0xB8, 0x87};
inline constexpr colour cadet_blue{0x5F, 0x9E, 0xA0};
inline constexpr colour chartreuse{0x7F, 0xFF, 0};
inline constexpr colour chocolate{0xD2, 0x69, 0x1E};
inline constexpr colour coral{0xFF, 0x7F, 0x50};
inline constexpr colour cornflower_blue{0x64, 0x95, 0xED};
inline constexpr colour cornsilk{0xFF, 0xF8, 0xDC};
inline constexpr colour crimson{0xDC, 0x14, 0x3C};
inline constexpr colour cyan{0, 0xFF, 0xFF};
inline constexpr colour deep_pink{0xFF, 0x14, 0x93};
inline constexpr colour deep_sky_blue{0, 0xBF, 0xFF};
inline constexpr colour dim_grey{0x69, 0x69, 0x69};
inline constexpr colour dodger_blue{0x1E, 0x90, 0xFF};
inline constexpr colour fire_brick{0xB2, 0x22, 0x22};
inline constexpr colour floral_white{0xFF, 0xFA, 0xF0};
inline constexpr colour forest_green{0x22, 0x8B, 0x22};
inline constexpr colour fuchsia{0xFF, 0, 0xFF};
inline constexpr colour gainsboro{0xDC, 0xDC, 0xDC};
inline constexpr colour ghost_white{0xF8, 0xF8, 0xFF};
inline constexpr colour gold{0xFF, 0xD7, 0};
inline constexpr colour golden_rod{0xDA, 0xA5, 0x20};
inline constexpr colour grey{0x80, 0x80, 0x80};
inline constexpr colour green{0, 0x80, 0};
inline constexpr colour green_yellow{0xAD, 0xFF, 0x2F};
inline constexpr colour honey_dew{0xF0, 0xFF, 0xF0};
inline constexpr colour hot_pink{0xFF, 0x69, 0xB4};
inline constexpr colour indian_red{0xCD, 0x5C, 0x5C};
inline constexpr colour indigo{0x4B, 0, 0x82};
inline constexpr colour ivory{0xFF, 0xFF, 0xF0};
inline constexpr colour khaki{0xF0, 0xE6, 0x8C};
inline constexpr colour lavender{0xE6, 0xE6, 0xFA};
inline constexpr colour lavender_blush{0xFF, 0xF0, 0xF5};
inline constexpr colour lawn_green{0x7C, 0xFC, 0};
inline constexpr colour lemon_chiffon{0xFF, 0xFA, 0xCD};
inline constexpr colour lime{0, 0xFF, 0};
inline constexpr colour lime_green{0x32, 0xCD, 0x32};
inline constexpr colour linen{0xFA, 0xF0, 0xE6};
inline constexpr colour magenta{0xFF, 0, 0xFF};
inline constexpr colour maroon{0x80, 0, 0};
inline constexpr colour midnight_blue{0x19, 0x19, 0x70};
inline constexpr colour mint_cream{0xF5, 0xFF, 0xFA};
inline constexpr colour misty_rose{0xFF, 0xE4, 0xE1};
inline constexpr colour moccasin{0xFF, 0xE4, 0xB5};
inline constexpr colour navajo_white{0xFF, 0xDE, 0xAD};
inline constexpr colour navy{0, 0, 0x80};
inline constexpr colour old_lace{0xFD, 0xF5, 0xE6};
inline constexpr colour olive{0x80, 0x80, 0};
inline constexpr colour olive_drab{0x6B, 0x8E, 0x23};
inline constexpr colour orange{0xFF, 0xA5, 0};
inline constexpr colour orange_red{0xFF, 0x45, 0};
inline constexpr colour orchid{0xDA, 0x70, 0xD6};
inline constexpr colour pale_golden_rod{0xEE, 0xE8, 0xAA};
inline constexpr colour pale_green{0x98, 0xFB, 0x98};
inline constexpr colour pale_turquoise{0xAF, 0xEE, 0xEE};
inline constexpr colour pale_violet_red{0xDB, 0x70, 0x93};
inline constexpr colour papaya_whip{0xFF, 0xEF, 0xD5};
inline constexpr colour peach_puff{0xFF, 0xDA, 0xB9};
inline constexpr colour peru{0xCD, 0x85, 0x3F};
inline constexpr colour pink{0xFF, 0xC0, 0xCB};
inline constexpr colour plum{0xDD, 0xA0, 0xDD};
inline constexpr colour powder_blue{0xB0, 0xE0, 0xE6};
inline constexpr colour purple{0x80, 0, 0x80};
inline constexpr colour rebecca_purple{0x66, 0x33, 0x99};
inline constexpr colour red{0xFF, 0, 0};
inline constexpr colour rosy_brown{0xBC, 0x8F, 0x8F};
inline constexpr colour royal_blue{0x41, 0x69, 0xE1};
inline constexpr colour saddle_brown{0x8B, 0x45, 0x13};
inline constexpr colour salmon{0xFA, 0x80, 0x72};
inline constexpr colour sandy_brown{0xF4, 0xA4, 0x60};
inline constexpr colour sea_green{0x2E, 0x8B, 0x57};
inline constexpr colour sea_shell{0xFF, 0xF5, 0xEE};
inline constexpr colour sienna{0xA0, 0x52, 0x2D};
inline constexpr colour silver{0xC0, 0xC0, 0xC0};
inline constexpr colour sky_blue{0x87, 0xCE, 0xEB};
inline constexpr colour slate_blue{0x6A, 0x5A, 0xCD};
inline constexpr colour slate_grey{0x70, 0x80, 0x90};
inline constexpr colour snow{0xFF, 0xFA, 0xFA};
inline constexpr colour spring_green{0, 0xFF, 0x7F};
inline constexpr colour steel_blue{0x46, 0x82, 0xB4};
inline constexpr colour tan{0xD2, 0xB4, 0x8C};
inline constexpr colour teal{0, 0x80, 0x80};
inline constexpr colour thistle{0xD8, 0xBF, 0xD8};
inline constexpr colour tomato{0xFF, 0x63, 0x47};
inline constexpr colour turquoise{0x40, 0xE0, 0xD0};
inline constexpr colour violet{0xEE, 0x82, 0xEE};
inline constexpr colour wheat{0xF5, 0xDE, 0xB3};
inline constexpr colour white_smoke{0xF5, 0xF5, 0xF5};
inline constexpr colour yellow{0xFF, 0xFF, 0};
inline constexpr colour yellow_green{0x9A, 0xCD, 0x32};
inline constexpr colour light_blue{0xAD, 0xD8, 0xE6};
inline constexpr colour light_coral{0xF0, 0x80, 0x80};
inline constexpr colour light_cyan{0xE0, 0xFF, 0xFF};
inline constexpr colour light_golden_rod_yellow{0xFA, 0xFA, 0xD2};
inline constexpr colour light_grey{0xD3, 0xD3, 0xD3};
inline constexpr colour light_green{0x90, 0xEE, 0x90};
inline constexpr colour light_pink{0xFF, 0xB6, 0xC1};
inline constexpr colour light_salmon{0xFF, 0xA0, 0x7A};
inline constexpr colour light_sea_green{0x20, 0xB2, 0xAA};
inline constexpr colour light_sky_blue{0x87, 0xCE, 0xFA};
inline constexpr colour light_slate_grey{0x77, 0x88, 0x99};
inline constexpr colour light_steel_blue{0xB0, 0xC4, 0xDE};
inline constexpr colour light_yellow{0xFF, 0xFF, 0xE0};
inline constexpr colour medium_aqua_marine{0x66, 0xCD, 0xAA};
inline constexpr colour medium_blue{0, 0, 0xCD};
inline constexpr colour medium_orchid{0xBA, 0x55, 0xD3};
inline constexpr colour medium_purple{0x93, 0x70, 0xDB};
inline constexpr colour medium_sea_green{0x3C, 0xB3, 0x71};
inline constexpr colour medium_slate_blue{0x7B, 0x68, 0xEE};
inline constexpr colour medium_spring_green{0, 0xFA, 0x9A};
inline constexpr colour medium_turquoise{0x48, 0xD1, 0xCC};
inline constexpr colour medium_violet_red{0xC7, 0x15, 0x85};
inline constexpr colour dark_blue{0, 0, 0x8B};
inline constexpr colour dark_cyan{0, 0x8B, 0x8B};
inline constexpr colour dark_golden_rod{0xB8, 0x86, 0x0B};
inline constexpr colour dark_grey{0xA9, 0xA9, 0xA9};
inline constexpr colour dark_green{0, 0x64, 0};
inline constexpr colour dark_khaki{0xBD, 0xB7, 0x6B};
inline constexpr colour dark_magenta{0x8B, 0, 0x8B};
inline constexpr colour dark_olive_green{0x55, 0x6B, 0x2F};
inline constexpr colour dark_orange{0xFF, 0x8C, 0};
inline constexpr colour dark_orchid{0x99, 0x32, 0xCC};
inline constexpr colour dark_red{0x8B, 0, 0};
inline constexpr colour dark_salmon{0xE9, 0x96, 0x7A};
inline constexpr colour dark_sea_green{0x8F, 0xBC, 0x8F};
inline constexpr colour dark_slate_blue{0x48, 0x3D, 0x8B};
inline constexpr colour dark_slate_gray{0x2F, 0x4F, 0x4F};
inline constexpr colour dark_slate_grey{dark_slate_gray};
inline constexpr colour dark_turquoise{0, 0xCE, 0xD1};
inline constexpr colour dark_violet{0x94, 0, 0xD3};

} // namespace colours

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_VIDEO_COLOUR_HPP_
