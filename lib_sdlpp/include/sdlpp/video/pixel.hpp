#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_PIXEL_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_PIXEL_HPP_

#include <SDL2/SDL_pixels.h>

#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/sdl_object.hpp"

/**
 * @author Nicolò Plebani
 * @file pixel.hpp
 */

namespace sdlpp {

/**
 * @brief Pixel format enumeration.
 */
enum class pixel_format : uint32 {
  unknown = SDL_PIXELFORMAT_UNKNOWN,
  index1_lsb = SDL_PIXELFORMAT_INDEX1LSB,
  index1_msb = SDL_PIXELFORMAT_INDEX1MSB,
  index4_lsb = SDL_PIXELFORMAT_INDEX4LSB,
  index4_msb = SDL_PIXELFORMAT_INDEX4MSB,
  index8 = SDL_PIXELFORMAT_INDEX8,
  rgba32 = SDL_PIXELFORMAT_RGBA32,
  argb32 = SDL_PIXELFORMAT_ARGB32,
  bgra32 = SDL_PIXELFORMAT_BGRA32,
  abgr32 = SDL_PIXELFORMAT_ABGR32,
  rgb332 = SDL_PIXELFORMAT_RGB332,
  rgb444 = SDL_PIXELFORMAT_RGB444,
  bgr444 = SDL_PIXELFORMAT_BGR444,
  rgb555 = SDL_PIXELFORMAT_RGB555,
  bgr555 = SDL_PIXELFORMAT_BGR555,
  argb4444 = SDL_PIXELFORMAT_ARGB4444,
  rgba4444 = SDL_PIXELFORMAT_RGBA4444,
  abgr4444 = SDL_PIXELFORMAT_ABGR4444,
  bgra4444 = SDL_PIXELFORMAT_BGRA4444,
  argb1555 = SDL_PIXELFORMAT_ARGB1555,
  rgba5551 = SDL_PIXELFORMAT_RGBA5551,
  abgr1555 = SDL_PIXELFORMAT_ABGR1555,
  bgra5551 = SDL_PIXELFORMAT_BGRA5551,
  rgb565 = SDL_PIXELFORMAT_RGB565,
  bgr565 = SDL_PIXELFORMAT_BGR565,
  rgb24 = SDL_PIXELFORMAT_RGB24,
  bgr24 = SDL_PIXELFORMAT_BGR24,
  rgb888 = SDL_PIXELFORMAT_RGB888,
  bgr888 = SDL_PIXELFORMAT_BGR888,
  rgbx8888 = SDL_PIXELFORMAT_RGBX8888,
  bgrx8888 = SDL_PIXELFORMAT_BGRX8888,
  argb8888 = SDL_PIXELFORMAT_ARGB8888,
  rgba8888 = SDL_PIXELFORMAT_RGBA8888,
  abgr8888 = SDL_PIXELFORMAT_ABGR8888,
  bgra8888 = SDL_PIXELFORMAT_BGRA8888,
  argb2101010 = SDL_PIXELFORMAT_ARGB2101010,
  yv12 = SDL_PIXELFORMAT_YV12,
  iyuv = SDL_PIXELFORMAT_IYUV,
  yuy2 = SDL_PIXELFORMAT_YUY2,
  uyvy = SDL_PIXELFORMAT_UYVY,
  yvyu = SDL_PIXELFORMAT_YVYU,
  nv12 = SDL_PIXELFORMAT_NV12,
  nv21 = SDL_PIXELFORMAT_NV21,
  external_oes = SDL_PIXELFORMAT_EXTERNAL_OES,
  xrgb4444 = SDL_PIXELFORMAT_XRGB4444,
  xbgr4444 = SDL_PIXELFORMAT_XBGR4444,
  xrgb1555 = SDL_PIXELFORMAT_XRGB1555,
  xbgr1555 = SDL_PIXELFORMAT_XBGR1555,
  xrgb8888 = SDL_PIXELFORMAT_XRGB8888,
  xbgr8888 = SDL_PIXELFORMAT_XBGR8888
};

class colour;

/**
 * @brief CLass pixel_format_info.
 * Manages SDL_PixelFormat which contains pixel format information.
 */
class pixel_format_info final : public sdl_object {
private:
  SDL_PixelFormat *mFormat;

public:
  /**
   * @brief Gets pointer to the managed SDL_PixelFormat structure.
   * @return The pointer to the managed SDL_PixelFormat structure.
   */
  [[nodiscard]] auto operator()() const noexcept -> SDL_PixelFormat *;

  /**
   * @brief Secondary constructor.
   * Constructs from existing SDL_PixelFormat structure.
   */
  explicit pixel_format_info(SDL_PixelFormat *format);

  /**
   * @brief Secondary constructor.
   * Creates a pixel_format_info from the specified format.
   * @param format The pixel format to use.
   */
  explicit pixel_format_info(const pixel_format format);

  /**
   * @brief Destructor;
   */
  ~pixel_format_info();

  /**
   * @brief Move constructor.
   */
  pixel_format_info(pixel_format_info &&other) noexcept;

  /**
   * @brief Move assignment operator.
   */
  pixel_format_info &operator=(pixel_format_info &&other) noexcept;

  /**
   * @brief Gets the RGBA values from a pixel in the specified format.
   * @param pixel The pixel value.
   * @return The colour with the values of the pixel.
   */
  [[nodiscard]] auto pixel_to_rgba(const uint32 pixel) const noexcept -> colour;

  /**
   * @brief Gets the pixel format.
   * @return The pixel format.
   */
  [[nodiscard]] auto format() const noexcept -> pixel_format;

  /**
   * @brief Gets the human readable name of a pixel format.
   * @return The readable pixel format name.
   */
  [[nodiscard]] auto name() const noexcept -> std::string;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_VIDEO_PIXEL_HPP_
