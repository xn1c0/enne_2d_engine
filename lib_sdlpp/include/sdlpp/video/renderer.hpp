#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_RENDERER_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_RENDERER_HPP_

#include <SDL2/SDL_render.h>

#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/sdl_object.hpp"
#include "sdlpp/video/blend.hpp"
#include "sdlpp/video/colour.hpp"
#include "sdlpp/video/renderer_info.hpp"
#include "sdlpp/video/window.hpp"
#include <cassert>

/**
 * @author Nicolò Plebani
 * @file renderer.hpp
 */

struct SDL_RendererInfo;
struct SDL_Renderer;

namespace sdlpp {

class window;
class texture;

/**
 * @brief Renderer Flags.
 */
enum class renderer_flags : uint32 {
  software = SDL_RENDERER_SOFTWARE,
  accelerated = SDL_RENDERER_ACCELERATED,
  present_vsync = SDL_RENDERER_PRESENTVSYNC,
  target_texture = SDL_RENDERER_TARGETTEXTURE,
};

/**
 * @brief Bitwise operator &.
 * @param lhs The left hand side operand.
 * @param rhs The right hand side operand.
 * @return The window_flag resulted form the bitwise operation.
 */
inline constexpr auto
operator&(renderer_flags const &lhs,
          renderer_flags const &rhs) noexcept -> renderer_flags {
  return static_cast<renderer_flags>(static_cast<uint32>(lhs) &
                                     static_cast<uint32>(rhs));
}

/**
 * @brief Bitwise operator |.
 * @param lhs The left hand side operand.
 * @param rhs The right hand side operand.
 * @return The window_flag resulted form the bitwise operation.
 */
inline constexpr auto
operator|(renderer_flags const &lhs,
          renderer_flags const &rhs) noexcept -> renderer_flags {
  return static_cast<renderer_flags>(static_cast<uint32>(lhs) |
                                     static_cast<uint32>(rhs));
}

/**
 * @brief Renderer flip.
 */
enum class renderer_flip {
  none = SDL_FLIP_NONE,
  horizontal = SDL_FLIP_HORIZONTAL,
  vertical = SDL_FLIP_VERTICAL
};

/**
 * @brief Renderer class.
 * The two dimensions rendering context.
 * This class provides graphical primitives.
 */
class renderer final : public sdl_object {
private:
  SDL_Renderer *mRenderer; ///< The SDL_Renderer object.

public:
  /**
   * @brief Gets the pointer to the managed SDL_Renderer.
   * @return The pointer to the managed SDL_Renderer.
   */
  auto operator()() const noexcept -> SDL_Renderer *;

  /**
   * @brief Secondary constuctor.
   * Construct from existing SDL_Renderer structure.
   * @param renderer The existing SDL_Renderer to manage.
   * @post The renderer_ pointer != nullptr.
   */
  explicit renderer(SDL_Renderer *renderer) noexcept;

  /**
   * @brief Secondary constructor.
   * Creates the renderer from the specified arguments.
   * @param window The window where the rendering is displayed.
   * @param index The index of the rendering driver to initialise.
   * @param flags The flags for SDL_renderer_flags (bitwise operations allowed).
   * @post The renderer_ pointer != nullptr.
   * @throws sdlpp::exception
   */
  renderer(const window &window, const int index, const renderer_flags &flags);

  /**
   * @brief Destructor.
   * @post The renderer_ pointer == nullptr.
   */
  virtual ~renderer();

  /**
   * @brief Move constructor.
   */
  renderer(renderer &&other) noexcept;

  /**
   * @brief Move assignment operator.
   * @return The reference to self.
   */
  auto operator=(renderer &&other) noexcept -> renderer &;

  /**
   * @brief Get information about a rendering context.
   * @return The the informations about the renderer.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto info() -> renderer_info;

  /**
   * @brief Renders a frame.
   * Updates the screen with any rendering performed since the previous call.
   * @return The reference to self.
   */
  auto present() noexcept -> renderer &;

  /**
   * @brief Clears the rendering target.
   * Clears the current rendering target with the drawing colour
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto clear() -> renderer &;

  /**
   * @brief Copy a portion of the texture to the current rendering.
   * If the source Rectangle is not set the entire texture is used.
   * If the destination Rectangle is not set the entire rendering target is set.
   * @param texture The source texture.
   * @param source The source rectangle.
   * @param destination The destination rectangle.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto copy(texture &texture, const maybe<recti> &source = nothing,
            const maybe<recti> &destination = nothing) -> renderer &;

  /**
   * @brief Copy a portion of the texture to the current rendering.
   * The signature is extended with optional rotation and flipping.
   * If the source Rectangle is not set the entire texture is used.
   * If the destination Rectangle is not set the entire rendering target is set.
   * If the center of rotation is not set, it is around destination Rectangle.
   * The rotation angle is in degrees and it's clockwise.
   * @param texture The source texture.
   * @param source The source rectangle.
   * @param destination The destination rectangle.
   * @param angle The angle of the rotation.
   * @param center The point around which destination will be rotated.
   * @param flip The SDL_RendererFlip value to perform on texture.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto copy(texture &texture, const maybe<recti> &source,
            const maybe<recti> &destination, const double angle,
            const maybe<pointi> &center = nothing,
            const renderer_flip flip = renderer_flip::none) -> renderer &;

  /**
   * @brief Gets the colour used for drawing operations (Rect, Line and Clear).
   * @return The current colour.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto draw_colour() const -> colour;

  /**
   * @brief Sets colour user for drawing operations.
   * Sets the colour values and alpha value to draw on the rendering target.
   * @param colour The new colour to be used in drawing operations.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_draw_colour(const colour &colour) -> renderer &;

  /**
   * @brief Gets the blend mode used for drawing operations.
   * @return The blend mode used by the renderer.
   */
  [[nodiscard]] auto draw_blend_mode() const noexcept -> blend_mode;

  /**
   * @brief Sets the blend mode used for drawing operations.
   * @param mode The new blend mode to be used in drawing operations.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto
  set_draw_blend_mode(const blend_mode mode = blend_mode::none) -> renderer &;

  // Better not to implement this for design reasons.
  // It still can be called by C SDL function.
  ///**
  // * @brief Get the current renderer's target.
  // * @return A Texture or nothing if the default target is used.
  // * @note The texture returned IS modifiable!
  // */
  // maybe<SDL_Texture> target() noexcept;

  /**
   * @brief Sets the specified texture as the render target.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_target(texture &texture) -> renderer &;

  /**
   * @brief Gets the clipping rectangle of the renderer.
   * @note If clipping is disabled nothing is returned.
   * @return The clipping rectangle or nothing.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto clip_rect() const noexcept -> maybe<recti>;

  /**
   * @brief Sets the clipping rectangle for the renderer.
   * @param rect The new clipping rectangle.
   * @note To disable clipping pass nothing as parameter.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_clip_rect(const maybe<recti> &rect) -> renderer &;

  /**
   * @brief Gets device independent resolution for rendering.
   * @return The rsi representing the logical resolution.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto logical_size() const noexcept -> rsi;

  /**
   * @brief Sets a device independent resolution for rendering.
   * @param size The new size of the logical resolution.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_logical_size(const rsi &size) -> renderer &;

  [[nodiscard]] auto to_logical(const int realX,
                                const int realY) const noexcept -> pointf {
    pointf logicalPosition{};
    SDL_RenderWindowToLogical(mRenderer, realX, realY, &logicalPosition.x(),
                              &logicalPosition.y());
    return logicalPosition;
  }

  [[nodiscard]] auto
  to_logical(const pointi &realPosition) const noexcept -> pointf {
    return to_logical(realPosition.x(), realPosition.y());
  }

  [[nodiscard]] auto
  from_logical(const float logicalX,
               const float logicalY) const noexcept -> pointi {
    pointi realPosition{};
    SDL_RenderLogicalToWindow(mRenderer, logicalX, logicalY, &realPosition.x(),
                              &realPosition.y());
    return realPosition;
  }

  [[nodiscard]] auto
  from_logical(const pointf &logicalPosition) const noexcept -> pointi {
    return from_logical(logicalPosition.x(), logicalPosition.y());
  }

  /**
   * @brief Gets the drawi wcng scale for the current target.
   * @return The pointf representing the horizonal and vertical scaling
   * factor.
   */
  [[nodiscard]] auto get_scale() const noexcept -> pointf;

  /**
   * @brief Sets the drawing scale for rendering on the current target.
   * @param scale The new scaling factor.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_scale(const pointf &scale) -> renderer &;

  /**
   * @brief Get whether integer scales are forced for resolution-independent
   * rendering.
   * @return True if integer scales are forced or false if not.
   */
  [[nodiscard]] auto uses_integer_scale() const noexcept -> bool;

  /**
   * @brief Sets whether to force integer scales from resolution-independent
   * rendering.
   * @param enabled The bool denoting if integer scaling should be enabled.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_integer_scale(const bool enabled) noexcept -> renderer &;

  /**
   * @brief Gets the drawing area for the current target.
   * @return The recti representing current drawing area.
   */
  [[nodiscard]] auto wiewport() const noexcept -> recti;

  /**
   * @brief Sets the drawing area for rendering on the current target.
   * @param viewport The new drawing area.
   * @note To set the entire target as viewport pass nothing as parameter.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_viewport(const maybe<recti> &viewport) -> renderer &;

  /**
   * @brief Gets the output size of a rendering context.
   * @return The point representing output size.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto get_output_size() const -> rsi;

  /**
   * @brief Checks if the window supports the use of render target.
   * @return True if the render target is supported.
   */
  [[nodiscard]] auto is_target_supported() const noexcept -> bool;

  /**
   * @brief Draws a pixel (point).
   * Draws a point with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param point The point, position where to draw the pixel.
   * @param colour The optional colour of the pixel.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_point(const point<T> &point,
                       const maybe<colour> &colour = nothing) {
    if (colour) {
      set_draw_colour(colour.value());
      if (colour.value().a() == 255u)
        set_draw_blend_mode(blend_mode::none);
    }

    if constexpr (std::is_same_v<T, int>)
      query_status(SDL_RenderDrawPoint(mRenderer, point.x(), point.y()),
                   "SDL_RendererDrawPoint");
    else
      query_status(SDL_RenderDrawPointF(mRenderer, point.x(), point.y()),
                   "SDL_RendererDrawPointF");
    return *this;
  }

  /**
   * @brief Draws multiple pixels (points).
   * Draws a multiple points with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param points The vector of points.
   * @param colour The optional colour of the pixels.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_points(const std::vector<point<T>> &points,
                        const maybe<colour> &colour = nothing) {
    if (colour) {
      set_draw_colour(colour.value());
      if (colour.value().a() == 255u)
        set_draw_blend_mode(blend_mode::none);
    }

    if constexpr (std::is_same_v<T, int>)
      query_status(
          SDL_RenderDrawPoints(mRenderer, (*points.data())(), points.size()),
          "SDL_RendererDrawPoints");
    else
      query_status(
          SDL_RenderDrawPointsF(mRenderer, (*points.data())(), points.size()),
          "SDL_RendererDrawPointsF");
    return *this;
  }

  /**
   * @brief Draws a line.
   * Draws a line with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param p0 The starting point of the line.
   * @param p1 The ending point of the line.
   * @param colour The optional colour of the line.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_line(const point<T> &p0, const point<T> &p1,
                      const maybe<colour> &colour = nothing) {
    if (colour) {
      set_draw_colour(colour.value());
      if (colour.value().a() == 255u)
        set_draw_blend_mode(blend_mode::none);
    }

    if constexpr (std::is_same_v<T, int>)
      query_status(
          SDL_RenderDrawLine(mRenderer, p0.x(), p0.y(), p1.x(), p1.y()),
          "SDL_RendererDrawLine");
    else
      query_status(
          SDL_RenderDrawLineF(mRenderer, p0.x(), p0.y(), p1.x(), p1.y()),
          "SDL_RendererDrawLineF");
    return *this;
  }

  /**
   * @brief Draws multiple lines.
   * Draws a multiple lines with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param points The vector of points.
   * @param colour The optional colour of the lines.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_lines(const std::vector<point<T>> &points,
                       const maybe<colour> &colour = nothing) {
    if (colour) {
      set_draw_colour(colour.value());
      if (colour.value().a() == 255u)
        set_draw_blend_mode(blend_mode::none);
    }

    if constexpr (std::is_same_v<T, int>)
      query_status(
          SDL_RenderDrawLines(mRenderer, (*points.data())(), points.size()),
          "SDL_RendererDrawLines");
    else
      query_status(
          SDL_RenderDrawLinesF(mRenderer, (*points.data())(), points.size()),
          "SDL_RendererDrawLinesF");
    return *this;
  }

  /**
   * @brief Draws a circle.
   * Draws a circle with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param center The point, center of the circle.
   * @param radius The radius of the cirlce.
   * @param colour The optional colour of the circle.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_circle(const point<T> center, const T radius,
                        const maybe<colour> &colour = nothing) {
    auto x0{center.x()};
    auto y0{center.y()};
    auto r{radius};
    auto x{-r}, y{0}, err{2 - 2 * r};
    std::vector<point<T>> points;

    do {
      if constexpr (std::is_same_v<T, int>) {
        points.emplace_back(pointi{xi{x0 - x}, yi{y0 + y}}); // I. Quadrant
        points.emplace_back(pointi{xi{x0 - y}, yi{y0 - x}}); // II. Quadrant
        points.emplace_back(pointi{xi{x0 + x}, yi{y0 - y}}); // III. Quadrant
        points.emplace_back(pointi{xi{x0 + y}, yi{y0 + x}}); // IV. Quadrant
      } else {
        points.emplace_back(pointf{xf{x0 - x}, yf{y0 + y}}); // I. Quadrant
        points.emplace_back(pointf{xf{x0 - y}, yf{y0 - x}}); // II. Quadrant
        points.emplace_back(pointf{xf{x0 + x}, yf{y0 - y}}); // III. Quadrant
        points.emplace_back(pointf{xf{x0 + y}, yf{y0 + x}}); // IV. Quadrant
      }
      r = err;
      if (r <= y)
        err += ++y * 2 + 1; // e_xy+e_y < 0
      if (r > x || err > y)
        err += ++x * 2 + 1; // e_xy+e_x > 0 or no 2nd y-step

    } while (x < 0);
    return draw_points(points, colour);
  }

  /**
   * @brief Draws a solid circle.
   * Draws a solid circle with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param center The point, center of the circle.
   * @param radius The radius of the cirlce.
   * @param colour The optional colour of the circle.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_fill_circle(const point<T> center, const T radius,
                             const maybe<colour> &colour = nothing) {
    if (colour) {
      set_draw_colour(colour.value());
      if (colour.value().a() == 255u)
        set_draw_blend_mode(blend_mode::none);
    }

    auto x0{center.x()};
    auto y0{center.y()};
    auto r{radius};
    auto x{-r}, y{0}, err{2 - 2 * r};

    do {
      if constexpr (std::is_same_v<T, int>) {
        // I. Quadrant
        draw_line(pointi{xi{x0 + x}, yi{y0 + y}},
                  pointi{xi{x0 - x}, yi{y0 + y}});
        // II. Quadrant
        draw_line(pointi{xi{x0 + y}, yi{y0 + x}},
                  pointi{xi{x0 - y}, yi{y0 + x}});
        // III. Quadrant
        draw_line(pointi{xi{x0 + x}, yi{y0 - y}},
                  pointi{xi{x0 - x}, yi{y0 - y}});
        // IV. Quadrant
        draw_line(pointi{xi{x0 + y}, yi{y0 - x}},
                  pointi{xi{x0 - y}, yi{y0 - x}});
      } else {
        // I. Quadrant
        draw_line(pointf{xf{x0 + x}, yf{y0 + y}},
                  pointf{xf{x0 - x}, yf{y0 + y}});
        // II. Quadrant
        draw_line(pointf{xf{x0 + y}, yf{y0 + x}},
                  pointf{xf{x0 - y}, yf{y0 + x}});
        // III. Quadrant
        draw_line(pointf{xf{x0 + x}, yf{y0 - y}},
                  pointf{xf{x0 - x}, yf{y0 - y}});
        // IV. Quadrant
        draw_line(pointf{xf{x0 + y}, yf{y0 - x}},
                  pointf{xf{x0 - y}, yf{y0 - x}});
      }
      r = err;
      if (r <= y)
        err += ++y * 2 + 1; /* e_xy+e_y < 0 */
      if (r > x || err > y)
        err += ++x * 2 + 1; /* e_xy+e_x > 0 or no 2nd y-step */

    } while (x < 0);
    return *this;
  }

  /**
   * @brief Draws a rectangle.
   * Draws a rectangle with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param rect The rectangle to draw.
   * @param colour The optional colour of the rectangle.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_rect(const rect<T> &rect,
                      const maybe<colour> &colour = nothing) {
    if (colour) {
      set_draw_colour(colour.value());
      if (colour.value().a() == 255u)
        set_draw_blend_mode(blend_mode::none);
    }

    if constexpr (std::is_same_v<T, int>)
      query_status(SDL_RenderDrawRect(mRenderer, rect()),
                   "SDL_RendererDrawRect");
    else
      query_status(SDL_RenderDrawRectF(mRenderer, rect()),
                   "SDL_RendererDrawRectF");
    return *this;
  }

  /**
   * @brief Draws a solid rectangle.
   * Draws a solid rectangle with the specified arguments.
   * The colour is optional, not setting it means to use the last colour set.
   * @param rect The rectangle to draw.
   * @param colour The optional colour of the rectangle.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  template <typename T>
  renderer &draw_fill_rect(const rect<T> &rect,
                           const maybe<colour> &colour = nothing) {
    if (colour) {
      set_draw_colour(colour.value());
      if (colour.value().a() == 255u)
        set_draw_blend_mode(blend_mode::none);
    }

    if constexpr (std::is_same_v<T, int>)
      query_status(SDL_RenderFillRect(mRenderer, rect()),
                   "SDL_RendererFillRect");
    else
      query_status(SDL_RenderFillRectF(mRenderer, rect()),
                   "SDL_RendererFillRectF");
    return *this;
  }
};

[[nodiscard]] inline auto render_driver_count() noexcept -> int {
  return SDL_GetNumRenderDrivers();
}

[[nodiscard]] inline auto video_driver_count() noexcept -> int {
  return SDL_GetNumVideoDrivers();
}

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_RENDERER_HPP_
