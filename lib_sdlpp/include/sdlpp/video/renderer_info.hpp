#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_RENDERER_INFO_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_RENDERER_INFO_HPP_

#include <SDL2/SDL_render.h>

#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/video/pixel.hpp"
#include <cassert>

namespace sdlpp {

class renderer;

/**
 * @brief Manages SDL_RendererInfo structure.
 */
class renderer_info final {
private:
  SDL_RendererInfo mInfo;

public:
  /**
   * @brief Secondary constructor.
   * Constructs from SDL_RendererInfo.
   * @param info The SDL_RendererInfo to construct from.
   */
  explicit renderer_info(SDL_RendererInfo &info) noexcept;

  /**
   * @brief Gets the renderer info flags.
   * @return The info flags.
   */
  [[nodiscard]] auto flags() const noexcept -> uint32;

  /**
   * @brief Gets the name of the renderer.
   * @return The std::string with the name of the renderer
   */
  [[nodiscard]] auto name() const noexcept -> std::string;

  /**
   * @brief Gets the number of available texture formats.
   * @return The number of available texture formats.
   */
  [[nodiscard]] auto format_count() const noexcept -> usize;

  /**
   * @brief Gets the specified format form the available texture formats.
   * @param index The index for the array of the available texture formats.
   * @return The requested pixel format.
   */
  [[nodiscard]] auto get_format(const usize index) const -> pixel_format;

  /**
   * @brief Gets the maximum texture size.
   * @return The rsi indicating the maximum texture size.
   */
  [[nodiscard]] auto max_texture_size() const noexcept -> rsi;

  /**
   * @brief Checks if the renderer is a software fallback.
   * @return A boolean indicating if the renderer is a software dallback.
   */
  [[nodiscard]] auto is_software() const noexcept -> bool;

  /**
   * @brief Checks if the renderer hardware acceleration is used.
   * @return A boolean donoting whether hardware acceleration is enabled or not.
   */
  [[nodiscard]] auto is_accelerated() const noexcept -> bool;

  /**
   * @brief Checks if vsync is enabled for the renderer.
   * @return A boolean value indicating whether vsync is enabled or not.
   */
  [[nodiscard]] auto has_vsync() const noexcept -> bool;

  /**
   * @brief Checks if The renderer supports rendering to texture.
   * @return A boolean value indicating if the renderer supports it or not.
   */
  [[nodiscard]] auto has_target_texture() const noexcept -> bool;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_VIDEO_RENDERER_INFO_HPP_
