#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_SURFACE_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_SURFACE_HPP_

#include "sdlpp/common/math/rect.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/sdl_object.hpp"
#include "sdlpp/video/blend.hpp"
#include "sdlpp/video/pixel.hpp"

/**
 * @author Nicolò Plebani
 * @file surface.hpp
 */

struct SDL_Surface;
struct SDL_PixelFormat;

namespace sdlpp {

class colour;

/**
 * @brief Class surface.
 * Image stored in system memory with drecti access to pixel data.
 */
class surface final : sdl_object {
public:
  /**
   * @brief Class lock_handle
   * For drecti pixel access, SDL surface may need to be locked.
   * This class represents the lock and controls its lifetime as the lock is
   * released as soon as LockHandle is destroyed.
   */
  class lock_handle final : sdl_object {
    friend class surface;

  private:
    surface *mSurface; ///< surface this lock belongs to.

    /**
     * @brief Secondary constructor.
     * Creates the lock for the specified surface.
     */
    explicit lock_handle(surface *surface);

  public:
    /**
     * @brief Default constructor.
     * Creates a non operating lock, it may be initialised.
     */
    lock_handle();

    /**
     * @brief Destructor.
     * Releases the lock.
     */
    ~lock_handle();

    /**
     * @brief Move constructor.
     */
    lock_handle(lock_handle &&other) noexcept;

    /**
     * @brief Move assignment operator.
     */
    auto operator=(lock_handle &&other) noexcept -> lock_handle &;

    /**
     * @brief Gets the raw pixel data of locked region.
     * @return The pointer to raw pixel data of locked region.
     */
    [[nodiscard]] auto pixels() const -> void *;

    /**
     * @brief Gets pitch of locked pixel data.
     * @return The number of bytes in a row of pixel data, including padding
     * between the lines.
     */
    [[nodiscard]] auto pitch() const -> int;

    /**
     * @brief Gets pixel format of the surface.
     * @return Format of the pixels stored in the surface.
     */
    [[nodiscard]] auto format_info() const noexcept -> pixel_format_info;
  };

private:
  SDL_Surface *mSurface; ///< The managed SDL_Surface.

public:
  /**
   * @brief Gets pointer to the managed SDL_Surface structure.
   * @return The pointer to the managed SDL_Surface structure.
   */
  auto operator()() const noexcept -> SDL_Surface *;

  surface() = delete;

  /**
   * @brief Secondary constructor.
   * Constructs from existing SDL_Surface structure.
   * @param surface The SDL_Surface to construct from.
   */
  explicit surface(SDL_Surface *surface);

  /**
   * @brief Secondary constructor.
   * Creates a RGB surface with the specified format.
   * @param size The size of the surface.
   * @param format The pixel format that the surface must use.
   */
  surface(const rsi &size, const pixel_format &format);

  /**
   * @brief Secondary constructor.
   * Creates surface by loading it from the specified file.
   * @param filePath The path to an image file.
   */
  explicit surface(const std::string &filePath);

  /**
   * @brief Destructor.
   * Frees the managed SDL surface.
   */
  virtual ~surface();

  /**
   * @brief Move constructor.
   */
  surface(surface &&other) noexcept;

  /**
   * @brief Move assignment operator.
   */
  surface &operator=(surface &&other) noexcept;

  /**
   * @brief Copies an existing surface to a new surface of the specified format.
   * @param format The new pixel format the new surface must use.
   */
  [[nodiscard]] auto convert_to(const pixel_format format) const -> surface;

  /**
   * @brief Locks the surface for drecti pixel access.
   * @return The lock_handle used to access pixel data and to control lock
   * lifetime.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto lock() -> lock_handle;

  /**
   * @brief Gets the surface format.
   * @return The surface pixel format.
   */
  [[nodiscard]] auto format() const noexcept -> pixel_format;

  /**
   * @brief Gets the clipping rectangle of the surface.
   * @return The clipping rect of the surface.
   */
  [[nodiscard]] auto clip() const -> recti;

  /**
   * @brief Sets the clipping rect for the surface.
   * @param rect The rect representing the clipping or nothing.
   * @note To disable clipping pass nothing as parameter.
   * @return The reference to self.
   */
  auto set_clip(const maybe<recti> &rect) -> surface &;

  /**
   * @brief Gets the surface size.
   * @return The rsi representing surface dimensions in pixels.
   */
  [[nodiscard]] auto size() const noexcept -> rsi;

  /**
   * @brief Gets the additional colour value multiplied into blit operations.
   * @return The colour values used to do blit operations.
   */
  [[nodiscard]] auto colour_mod() const -> colour;

  /**
   * @brief Sets an additional color value multiplied into blit operations.
   * @param colour The colour to be multiplied into blit operations.
   * @return The reference to self.
   */
  auto set_colour_mod(const colour &colour) -> surface &;

  /**
   * @brief Sets an additional alpha value used in blit operations.
   * @param alpha The alpha value multiplied into blit operations.
   * @return The reference to self.
   */
  auto set_alpha_mod(const uint8 alpha) -> surface &;

  /**
   * @brief Gets the blend mode used in blit operations.
   * @return The blend mode used un blit operations.
   */
  [[nodiscard]] auto blend_mode() const noexcept -> blend_mode;

  /**
   * @brief Sets the blend mode used for blit operations.
   * @param mode The blend mode to use for blit blending.
   * @return The reference to self.
   */
  auto set_blend_mode(const enum blend_mode mode) -> surface &;

  /**
   * @brief Checks if RLE acceleration is enabled.
   * @return A boolean value indicating if it is enabled or not.
   */
  [[nodiscard]] auto has_rle() const noexcept -> bool;

  /**
   * @brief Sets the RLE acceleration hint for a surface.
   * @param enabled The boolean value indicating to enable or not rle.
   * @return The reference to self.
   */
  auto set_rle(const bool enabled) -> surface &;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_VIDEO_SURFACE_HPP_
