#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_TEXTURE_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_TEXTURE_HPP_

#include <SDL2/SDL_render.h>

#include "sdlpp/common/math/rect.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/sdl_object.hpp"
#include "sdlpp/video/blend.hpp"
#include "sdlpp/video/pixel.hpp"

/**
 * @author Nicolò Plebani
 * @file texture.hpp
 */

struct SDL_Texture;

namespace sdlpp {

enum class texture_access {
  non_lockable =
      SDL_TEXTUREACCESS_STATIC, ///< Texture changes rarely and isn't lockable.
  streaming = SDL_TEXTUREACCESS_STREAMING, ///< Texture changes frequently and
                                           ///< is lockable.
  target = SDL_TEXTUREACCESS_TARGET ///< Texture can be used as a render target.
};

enum class scale_mode {
  nearest = SDL_ScaleModeNearest, ///< Nearest pixel sampling.
  linear = SDL_ScaleModeLinear,   ///< Linear filtering.
  best = SDL_ScaleModeBest        ///< Anisotropic filtering.
};

class renderer;

/**
 * @brief Texture class.
 * Handles SDL2_Texture object.
 */
class texture final : public sdl_object {
public:
  /**
   * @brief Class lock_handle
   * Textures with SDL_TEXTUREACCESS_STREAMING access mode may be locked which
   * provides (writeonly) access to their raw pixel data.
   * This class represents the lock and controls its lifetime as the lock is
   * released as soon as LockHandle is destroyed.
   */
  class lock_handle final : sdl_object {
    friend class texture;

  private:
    texture *mTexture; ///< texture this lock belongs to.
    void *mPixels;     ///< Pointer to raw pixel data of locked region
    int mPitch; ///< Number of bytes in a row of pixel data, including padding
                ///< between lines

  private:
    /**
     * @brief Secondary constructor.
     * Creates the lock for the specified texture.
     * @param rect The rect indicating region to lock.
     */
    lock_handle(texture *texture, const maybe<recti> &rect);

  public:
    /**
     * @brief Default constructor.
     * Creates a non operating lock, it may be initialised.
     */
    lock_handle();

    /**
     * @brief Destructor.
     * Releases the lock.
     */
    ~lock_handle();

    /**
     * @brief Move constructor.
     */
    lock_handle(lock_handle &&other) noexcept;

    /**
     * @brief Move assignment operator.
     */
    auto operator=(lock_handle &&other) noexcept -> lock_handle &;

    /**
     * @brief Gets the raw pixel data of locked region.
     * @return The pointer to raw pixel data of locked region.
     */
    auto pixels() const -> void *;

    /**
     * @brief Gets pitch of locked pixel data.
     * @return The number of bytes in a row of pixel data, including padding
     * between the lines.
     */
    [[nodiscard]] auto pitch() const -> int;
  };

private:
  SDL_Texture *mTexture; ///< The SDL_texture object;

public:
  /**
   * @brief Gets the pointer to the managed SDL_Texture.
   * @return The pointer to the managed SDL_Texture.
   */
  auto operator()() const noexcept -> SDL_Texture *;

  /**
   * @brief Default constrcutor.
   */
  texture() = delete;

  /**
   * @brief Secondary constrcutor.
   * Construct from existing SDL_Texture structure.
   * @param texture The existing SDL_Texture to manage.
   */
  explicit texture(SDL_Texture *texture);

  /**
   * @brief Secondary constrcutor.
   * Creates a texture by loading it from file.
   * @param renderer The reference to the renderer.
   * @param path The filename path to an image file.
   */
  texture(renderer &renderer, const std::string &path);

  /**
   * @brief Destructor
   * @post The window_ pointer == nullptr.
   */
  virtual ~texture();

  /**
   * @brief Move constructor.
   */
  texture(texture &&other) noexcept;

  /**
   * @brief Move assignment operator.
   * @return The reference to self.
   */
  auto operator=(texture &&other) noexcept -> texture &;

  /**
   * @brief Locks the texture for write-only pixel access.
   * @param rect The rect representing area to lock for access or nothing.
   * @note To lock the entire texture pass nothing as parameter.
   * @return The lock_handle used to access pixel data and to control lock
   * lifetime.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto lock(const maybe<recti> &rect) -> lock_handle;

  /**
   * @brief Gets the texture format.
   * @return The texture pixel format.
   */
  [[nodiscard]] auto format() const noexcept -> pixel_format;

  [[nodiscard]] auto access() const noexcept -> texture_access;

  /**
   * @brief Gets the texture size.
   * @return The rsi representing texture dimensions in pixels.
   */
  [[nodiscard]] auto size() const noexcept -> rsi;

  /**
   * @brief Gets the additional colour value multiplied into blit operations.
   * @return The colour values used to do blit operations.
   */
  [[nodiscard]] auto colour_mod() const -> colour;

  /**
   * @brief Sets an additional color value multiplied into blit operations.
   * @param colour The colour to be multiplied into blit operations.
   * @return The reference to self.
   */
  auto set_colour_mod(const colour &colour) -> texture &;

  /**
   * @brief Sets an additional alpha value used in blit operations.
   * @param alpha The alpha value multiplied into blit operations.
   * @return The reference to self.
   */
  auto set_alpha_mod(const uint8 alpha) -> texture &;

  /**
   * @brief Gets the blend mode used in blit operations.
   * @return The blend mode used un blit operations.
   */
  [[nodiscard]] auto blend_mode() const noexcept -> blend_mode;

  /**
   * @brief Sets the blend mode used for blit operations.
   * @param mode The blend mode to use for blit blending.
   * @return The reference to self.
   */
  auto set_blend_mode(const enum blend_mode mode) -> texture &;
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_VIDEO_TEXTURE_HPP_
