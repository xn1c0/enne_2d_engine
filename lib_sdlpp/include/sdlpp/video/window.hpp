#ifndef LIB_SDLPP_INCLUDE_SDLPP_VIDEO_WINDOW_HPP_
#define LIB_SDLPP_INCLUDE_SDLPP_VIDEO_WINDOW_HPP_

#include <SDL2/SDL_stdinc.h>
#include <SDL2/SDL_video.h>

#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/sdl_object.hpp"
#include <cassert>
#include <string>

/**
 * @author Nicolò Plebani
 * @file window.hpp
 * @brief This file contains the window class.
 */

struct SDL_Window;

namespace sdlpp {

/**
 * @brief Window Flags.
 */
enum class window_flags : uint32 {
  none = 0,
  fullscreen = SDL_WINDOW_FULLSCREEN,
  fullscreen_desktop = SDL_WINDOW_FULLSCREEN_DESKTOP,
  opengl = SDL_WINDOW_OPENGL,
  vulkan = SDL_WINDOW_VULKAN,
  hidden = SDL_WINDOW_HIDDEN,
  borderless = SDL_WINDOW_BORDERLESS,
  resizable = SDL_WINDOW_RESIZABLE,
  minimised = SDL_WINDOW_MINIMIZED,
  maximised = SDL_WINDOW_MAXIMIZED,
  grabbed = SDL_WINDOW_INPUT_GRABBED,
  allow_high_dpi = SDL_WINDOW_ALLOW_HIGHDPI,
};

/**
 * @brief Bitwise operator &.
 * @param lhs The left hand side operand.
 * @param rhs The right hand side operand.
 * @return The window_flag resulted form the bitwise operation.
 */
[[nodiscard]] inline constexpr auto
operator&(window_flags const &lhs,
          window_flags const &rhs) noexcept -> window_flags {
  return static_cast<window_flags>(static_cast<uint32>(lhs) &
                                   static_cast<uint32>(rhs));
}

/**
 * @brief Bitwise operator |.
 * @param lhs The left hand side operand.
 * @param rhs The right hand side operand.
 * @return The window_flag resulted form the bitwise operation.
 */
[[nodiscard]] inline constexpr auto
operator|(window_flags const &lhs,
          window_flags const &rhs) noexcept -> window_flags {
  return static_cast<window_flags>(static_cast<uint32>(lhs) |
                                   static_cast<uint32>(rhs));
}

/**
 * @brief Window full screen flags.
 */
enum class fullscreen_flags : uint32 {
  windowed = 0,
  fllscreen = SDL_WINDOW_FULLSCREEN,
  fullscreen_desktop = SDL_WINDOW_FULLSCREEN_DESKTOP
};

class surface;

/**
 * @brief Class window.
 * Handles SDL2_window object.
 */
class window final : public sdl_object {
private:
  SDL_Window *mWindow; ///< The SDL_window object.

public:
  static constexpr point position_centered =
      pointi{xi{SDL_WINDOWPOS_CENTERED}, yi{SDL_WINDOWPOS_CENTERED}};

  static constexpr point position_undefined =
      pointi{xi{SDL_WINDOWPOS_UNDEFINED}, yi{SDL_WINDOWPOS_UNDEFINED}};

  /**
   * @brief Gets the pointer to the managed SDL_Window.
   * @return The pointer to the managed SDL_Window.
   */
  [[nodiscard]] auto operator()() const noexcept -> SDL_Window *;

  /**
   * @brief Secondary constuctor.
   * Construct from existing SDL_window structure.
   * @param window The existing SDL_window to manage.
   * @post The window_ pointer != nullptr.
   */
  explicit window(SDL_Window *window) noexcept;

  /**
   * @brief Secondary constructor.
   * Creates the window with the specified arguments.
   * @param title The title of the window.
   * @param position The position of the window.
   * @param size The size of the widnow (point indicating width and height).
   * @param flags The flags for SDL_window_flags (bitwise operations allowed).
   * @post The window_ pointer != nullptr.
   * @throws sdlpp::exception
   */
  window(const std::string &title, const pointi &position, const rsi &size,
         const window_flags flags);

  /**
   * @brief Destructor
   * @post The window_ pointer == nullptr.
   */
  virtual ~window();

  /**
   * @brief Move constructor.
   */
  window(window &&other) noexcept;

  /**
   * @brief Move assignment operator.
   * @return The reference to self.
   */
  window &operator=(window &&other) noexcept;

  /**
   * @brief Gets the window's identifier.
   * @return The window's id.
   */
  [[nodiscard]] auto id() const noexcept -> uint32;

  /**
   * @brief Gets the window size.
   * @return The size of the window.
   */
  [[nodiscard]] auto size() const noexcept -> rsi;

  /**
   * @brief Gets the drawable size of the window.
   * @return The drawable size of the window.
   */
  [[nodiscard]] auto drawable_size() const noexcept -> rsi;

  /**
   * @brief Sets the title of the window.
   * @param title The new title of the window.
   * @return The reference to self.
   */
  auto set_title(const std::string &title) noexcept -> window &;

  /**
   * @brief Gets the title of the window.
   * @return A string which contains the title of the window.
   */
  [[nodiscard]] auto title() const noexcept -> std::string;

  /**
   * @brief Maximises the window (as large as possible).
   * @returns The reference to self.
   */
  auto maximise() noexcept -> window &;

  /**
   * @brief Minimises the window to an icon on the panel.
   * @returns The reference to self.
   */
  auto minimise() noexcept -> window &;

  /**
   * @brief Hides the window.
   * @returns The reference to self.
   */
  auto hide() noexcept -> window &;

  /**
   * @brief Restores the window.
   * Restore the size and the position of a minimised or maximised window.
   * @returns The reference to self.
   */
  auto restore() noexcept -> window &;

  /**
   * @brief Raises a window above other windows and set the input focus.
   * @returns The reference to self.
   */
  auto raise() noexcept -> window &;

  /**
   * @brief Shows the window.
   * @returns The reference to self.
   */
  auto show() noexcept -> window &;

  /**
   * @brief Sets the window's fullscreen state.
   * Default SDL flags.
   * @param flags The SDL_WINDOW_FULLSCREEN_<VALUE> or 0.
   * @returns The reference to self.
   * @throws sdlpp::exception
   */
  auto set_fullscreen(uint32 flags) -> window &;

  /**
   * @brief Sets the window's fullscreen state.
   * Custom enum flags.
   * @param flags The SDL_WINDOW_FULLSCREEN_<VALUE> or 0.
   * @returns The reference to self.
   * @throws sdlpp::exception
   */
  auto set_fullscreen(fullscreen_flags flags) -> window &;

  /**
   * @brief Sets the window's size in a client area.
   * @param size The point representing the new window dimension.
   * @note Unit of measure is pixel.
   * @returns The reference to self.
   */
  auto set_size(const rsi &size) noexcept -> window &;

  /**
   * @brief Gets the brightness of the display owning the window.
   * Brightness is the gamma multiplier.
   * @returns The brightness in range [0.0, 1.0] (dark, bright).
   */
  [[nodiscard]] auto brightness() const noexcept -> float;

  /**
   * @brief Sets the brightness of the display owning the window.
   * Brightness is the gamma multiplier.
   * @param brightness The new brightness value.
   * @note The brightness value (0.0f - completely dark, 1.0 - opaque).
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_brightness(float brightness) -> window &;

  /**
   * @brief Gets the window's position.
   * @return The point representing the window position.
   */
  [[nodiscard]] auto position() const noexcept -> pointi;

  /**
   * @brief Sets the position of the window.
   * As the coordinates are allowed SDL_WINOWPOS_<VALUE> constant values.
   * @param position The point representing the new window position.
   * @return The reference to self.
   */
  auto set_position(const pointi &position) noexcept -> window &;

  /**
   * @brief Gets the minimum size for the window in the client area.
   * @return point The point representing the minimum size.
   */
  [[nodiscard]] auto minimum_size() const noexcept -> rsi;

  /**
   * @brief Sets the minimum size of the window in the client area.
   * @param size The new minimum size of the window.
   * @note Unit of measure is pixel.
   * @return The reference to self.
   */
  auto set_minimum_size(const rsi &size) noexcept -> window &;

  /**
   * @brief Gets the minimum size for the window in the client area.
   * @return point The point representing the minimum size.
   */
  [[nodiscard]] auto maximum_size() const noexcept -> rsi;

  /**
   * @brief Sets the maximum size of the window in the client area.
   * @param size The new maximum size of the window.
   * @note Unit of measure is pixel.
   * @return The reference to self.
   */
  auto set_maximum_size(const rsi &size) noexcept -> window &;

  /**
   * @brief Checks if this wivoidndow is grabbed.
   * @return True if the window is grabbed.
   */
  [[nodiscard]] auto is_grabbed() const noexcept -> bool;

  /**
   * @brief Changes the grabbed state of the window.
   * @param grabbed The bool denoting if the window should be grabbed.
   * @return The reference to self.
   */
  auto set_grabbed(const bool grabbed) noexcept -> window &;

  /**
   * @brief Gets the index of the display running the window.
   * @returns The index of the voiddisplay containing the center of the
   * window.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto display_index() const -> int;

  /**
   * @brief Gets the display mode to use when a window is visible at
   * fullscreen.
   * @return The SDL_DisplayMode if the call succeeded.
   * @throws sdlpp::exception
   */
  [[nodiscard]] auto display_mode() const -> SDL_DisplayMode;

  /**
   * @brief Sets the display mode to use when the window is at fullscreen.
   * @param mode The SDL_DisplayMode denoting the window's new display mode.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_display_mode(SDL_DisplayMode const &mode) -> window &;

  /**
   * @brief Sets the display mode to match dimension of the window and the
   * refresh rate of the desktop.
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_display_mode() -> window &;

  /**
   * @brief Gets the window flags.
   * @return The mask representing the SDL_window_flags associated to the
   * window.
   */
  [[nodiscard]] auto flags() const noexcept -> window_flags;

  /**
   * @brief Stets the border state of the window.
   * @param bordered True if the window should be bordered.
   * @return The reference to self.
   */
  auto set_bordered(const bool bordered = true) -> window &;

  /**
   * @brief Gets the opacity of the window.
   * @return The opacity value (0.0f - transparent, 1.0f - opaque).
   */
  [[nodiscard]] auto opacity() const noexcept -> float;

  /**
   * @brief Set the opacity of the window.
   * @param opacity The new opacity value.
   * @note The opacity value (0.0f - transparent, 1.0f - opaque)
   * @return The reference to self.
   * @throws sdlpp::exception
   */
  auto set_opacity(float opacity = 1.0f) -> window &;

  /**
   * @brief Sets the resizable state of the window (by the user).
   * @param resizable True if the window is resizable.
   * @return The reference to self.
   */
  auto set_resizable(const bool resizable = true) -> window &;

  /**
   * @brief Sets the icon of the window.
   * @param icon The surface containing the new icon.
   */
  window &set_icon(const surface &icon);
};

} // namespace sdlpp

#endif // LIB_SDLPP_INCLUDE_SDLPP_VIDEO_WINDOW_HPP_
