#include <SDL2/SDL.h>

#include "sdlpp/common/exception.hpp"
#include <sstream>

namespace sdlpp {

std::string exception::function() const noexcept { return function_; }

std::string exception::error() const noexcept { return error_; }

std::string exception::make_what(const char *function, const char *error) {
  std::ostringstream oss;
  oss << function << " failed: " << error;
  return oss.str();
}

exception::exception(const char *function)
    : std::runtime_error(make_what(function, SDL_GetError())),
      function_(function), error_(SDL_GetError()) {}

} // namespace sdlpp
