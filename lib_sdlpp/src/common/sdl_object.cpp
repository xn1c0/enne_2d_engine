#include "sdlpp/common/sdl_object.hpp"
#include "sdlpp/common/exception.hpp"

namespace sdlpp {

void sdl_object::query_status(int result, const char *function) const {
  if (result != 0) {
    throw exception{function};
  }
}

} // namespace sdlpp
