#include "sdlpp/events/window_event.hpp"

namespace sdlpp {

window_event::window_event() : event{event_type::window} {}

window_event::window_event(const SDL_WindowEvent &e) noexcept : event{e} {}

auto window_event::event_id() const noexcept -> window_event_id {
  return static_cast<window_event_id>(mEvent.event);
}

auto window_event::set_event_id(const window_event_id id) noexcept -> void {
  mEvent.event = static_cast<uint8>(id);
}

auto window_event::data1() const noexcept -> int32 { return mEvent.data1; }

auto window_event::set_data1(const int32 value) noexcept -> void {
  mEvent.data1 = value;
}

auto window_event::data2() const noexcept -> int32 { return mEvent.data2; }

auto window_event::set_data2(const int32 value) noexcept -> void {
  mEvent.data2 = value;
}

} // namespace sdlpp
