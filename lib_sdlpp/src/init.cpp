#include <SDL2/SDL.h>

#include "sdlpp/common/utils.hpp"
#include "sdlpp/init.hpp"

namespace sdlpp {

sdl::sdl(const init_flags flags) {
  query_status(SDL_Init(to_underlying(flags)), "SDL_Init");
}

sdl::~sdl() noexcept { SDL_Quit(); }

auto sdl::is_initialized(const init_flags flags) noexcept -> init_flags {
  return static_cast<init_flags>(SDL_WasInit(to_underlying(flags)));
}

auto sdl::init_subsystem(const init_flags flags) -> void {
  query_status(SDL_InitSubSystem(to_underlying(flags)), "SDL_InitSubsystem");
}

auto sdl::quit_subsystem(const init_flags flags) noexcept -> void {
  SDL_QuitSubSystem(to_underlying(flags));
}

} // namespace sdlpp
