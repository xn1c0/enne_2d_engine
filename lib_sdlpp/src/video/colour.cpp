#include <sdlpp/video/colour.hpp>

namespace sdlpp {

std::ostream &operator<<(std::ostream &os, const colour &colour) noexcept {
  os << to_string(colour);
  return os;
}

}
