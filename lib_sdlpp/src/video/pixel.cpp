#include "sdlpp/video/pixel.hpp"
#include "sdlpp/common/exception.hpp"
#include "sdlpp/video/colour.hpp"
#include <SDL_pixels.h>
#include <cassert>

namespace sdlpp {

auto pixel_format_info::operator()() const noexcept -> SDL_PixelFormat * {
  return mFormat;
}

pixel_format_info::pixel_format_info(SDL_PixelFormat *format)
    : mFormat{nullptr} {
  assert(format);
  mFormat = format;
}

pixel_format_info::pixel_format_info(const pixel_format format)
    : mFormat{nullptr} {
  SDL_PixelFormat *const tmp{SDL_AllocFormat(to_underlying(format))};
  if (tmp)
    mFormat = tmp;
  else
    throw exception{"SDL_AllocFormat"};
}

pixel_format_info::~pixel_format_info() {
  if (mFormat != nullptr)
    SDL_FreeFormat(mFormat);
}

pixel_format_info::pixel_format_info(pixel_format_info &&other) noexcept
    : mFormat{nullptr} {
  *this = std::move(other);
}

auto pixel_format_info::operator=(pixel_format_info &&other) noexcept
    -> pixel_format_info & {
  if (this != &other) {
    if (mFormat != nullptr)
      SDL_FreeFormat(mFormat);
    mFormat = other.mFormat;
    other.mFormat = nullptr;
  }
  return *this;
}

auto pixel_format_info::pixel_to_rgba(const uint32 pixel) const noexcept
    -> colour {
  uint8 red{}, green{}, blue{}, alpha{};
  SDL_GetRGBA(pixel, mFormat, &red, &green, &blue, &alpha);
  return {red, green, blue, alpha};
}

auto pixel_format_info::format() const noexcept -> pixel_format {
  return static_cast<pixel_format>(mFormat->format);
}

auto pixel_format_info::name() const noexcept -> std::string {
  const char *tmp = SDL_GetPixelFormatName(mFormat->format);
  return std::string(tmp);
}

} // namespace sdlpp
