#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include <SDL_blendmode.h>

#include "sdlpp/common/exception.hpp"
#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/utils.hpp"
#include "sdlpp/video/colour.hpp"
#include "sdlpp/video/renderer.hpp"
#include "sdlpp/video/texture.hpp"
#include "sdlpp/video/window.hpp"

namespace sdlpp {

auto renderer::operator()() const noexcept -> SDL_Renderer * {
  return mRenderer;
}

renderer::renderer(SDL_Renderer *renderer) noexcept : mRenderer{nullptr} {
  assert(renderer);
  mRenderer = renderer;
}

renderer::renderer(const window &window, const int index,
                   const renderer_flags &flags)
    : mRenderer{nullptr} {
  SDL_Renderer *const tmp =
      SDL_CreateRenderer(window(), index, to_underlying(flags));
  if (tmp)
    mRenderer = tmp;
  else
    throw exception("SDL_CreateRenderer");
}

renderer::~renderer() {
  if (mRenderer)
    SDL_DestroyRenderer(mRenderer);
}

renderer::renderer(renderer &&other) noexcept : mRenderer{nullptr} {
  *this = std::move(other);
}

auto renderer::operator=(renderer &&other) noexcept -> renderer & {
  if (this != &other) {
    if (mRenderer != nullptr)
      SDL_DestroyRenderer(mRenderer);
    mRenderer = other.mRenderer;
    other.mRenderer = nullptr;
  }
  return *this;
}

auto renderer::info() -> renderer_info {
  SDL_RendererInfo info{};
  query_status(SDL_GetRendererInfo((*this)(), &info), "SDL_GetRendererInfo");
  return renderer_info{info};
}

auto renderer::present() noexcept -> renderer & {
  SDL_RenderPresent(mRenderer);
  return *this;
}

auto renderer::clear() -> renderer & {
  query_status(SDL_RenderClear(mRenderer), "SDL_RenderClear");
  return *this;
}

renderer &renderer::copy(texture &texture, const maybe<recti> &source,
                         const maybe<recti> &destination) {
  const SDL_Rect *src = source ? source.value()() : nullptr;
  const SDL_Rect *dst = destination ? destination.value()() : nullptr;
  query_status(SDL_RenderCopy(mRenderer, texture(), src, dst),
               "SDL_RendererCopy");
  return *this;
}

auto renderer::copy(texture &texture, const maybe<recti> &source,
                    const maybe<recti> &destination, const double angle,
                    const maybe<pointi> &center,
                    const renderer_flip flip) -> renderer & {
  const SDL_Rect *src = source ? source.value()() : nullptr;
  const SDL_Rect *dst = destination ? destination.value()() : nullptr;
  const SDL_Point *c = center ? center.value()() : nullptr;
  auto f = static_cast<SDL_RendererFlip>(flip);
  query_status(SDL_RenderCopyEx(mRenderer, texture(), src, dst, angle, c, f),
               "SDL_RendererCopyEx");
  return *this;
}

auto renderer::draw_colour() const -> colour {
  colour colour{};
  query_status(SDL_GetRenderDrawColor(mRenderer, &colour.r(), &colour.g(),
                                      &colour.b(), &colour.a()),
               "SDL_GetRenderDrawColor");
  return colour;
}

auto renderer::set_draw_colour(const colour &colour) -> renderer & {
  query_status(SDL_SetRenderDrawColor(mRenderer, colour.r(), colour.g(),
                                      colour.b(), colour.a()),
               "SDL_SetRendererDrawColor");
  return *this;
}

auto renderer::draw_blend_mode() const noexcept -> blend_mode {
  SDL_BlendMode mode{};
  query_status(SDL_GetRenderDrawBlendMode(mRenderer, &mode),
               "SDL_GetRenderDrawBlendMode");
  return static_cast<blend_mode>(mode);
}

auto renderer::set_draw_blend_mode(const blend_mode mode) -> renderer & {
  query_status(
      SDL_SetRenderDrawBlendMode(mRenderer, static_cast<SDL_BlendMode>(mode)),
      "SDL_SetRenderDrawBlendMode");
  return *this;
}

auto renderer::set_target(texture &texture) -> renderer & {
  query_status(SDL_SetRenderTarget(mRenderer, texture()),
               "SDL_SetRenderTarget");
  return *this;
}

auto renderer::clip_rect() const noexcept -> maybe<recti> {
  recti rect{};
  SDL_RenderGetClipRect(mRenderer, rect());
  if (rect.size().has_area())
    return rect;
  else
    return nothing;
}

auto renderer::set_clip_rect(const maybe<recti> &rect) -> renderer & {
  query_status(
      SDL_RenderSetClipRect(mRenderer, rect ? rect.value()() : nullptr),
      "SDL_RenderSetClipRect");
  return *this;
}

auto renderer::logical_size() const noexcept -> rsi {
  rsi size;
  SDL_RenderGetLogicalSize(mRenderer, &size.w(), &size.h());
  return size;
}

auto renderer::set_logical_size(const rsi &size) -> renderer & {
  query_status(SDL_RenderSetLogicalSize(mRenderer, size.w(), size.h()),
               "SDL_RenderSetLogicalSize");
  return *this;
}

auto renderer::get_scale() const noexcept -> pointf {
  pointf scale;
  SDL_RenderGetScale(mRenderer, &scale.x(), &scale.y());
  return scale;
}

auto renderer::set_scale(const pointf &scale) -> renderer & {
  query_status(SDL_RenderSetScale(mRenderer, scale.x(), scale.y()),
               "SDL_RenderSetScale");
  return *this;
}

auto renderer::uses_integer_scale() const noexcept -> bool {
  return SDL_RenderGetIntegerScale(mRenderer);
}

auto renderer::set_integer_scale(const bool enabled) noexcept -> renderer & {
  query_status(
      SDL_RenderSetIntegerScale(mRenderer, enabled ? SDL_TRUE : SDL_FALSE),
      "SDL_RendererSetIntegerScale");
  return *this;
}

auto renderer::wiewport() const noexcept -> recti {
  recti rect;
  SDL_RenderGetViewport(mRenderer, rect());
  return rect;
}

auto renderer::set_viewport(const maybe<recti> &viewport) -> renderer & {
  query_status(
      SDL_RenderSetViewport(mRenderer, viewport ? viewport.value()() : nullptr),
      "SDL_RenderSetViewport");
  return *this;
}

auto renderer::get_output_size() const -> rsi {
  rsi size;
  query_status(SDL_GetRendererOutputSize(mRenderer, &size.w(), &size.h()),
               "SDL_GetRendererOutputSize");
  return size;
}

auto renderer::is_target_supported() const noexcept -> bool {
  return SDL_RenderTargetSupported(mRenderer) == SDL_TRUE;
}

} // namespace sdlpp
