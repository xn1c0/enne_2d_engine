#include "sdlpp/video/renderer_info.hpp"
#include "sdlpp/common/exception.hpp"

namespace sdlpp {

renderer_info::renderer_info(SDL_RendererInfo &info) noexcept : mInfo{info} {}

auto renderer_info::flags() const noexcept -> uint32 { return mInfo.flags; }

auto renderer_info::name() const noexcept -> std::string {
  return {mInfo.name};
}

auto renderer_info::format_count() const noexcept -> usize {
  return static_cast<usize>(mInfo.num_texture_formats);
}

auto renderer_info::get_format(const usize index) const -> pixel_format {
  if (index < format_count()) {
    return static_cast<pixel_format>(mInfo.texture_formats[index]);
  } else {
    throw exception{"Invalid pixel format index!"};
  }
}

auto renderer_info::max_texture_size() const noexcept -> rsi {
  return {wi{mInfo.max_texture_width}, hi{mInfo.max_texture_height}};
}

auto renderer_info::is_software() const noexcept -> bool {
  return flags() & SDL_RENDERER_SOFTWARE;
}

auto renderer_info::is_accelerated() const noexcept -> bool {
  return flags() & SDL_RENDERER_ACCELERATED;
}

auto renderer_info::has_vsync() const noexcept -> bool {
  return flags() & SDL_RENDERER_PRESENTVSYNC;
}

auto renderer_info::has_target_texture() const noexcept -> bool {
  return flags() & SDL_RENDERER_TARGETTEXTURE;
}

} // namespace sdlpp
