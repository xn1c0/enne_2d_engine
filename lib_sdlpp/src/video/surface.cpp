#include <SDL2/SDL_image.h>
#include <SDL2/SDL_surface.h>

#include "sdlpp/common/exception.hpp"
#include "sdlpp/video/colour.hpp"
#include "sdlpp/video/pixel.hpp"
#include "sdlpp/video/surface.hpp"
#include <cassert>

namespace sdlpp {

surface::lock_handle::lock_handle() : mSurface{nullptr} {}

surface::lock_handle::lock_handle(surface *surface) : mSurface(surface) {
  if (SDL_MUSTLOCK((*mSurface)())) {
    if (SDL_LockSurface((*mSurface)()))
      throw exception{"SDL_LockSurface"};
  }
}

surface::lock_handle::lock_handle(surface::lock_handle &&other) noexcept {
  *this = std::move(other);
}

auto surface::lock_handle::operator=(surface::lock_handle &&other) noexcept
    -> surface::lock_handle & {
  if (this != &other) {
    if (mSurface != nullptr)
      if (SDL_MUSTLOCK((*mSurface)()))
        SDL_UnlockSurface((*mSurface)());
    mSurface = other.mSurface;
    other.mSurface = nullptr;
  }
  return *this;
}

surface::lock_handle::~lock_handle() {
  if (mSurface != nullptr) {
    if (SDL_MUSTLOCK((*mSurface)()))
      SDL_UnlockSurface((*mSurface)());
  }
}

auto surface::lock_handle::pixels() const -> void * {
  return (*mSurface)()->pixels;
}

auto surface::lock_handle::pitch() const -> int { return (*mSurface)()->pitch; }

auto surface::lock_handle::format_info() const noexcept -> pixel_format_info {
  return pixel_format_info{(*mSurface)()->format};
}

auto surface::operator()() const noexcept -> SDL_Surface * { return mSurface; }

surface::surface(SDL_Surface *surface) : mSurface{nullptr} {
  assert(surface);
  mSurface = surface;
}

surface::surface(const rsi &size, const pixel_format &format)
    : mSurface{nullptr} {
  SDL_Surface *tmp = SDL_CreateRGBSurfaceWithFormat(0, size.w(), size.h(), 0,
                                                    to_underlying(format));
  if (tmp)
    mSurface = tmp;
  else
    throw exception{"SDL_CreateRGBSurfaceWithFormat"};
}

surface::surface(const std::string &filePath) {
  SDL_Surface *tmp = IMG_Load(filePath.c_str());
  if (tmp)
    mSurface = tmp;
  else
    throw exception{"IMG_Load"};
}

surface::~surface() {
  if (mSurface != nullptr)
    SDL_FreeSurface(mSurface);
}

surface::surface(surface &&other) noexcept { *this = std::move(other); }

surface &surface::operator=(surface &&other) noexcept {
  if (this != &other) {
    if (mSurface != nullptr)
      SDL_FreeSurface(mSurface);
    mSurface = other.mSurface;
    other.mSurface = nullptr;
  }
  return *this;
}

auto surface::convert_to(const pixel_format format) const -> surface {
  SDL_Surface *tmp =
      SDL_ConvertSurfaceFormat(mSurface, to_underlying(format), 0);
  if (tmp) {
    surface result{tmp};
    result.set_blend_mode(blend_mode());
    return result;
  } else
    throw exception{"SDL_ConvertSurfaceFormat"};
}

auto surface::lock() -> lock_handle { return lock_handle{this}; }

auto surface::format() const noexcept -> pixel_format {
  return static_cast<pixel_format>(mSurface->format->format);
}

auto surface::clip() const -> recti {
  recti rect;
  SDL_GetClipRect(mSurface, rect());
  return rect;
}

auto surface::set_clip(const maybe<recti> &rect) -> surface & {
  query_status(SDL_SetClipRect(mSurface, rect ? rect.value()() : nullptr),
               "SDL_SetClipRect");
  return *this;
}

auto surface::size() const noexcept -> rsi {
  return {wi{mSurface->w}, hi{mSurface->h}};
}

auto surface::colour_mod() const -> colour {
  colour colour;
  query_status(
      SDL_GetSurfaceColorMod(mSurface, &colour.r(), &colour.g(), &colour.b()),
      "SDL_GetSurfaceColorMod");
  query_status(SDL_GetSurfaceAlphaMod(mSurface, &colour.a()),
               "SDL_GetSurfaceAlphaMod");
  return colour;
}

auto surface::set_colour_mod(const colour &colour) -> surface & {
  query_status(
      SDL_SetSurfaceColorMod(mSurface, colour.r(), colour.g(), colour.b()),
      "SDL_SetSurfaceColorMod");
  return *this;
}

auto surface::set_alpha_mod(const uint8 alpha) -> surface & {
  query_status(SDL_SetSurfaceAlphaMod(mSurface, alpha),
               "SDL_SetSurfaceAlphaMod");
  return *this;
}

auto surface::blend_mode() const noexcept -> enum blend_mode {
  SDL_BlendMode mode; query_status(SDL_GetSurfaceBlendMode(mSurface, &mode),
                                   "SDL_GetSurfaceBlendMode");
  return static_cast<enum blend_mode>(mode);
}

auto surface::set_blend_mode(const enum blend_mode mode) -> surface & {
  query_status(
      SDL_SetSurfaceBlendMode(mSurface, static_cast<SDL_BlendMode>(mode)),
      "SDL_SetSurfaceBlendMode");
  return *this;
}

auto surface::has_rle() const noexcept -> bool {
  return SDL_HasSurfaceRLE(mSurface) == SDL_TRUE;
}

auto surface::set_rle(const bool enabled) -> surface & {
  query_status(SDL_SetSurfaceRLE(mSurface, enabled ? SDL_TRUE : SDL_FALSE),
               "SDL_SetSurfaceRLE");
  return *this;
}

} // namespace sdlpp
