
#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <SDL_rect.h>
#include <SDL_render.h>
#include <cassert>

#include "sdlpp/common/exception.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/video/pixel.hpp"
#include "sdlpp/video/renderer.hpp"
#include "sdlpp/video/texture.hpp"

namespace sdlpp {

texture::lock_handle::lock_handle()
    : mTexture{nullptr}, mPixels{nullptr}, mPitch{0} {}

texture::lock_handle::lock_handle(texture *texture, const maybe<recti> &rect)
    : mTexture{nullptr}, mPixels{nullptr}, mPitch{0} {
  assert(texture);
  mTexture = texture;
  if (SDL_LockTexture((*mTexture)(), rect ? rect.value()() : nullptr, &mPixels,
                      &mPitch) != 0)
    throw exception{"SDL_LockTexture"};
}

texture::lock_handle::lock_handle(texture::lock_handle &&other) noexcept
    : mTexture{nullptr}, mPixels{nullptr}, mPitch{0} {
  *this = std::move(other);
}

auto texture::lock_handle::operator=(texture::lock_handle &&other) noexcept
    -> texture::lock_handle & {
  if (this != &other) {
    if (mTexture != nullptr)
      SDL_UnlockTexture((*mTexture)());
    mTexture = other.mTexture;
    mPixels = other.mPixels;
    mPitch = other.mPitch;
    other.mTexture = nullptr;
    other.mPixels = nullptr;
    other.mPitch = 0;
  }
  return *this;
}

texture::lock_handle::~lock_handle() {
  if (mTexture != nullptr)
    SDL_UnlockTexture((*mTexture)());
}

auto texture::lock_handle::pixels() const -> void * { return mPixels; }

auto texture::lock_handle::pitch() const -> int { return mPitch; }

auto texture::operator()() const noexcept -> SDL_Texture * { return mTexture; }

texture::texture(SDL_Texture *texture) : mTexture{nullptr} {
  assert(texture);
  mTexture = texture;
}

texture::texture(renderer &renderer, const std::string &filename)
    : mTexture{nullptr} {
  SDL_Texture *tmp = IMG_LoadTexture(renderer(), filename.c_str());
  if (tmp)
    mTexture = tmp;
  else
    throw exception{"IMG_Loadtexture"};
}

texture::~texture() {
  if (mTexture != nullptr)
    SDL_DestroyTexture(mTexture);
}

texture::texture(texture &&other) noexcept : mTexture{nullptr} {
  *this = std::move(other);
}

auto texture::operator=(texture &&other) noexcept -> texture & {
  if (this != &other) {
    if (mTexture != nullptr)
      SDL_DestroyTexture(mTexture);
    mTexture = other.mTexture;
    other.mTexture = nullptr;
  }
  return *this;
}

auto texture::lock(const maybe<recti> &rect) -> lock_handle {
  return lock_handle(this, rect);
}

auto texture::format() const noexcept -> pixel_format {
  uint32 format{};
  query_status(SDL_QueryTexture(mTexture, &format, nullptr, nullptr, nullptr),
               "SDL_QueryTexture");
  return static_cast<pixel_format>(format);
}

auto texture::access() const noexcept -> texture_access {
  int access{};
  query_status(SDL_QueryTexture(mTexture, nullptr, &access, nullptr, nullptr),
               "SDL_QueryTexture");
  return static_cast<texture_access>(access);
}

auto texture::size() const noexcept -> rsi {
  int width{}, height{};
  query_status(SDL_QueryTexture(mTexture, nullptr, nullptr, &width, &height),
               "SDL_QueryTexture");
  return {wi{width}, hi{height}};
}

auto texture::colour_mod() const -> colour {
  colour colour;
  query_status(
      SDL_GetTextureColorMod(mTexture, &colour.r(), &colour.g(), &colour.b()),
      "SDL_GetTextureColorMod");
  query_status(SDL_GetTextureAlphaMod(mTexture, &colour.a()),
               "SDL_GetTextureAlphaMod");
  return colour;
}

auto texture::set_colour_mod(const colour &colour) -> texture & {
  query_status(
      SDL_SetTextureColorMod(mTexture, colour.r(), colour.g(), colour.b()),
      "SDL_SetTextureColorMod");
  return *this;
}

auto texture::set_alpha_mod(const uint8 alpha) -> texture & {
  query_status(SDL_SetTextureAlphaMod(mTexture, alpha),
               "SDL_SetTextureAlphaMod");
  return *this;
}

auto texture::blend_mode() const noexcept -> enum blend_mode {
  SDL_BlendMode mode; query_status(SDL_GetTextureBlendMode(mTexture, &mode),
                                   "SDL_GetTextureBlendMode");
  return static_cast<enum blend_mode>(mode);
}

auto texture::set_blend_mode(const enum blend_mode mode) -> texture & {
  query_status(
      SDL_SetTextureBlendMode(mTexture, static_cast<SDL_BlendMode>(mode)),
      "SDL_SetTextureBlendMode");
  return *this;
}

} // namespace sdlpp
