#include <SDL2/SDL.h>
#include <SDL_video.h>

#include "sdlpp/common/exception.hpp"
#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include "sdlpp/common/utils.hpp"
#include "sdlpp/video/surface.hpp"
#include "sdlpp/video/window.hpp"

namespace sdlpp {

auto window::operator()() const noexcept -> SDL_Window * { return mWindow; }

window::window(SDL_Window *window) noexcept : mWindow{nullptr} {
  assert(window);
  mWindow = window;
}

window::window(const std::string &title, const pointi &position,
               const rsi &size, const window_flags flags)
    : mWindow{nullptr} {
  SDL_Window *const tmp =
      SDL_CreateWindow(title.c_str(), position.x(), position.y(), size.w(),
                       size.h(), to_underlying(flags));
  if (tmp)
    mWindow = tmp;
  else
    throw exception{"SDL_CreateWindow"};
}

window::~window() {
  if (mWindow != nullptr)
    SDL_DestroyWindow(mWindow);
}

window::window(window &&other) noexcept : mWindow{nullptr} {
  *this = std::move(other);
}

window &window::operator=(window &&other) noexcept {
  if (this != &other) {
    if (mWindow != nullptr)
      SDL_DestroyWindow(mWindow);
    mWindow = other.mWindow;
    other.mWindow = nullptr;
  }
  return *this;
}

auto window::id() const noexcept -> uint32 { return SDL_GetWindowID(mWindow); }

auto window::size() const noexcept -> rsi {
  rsi size;
  SDL_GetWindowSize(mWindow, &size.w(), &size.h());
  return size;
}

auto window::drawable_size() const noexcept -> rsi {
  rsi size;
  SDL_GL_GetDrawableSize(mWindow, &size.w(), &size.h());
  return size;
}

auto window::set_title(const std::string &title) noexcept -> window & {
  SDL_SetWindowTitle(mWindow, title.c_str());
  return *this;
}

auto window::title() const noexcept -> std::string {
  return SDL_GetWindowTitle(mWindow);
}

auto window::maximise() noexcept -> window & {
  SDL_MaximizeWindow(mWindow);
  return *this;
}

auto window::minimise() noexcept -> window & {
  SDL_MinimizeWindow(mWindow);
  return *this;
}

auto window::hide() noexcept -> window & {
  SDL_HideWindow(mWindow);
  return *this;
}

auto window::restore() noexcept -> window & {
  SDL_RestoreWindow(mWindow);
  return *this;
}

auto window::raise() noexcept -> window & {
  SDL_RaiseWindow(mWindow);
  return *this;
}

auto window::show() noexcept -> window & {
  SDL_ShowWindow(mWindow);
  return *this;
}

auto window::set_fullscreen(const uint32 flags) -> window & {
  query_status(SDL_SetWindowFullscreen(mWindow, flags),
               "SDL_SetWindowFullscreen");
  return *this;
}

auto window::set_fullscreen(const fullscreen_flags flags) -> window & {
  return set_fullscreen(static_cast<uint32>(flags));
}

auto window::set_size(const rsi &size) noexcept -> window & {
  SDL_SetWindowSize(mWindow, size.w(), size.h());
  return *this;
}

auto window::brightness() const noexcept -> float {
  return SDL_GetWindowBrightness(mWindow);
}

auto window::set_brightness(float brightness) -> window & {
  query_status(SDL_SetWindowBrightness(mWindow, brightness),
               "SDL_SetWindowBrightness");
  return *this;
}

auto window::position() const noexcept -> pointi {
  pointi position;
  SDL_GetWindowPosition(mWindow, &position.x(), &position.y());
  return position;
}

auto window::set_position(const pointi &position) noexcept -> window & {
  SDL_SetWindowPosition(mWindow, position.x(), position.y());
  return *this;
}

auto window::minimum_size() const noexcept -> rsi {
  rsi size;
  SDL_GetWindowMinimumSize(mWindow, &size.w(), &size.h());
  return size;
}

auto window::set_minimum_size(const rsi &size) noexcept -> window & {
  SDL_SetWindowMinimumSize(mWindow, size.w(), size.h());
  return *this;
}

auto window::maximum_size() const noexcept -> rsi {
  rsi size;
  SDL_GetWindowMaximumSize(mWindow, &size.w(), &size.h());
  return size;
}

auto window::set_maximum_size(const rsi &size) noexcept -> window & {
  SDL_SetWindowMaximumSize(mWindow, size.w(), size.h());
  return *this;
}

auto window::is_grabbed() const noexcept -> bool {
  return SDL_GetWindowGrab(mWindow) == SDL_TRUE;
}

auto window::set_grabbed(const bool grabbed) noexcept -> window & {
  SDL_SetWindowGrab(mWindow, grabbed ? SDL_TRUE : SDL_FALSE);
  return *this;
}

auto window::display_index() const -> int {
  int index = SDL_GetWindowDisplayIndex(mWindow);
  if (index < 0)
    throw exception{"SDL_GetWindowDisplayIndex"};
  return index;
}

auto window::display_mode() const -> SDL_DisplayMode {
  SDL_DisplayMode mode;
  query_status(SDL_GetWindowDisplayMode(mWindow, &mode),
               "SDL_GetWindowDisplayMode");
  return mode;
}

auto window::set_display_mode(const SDL_DisplayMode &mode) -> window & {
  query_status(SDL_SetWindowDisplayMode(mWindow, &mode),
               "SDL_SetWindowDisplayMode");
  return *this;
}

auto window::set_display_mode() -> window & {
  query_status(SDL_SetWindowDisplayMode(mWindow, nullptr),
               "SDL_SetWindowDisplayMode");
  return *this;
}

auto window::flags() const noexcept -> window_flags {
  return static_cast<window_flags>(SDL_GetWindowFlags(mWindow));
}

auto window::set_bordered(const bool bordered) -> window & {
  SDL_SetWindowBordered(mWindow, bordered ? SDL_TRUE : SDL_FALSE);
  return *this;
}

auto window::opacity() const noexcept -> float {
  float opacity;
  query_status(SDL_GetWindowOpacity(mWindow, &opacity), "SDL_GetWindowOpacity");
  return opacity;
}

auto window::set_opacity(float opacity) -> window & {
  query_status(SDL_SetWindowOpacity(mWindow, opacity), "SDL_SetWindowOpacity");
  return *this;
}

auto window::set_resizable(const bool resizable) -> window & {
  SDL_SetWindowResizable(mWindow, resizable ? SDL_TRUE : SDL_FALSE);
  return *this;
}

auto window::set_icon(const surface &surface) -> window & {
  SDL_SetWindowIcon(mWindow, surface());
  return *this;
}

} // namespace sdlpp
