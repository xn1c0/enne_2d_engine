#include <doctest/doctest.h>
#include <sdlpp/common/math/point.hpp>

using namespace sdlpp;

TEST_SUITE("Functional") {
  TEST_CASE("Basic usage") {
    pointi b{xi{12}, yi{3}};
    REQUIRE(b.x() == 12);
    REQUIRE(b.y() == 3);

    pointf a{xf{6.3f}, yf{10.9f}};
    REQUIRE(a.x() == 6.3f);
    REQUIRE(a.y() == 10.9f);
  } // TEST_CASE("Basic usage")

  TEST_CASE("Getters and setters") {
    SUBCASE("Run-time") {
      pointi a{};
      REQUIRE(a == pointi{xi{0}, yi{0}});
      a.x() = 32;
      REQUIRE(++a.x() == 33);
      a.y() = 1;
      REQUIRE(a.y()++ == 1);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pointi a{xi{2}, yi{3}};
      static_assert(a.x() == 2, "point values access is not constexpr");
      static_assert(++(pointi{}.operator+=(a)).x() == 3,
                    "point values access is not constexpr");
    } // SUBCASE("Compile-time")
  }

  TEST_CASE("Conversion constructor") {
    SUBCASE("Run-time") {
      pointi a{pointf{xf{5.6f}, yf{4.3f}}};
      REQUIRE(a == pointi{xi{5}, yi{4}});
    } // SUBCASE("Run-time")
    SUBCASE("Compile-time") {
      constexpr pointi a{pointf{xf{5.6f}, yf{4.3f}}};
      static_assert(a == pointi{xi{5}, yi{4}},
                    "point conversion is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Conversion constructor")

} // TEST_SUITE("Functional")

TEST_SUITE("Math") {

  TEST_CASE("Relational") {
    SUBCASE("Run-time") {
      REQUIRE(pointi{xi{10}, yi{4}} == pointi{xi{10}, yi{4}});
      REQUIRE_FALSE(pointi{xi{10}, yi{4}} == pointi{xi{10}, yi{0}});
      REQUIRE(pointi{xi{10}, yi{4}} != pointi{xi{10}, yi{1}});
      REQUIRE_FALSE(pointi{xi{10}, yi{4}} != pointi{xi{10}, yi{4}});
      REQUIRE(pointi{xi{10}, yi{4}} < pointi{xi{10}, yi{6}});
      REQUIRE_FALSE(pointi{xi{10}, yi{4}} < pointi{xi{10}, yi{4}});
      REQUIRE(pointi{xi{10}, yi{4}} <= pointi{xi{10}, yi{4}});
      REQUIRE_FALSE(pointi{xi{10}, yi{4}} <= pointi{xi{10}, yi{3}});
      REQUIRE(pointi{xi{10}, yi{4}} > pointi{xi{9}, yi{1}});
      REQUIRE_FALSE(pointi{xi{10}, yi{4}} > pointi{xi{10}, yi{8}});
      REQUIRE(pointi{xi{10}, yi{4}} >= pointi{xi{10}, yi{1}});
      REQUIRE_FALSE(pointi{xi{10}, yi{4}} >= pointi{xi{10}, yi{7}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      static_assert(pointi{xi{5}, yi{3}} == pointi{xi{5}, yi{3}},
                    "point comparison is not constexpr");
      static_assert(!(pointi{xi{5}, yi{3}} == pointi{xi{4}, yi{3}}),
                    "point comparison is not constexpr");
      static_assert(pointi{xi{5}, yi{3}} != pointi{xi{4}, yi{3}},
                    "point comparison is not constexpr");
      static_assert(!(pointi{xi{5}, yi{3}} != pointi{xi{5}, yi{3}}),
                    "point comparison is not constexpr");
      static_assert(pointi{xi{5}, yi{3}} < pointi{xi{5}, yi{4}},
                    "point comparison is not constexpr");
      static_assert(!(pointi{xi{5}, yi{3}} < pointi{xi{5}, yi{2}}),
                    "point is not constexpr");
      static_assert(pointi{xi{5}, yi{3}} <= pointi{xi{5}, yi{3}},
                    "point comparison is not constexpr");
      static_assert(!(pointi{xi{5}, yi{3}} <= pointi{xi{5}, yi{1}}),
                    "point is not constexpr");
      static_assert(pointi{xi{6}, yi{3}} > pointi{xi{5}, yi{4}},
                    "point comparison is not constexpr");
      static_assert(!(pointi{xi{5}, yi{3}} > pointi{xi{5}, yi{9}}),
                    "point comparison is not constexpr");
      static_assert(pointi{xi{5}, yi{3}} >= pointi{xi{5}, yi{2}},
                    "point comparison is not constexpr");
      static_assert(!(pointi{xi{5}, yi{3}} >= pointi{xi{10}, yi{10}}),
                    "point comparison is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Relational")

  TEST_CASE("Addition") {
    SUBCASE("Run-time") {
      pointi a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      REQUIRE(+a == pointi{xi{10}, yi{4}});
      REQUIRE(a + b == pointi{xi{17}, yi{6}});
      a += b;
      REQUIRE(a == pointi{xi{17}, yi{6}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pointi a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      static_assert(+a == pointi{xi{10}, yi{4}},
                    "point addition is not constexpr");
      static_assert(a + b == pointi{xi{17}, yi{6}},
                    "point addition is not constexpr");
      static_assert(pointi{xi{1}, yi{2}}.operator+=(a) == pointi{xi{11}, yi{6}},
                    "point addition is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Addition")

  TEST_CASE("Subtraction") {
    SUBCASE("Run-time") {
      pointi a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      REQUIRE(-a == pointi{xi{-10}, yi{-4}});
      REQUIRE(a - b == pointi{xi{3}, yi{2}});
      REQUIRE(b - a == pointi{xi{-3}, yi{-2}});
      a -= b;
      REQUIRE(a == pointi{xi{3}, yi{2}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pointi a{xi{10}, yi{4}}, b{xi{7}, yi{2}};
      static_assert(-a == pointi{xi{-10}, yi{-4}},
                    "point subtraction is not constexpr");
      static_assert(a - b == pointi{xi{3}, yi{2}},
                    "point subtraction is not constexpr");
      static_assert(b - a == -pointi{xi{3}, yi{2}},
                    "point subtraction is not constexpr");
      static_assert(pointi{xi{1}, yi{2}}.operator-=(a) ==
                        pointi{xi{-9}, yi{-2}},
                    "point subtraction is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Subtraction")

  TEST_CASE("Multiplication by a scalar") {
    SUBCASE("Run-time") {
      pointi a{xi{10}, yi{4}};
      REQUIRE(a * 3 == pointi{xi{30}, yi{12}});
      REQUIRE(3 * a == pointi{xi{30}, yi{12}});
      a *= 3;
      REQUIRE(a == pointi{xi{30}, yi{12}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pointi a{xi{10}, yi{4}};
      static_assert(a * 0 == pointi{}, "point multiplication is not constexpr");
      static_assert(0 * a == pointi{}, "point multiplication is not constexpr");
      static_assert(pointi{xi{1}, yi{2}}.operator*=(2) == pointi{xi{2}, yi{4}},
                    "point multiplication is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Multiplication by a scalar")

  TEST_CASE("Division by a scalar") {
    SUBCASE("Run-time") {
      pointi a{xi{10}, yi{4}};
      REQUIRE(a / 2 == pointi{xi{5}, yi{2}});
      a /= 2;
      REQUIRE(a == pointi{xi{5}, yi{2}});
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pointi a{xi{10}, yi{4}};
      static_assert(a / 4 == pointi{xi{2}, yi{1}},
                    "point multiplication is not constexpr");
      static_assert(pointi{xi{2}, yi{2}}.operator/=(2) == pointi{xi{1}, yi{1}},
                    "point multiplication is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Division by a scalar")

} // TEST_SUITE("Operations")
