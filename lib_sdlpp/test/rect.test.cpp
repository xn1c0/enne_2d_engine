#include "sdlpp/common/math/point.hpp"
#include "sdlpp/common/math/rect_size.hpp"
#include "sdlpp/common/primitives.hpp"
#include <doctest/doctest.h>
#include <sdlpp/common/math/rect.hpp>

using namespace sdlpp;

TEST_SUITE("Functional") {
  TEST_CASE("Getters and setters") {
    SUBCASE("Run-time") {
      pointi aPoint{xi{5}, yi{10}};
      rsi aSize{wi{100}, hi{100}};
      recti aRect{aPoint, aSize};
      REQUIRE(aRect.x() == 5);
      REQUIRE(aRect.y() == 10);
      REQUIRE(aRect.w() == 100);
      REQUIRE(aRect.h() == 100);
      REQUIRE(aRect.size().w() == aRect.w());
      REQUIRE(aRect.size().h() == aRect.h());
      REQUIRE(aRect.size() == aSize);
      REQUIRE(aRect.top_left_corner() == aPoint);
      REQUIRE(aRect.bottom_left_corner() == aPoint + pointi{xi{}, yi{100}});
      REQUIRE(aRect.top_right_corner() == aPoint + pointi{xi{100}, yi{}});
      REQUIRE(aRect.bottom_right_corner() == aPoint + pointi{xi{100}, yi{100}});
    } // TEST_CASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pointi aPoint{xi{5}, yi{10}};
      constexpr rsi aSize{wi{100}, hi{100}};
      constexpr recti aRect{aPoint, aSize};
      static_assert(aRect.x() == 5, "rect values access is not constexpr");
      static_assert(aRect.size() == aSize,
                    "rect values access is not constexpr");
      static_assert(
          (aRect.bottom_right_corner().operator-=(pointi{xi{100}, yi{100}}))
                  .x() == 5,
          "rect values access is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Basic usage")

  TEST_CASE("Conversion constructor") {
    SUBCASE("Run-time") {
      pointf aPoint{xf{5.4f}, yf{12.1f}};
      rsf aSize{wf{80.9f}, hf{70.5f}};
      recti aRect{rectf{aPoint, aSize}};
      recti bRect{pointi{xi{5}, yi{12}}, rsi{wi{80}, hi{70}}};
      REQUIRE(aRect == bRect);
    } // SUBCASE("Run-time")
    SUBCASE("Compile-time") {
      constexpr pointf aPoint{xf{5.4f}, yf{12.1f}};
      constexpr rsf aSize{wf{80.9f}, hf{70.5f}};
      constexpr recti aRect{rectf{aPoint, aSize}};
      constexpr recti bRect{pointi{xi{5}, yi{12}}, rsi{wi{80}, hi{70}}};
      static_assert(aRect == bRect, "rect conversion is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Conversion constructor")

  TEST_CASE("Centroid") {
    SUBCASE("Run-time") {
      pointi aPoint{xi{10}, yi{30}};
      rsi aSize{wi{309}, hi{203}};
      recti aRect{aPoint, aSize};
      pointf bPoint{xf{154.5f}, yf{101.5f}};
      pointf cPoint{xf{164.5f}, yf{131.5f}};
      REQUIRE(aRect.x() + aRect.w() / float{2} == aRect.centroid().x());
      REQUIRE(aRect.centroid() == bPoint + pointf{aPoint});
      REQUIRE(aRect.centroid() == cPoint);
    } // SUBCASE("Run-time")
    SUBCASE("Compile-time") {
      constexpr pointi aPoint{xi{10}, yi{30}};
      constexpr rsi aSize{wi{309}, hi{203}};
      constexpr recti aRect{aPoint, aSize};
      constexpr pointf bPoint{xf{154.5f}, yf{101.5f}};
      constexpr pointf cPoint{xf{164.5f}, yf{131.5f}};
      static_assert(aRect.x() + aRect.w() / float{2} == aRect.centroid().x(),
                    "rect centroid is not constexpr");
      static_assert(aRect.centroid() == bPoint + pointf{aPoint},
                    "rect centroid is not constexpr");
      static_assert(aRect.centroid() == cPoint,
                    "rect centroid is not constexpr");

    } // SUBCASE("Compile-time")
  } // TEST_CASE("Centroid")

  TEST_CASE("Contains") {
    SUBCASE("Run-time") {
      pointi aPoint{xi{10}, yi{10}};
      rsi aSize{wi{50}, hi{60}};
      recti aRect{aPoint, aSize};
      pointi bPoint{xi{15}, yi{30}};
      pointi cPoint{xi{10}, yi{9}};
      pointi dPoint{xi{60}, yi{70}};
      pointi ePoint{xi{10}, yi{70}};
      pointi pointf{xi{60}, yi{10}};
      pointi gPoint{xi{45}, yi{45}};
      pointi hPoint{xi{70}, yi{90}};
      pointi pointi{xi{0}, yi{0}};
      REQUIRE(aRect.contains(aPoint));
      REQUIRE(aRect.contains(bPoint));
      REQUIRE_FALSE(aRect.contains(cPoint));
      REQUIRE(aRect.contains(dPoint));
      REQUIRE(aRect.contains(ePoint));
      REQUIRE(aRect.contains(pointf));
      REQUIRE(aRect.contains(gPoint));
      REQUIRE_FALSE(aRect.contains(hPoint));
      REQUIRE_FALSE(aRect.contains(pointi));
    } // SUBCASE("Run-time")
    SUBCASE("Compile-time") {
      constexpr pointi aPoint{xi{10}, yi{10}};
      constexpr rsi aSize{wi{50}, hi{60}};
      constexpr recti aRect{aPoint, aSize};
      constexpr pointi bPoint{xi{15}, yi{30}};
      constexpr pointi cPoint{xi{10}, yi{9}};
      constexpr pointi dPoint{xi{60}, yi{70}};
      constexpr pointi ePoint{xi{10}, yi{70}};
      constexpr pointi pointf{xi{60}, yi{10}};
      constexpr pointi gPoint{xi{45}, yi{45}};
      constexpr pointi hPoint{xi{70}, yi{90}};
      constexpr pointi pointi{xi{0}, yi{0}};
      static_assert(aRect.contains(aPoint), "rect contains is not constexpr");
      static_assert(aRect.contains(bPoint), "rect contains is not constexpr");
      static_assert(!aRect.contains(cPoint), "rect contains is not constexpr");
      static_assert(aRect.contains(dPoint), "rect contains is not constexpr");
      static_assert(aRect.contains(ePoint), "rect contains is not constexpr");
      static_assert(aRect.contains(pointf), "rect contains is not constexpr");
      static_assert(aRect.contains(gPoint), "rect contains is not constexpr");
      static_assert(!aRect.contains(hPoint), "rect contains is not constexpr");
      static_assert(!aRect.contains(pointi), "rect contains is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Contains")

  TEST_CASE("Intersection") {
    SUBCASE("Run-time") {
      pointi aPoint{xi{10}, yi{10}};
      rsi aSize{wi{100}, hi{100}};
      recti aRect{aPoint, aSize};

      pointi bPoint{xi{0}, yi{0}};
      rsi bSize{wi{20}, hi{20}};
      recti bRect{bPoint, bSize};

      pointi cPoint{xi{100}, yi{100}};
      rsi cSize{wi{10}, hi{10}};
      recti cRect{cPoint, cSize};

      pointi dPoint{xi{30}, yi{30}};
      rsi dSize{wi{50}, hi{50}};
      recti dRect{dPoint, dSize};

      pointi ePoint{xi{120}, yi{120}};
      rsi eSize{wi{50}, hi{50}};
      recti eRect{ePoint, eSize};

      REQUIRE(aRect.intersection(aRect).has_value());
      REQUIRE(aRect.intersection(bRect).has_value());
      REQUIRE(aRect.intersection(cRect).has_value());
      REQUIRE(aRect.intersection(dRect).has_value());
      REQUIRE_FALSE(aRect.intersection(eRect).has_value());

      REQUIRE(aRect.intersection(bRect).value().size().area() == 100);
      REQUIRE(aRect.intersection(cRect).value().size().area() == 100);
      REQUIRE(aRect.intersection(dRect).value().size().area() == 2500);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pointi aPoint{xi{10}, yi{10}};
      constexpr rsi aSize{wi{100}, hi{100}};
      constexpr recti aRect{aPoint, aSize};

      constexpr pointi bPoint{xi{0}, yi{0}};
      constexpr rsi bSize{wi{20}, hi{20}};
      constexpr recti bRect{bPoint, bSize};

      constexpr pointi cPoint{xi{100}, yi{100}};
      constexpr rsi cSize{wi{10}, hi{10}};
      constexpr recti cRect{cPoint, cSize};

      constexpr pointi dPoint{xi{30}, yi{30}};
      constexpr rsi dSize{wi{50}, hi{50}};
      constexpr recti dRect{dPoint, dSize};

      constexpr pointi ePoint{xi{120}, yi{120}};
      constexpr rsi eSize{wi{50}, hi{50}};
      constexpr recti eRect{ePoint, eSize};

      static_assert(aRect.intersection(aRect).has_value(),
                    "rect values access is not constexpr");
      static_assert(aRect.intersection(bRect).has_value(),
                    "rect values access is not constexpr");
      static_assert(aRect.intersection(cRect).has_value(),
                    "rect values access is not constexpr");
      static_assert(aRect.intersection(dRect).has_value(),
                    "rect values access is not constexpr");
      static_assert(!aRect.intersection(eRect).has_value(),
                    "rect values access is not constexpr");
      static_assert(aRect.intersection(bRect).value().size().area() == 100,
                    "rect values access is not constexpr");
      static_assert(aRect.intersection(cRect).value().size().area() == 100,
                    "rect values access is not constexpr");
      static_assert(aRect.intersection(dRect).value().size().area() == 2500,
                    "rect values access is not constexpr");

    } // SUBCASE("Compile-time")
  } // TEST_CASE("Intersection")
} // TEST_SUITE("Functional")
