#ifndef LIB_ST_INCLUDE_ST_CRTP_HPP_
#define LIB_ST_INCLUDE_ST_CRTP_HPP_

/**
 * @author Nicolò Plebani
 * @file crtp.hpp
 */

namespace st {

/**
 * @brief Templated class crtp.
 * @tparam T The type of the underlyng value.
 * @tparam CRTP The phantom template, used to differentiate types.
 * @note This class serves to not rewrite the static_cast every time.
 */
template <typename T, template <typename> class CRTP> class crtp {
  // If two classes happen to derive from the same CRTP base class we likely get
  // to undefined behaviour when the CRTP will try to use the wrong class.
  // Since the constructor in the base class is private, no one can access it
  // except the friend classes. So if the derived class is different from the
  // template class, the code doesn’t compile.
  // So we have.
private:
  constexpr crtp() noexcept {}
  friend CRTP<T>;

public:
  constexpr T &underlying() noexcept { return static_cast<T &>(*this); }
  constexpr T const &underlying() const noexcept {
    return static_cast<const T &>(*this);
  }
};

} // namespace st

#endif // LIB_ST_INCLUDE_ST_CRTP_HPP_
