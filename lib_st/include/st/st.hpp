#ifndef LIB_ST_INCLUDE_ST_ST_HPP_
#define LIB_ST_INCLUDE_ST_ST_HPP_

/**
 * @author Nicolò Plebani
 * @file st.hpp
 */

#include <st/traits.hpp>
#include <st/type.hpp>

#endif // LIB_ST_INCLUDE_ST_ST_HPP_
