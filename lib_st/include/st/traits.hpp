#ifndef LIB_ST_INCLUDE_ST_TRAITS_HPP_
#define LIB_ST_INCLUDE_ST_TRAITS_HPP_

/**
 * @author Nicolò Plebani
 * @file traits.hpp
 */

#include "st/crtp.hpp"
#include "st/type.hpp"
#include <ostream>

namespace st::traits {

/**
 * @brief Ostream pperator << overloading.
 * @tparam T The type to be stored in type class.
 * @tparam Tag The phantom type specifieng which type this is.
 * @tparam ...Traits The parameter pack describing the type class traits.
 * @param os The outputstream.
 * @param object The object to be printed in output.
 * @note The object must be printable.
 */
template <typename T, typename Tag, template <typename> class... Traits>
  requires type<T, Tag, Traits...>::is_printable
std::ostream &operator<<(std::ostream &os,
                         type<T, Tag, Traits...> const &object) {
  object.print(os);
  return os;
}

/**
 * @brief Defines the printable feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct printable : crtp<T, printable> {
protected:
  constexpr printable() noexcept {}
  friend T;

public:
  static constexpr bool is_printable = true;
  void print(std::ostream &os) const { os << this->underlying().value(); }
};

/**
 * @brief Defines the relational feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct relational : crtp<T, relational> {
  [[nodiscard]] constexpr auto
  operator<(relational<T> const &other) const -> bool {
    return this->underlying().value() < other.underlying().value();
  }
  [[nodiscard]] constexpr auto
  operator>(relational<T> const &other) const -> bool {
    return other.underlying().value() < this->underlying().value();
  }
  [[nodiscard]] constexpr auto
  operator<=(relational<T> const &other) const -> bool {
    return !(other < *this);
  }
  [[nodiscard]] constexpr auto
  operator>=(relational<T> const &other) const -> bool {
    return !(*this < other);
  }
  [[nodiscard]] constexpr auto
  operator==(relational<T> const &other) const -> bool {
    return !(*this < other) && !(other < *this);
  }
  [[nodiscard]] constexpr auto
  operator!=(relational<T> const &other) const -> bool {
    return !(*this == other);
  }
};

namespace arithmetic {

/**
 * @brief Defines the pre_increment feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct pre_increment : crtp<T, pre_increment> {
protected:
  constexpr pre_increment() noexcept {}
  friend T;

public:
  constexpr auto operator++() noexcept -> T & {
    ++this->underlying().value();
    return this->underlying();
  }
};

/**
 * @brief Defines the post_increment feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct post_increment : crtp<T, post_increment> {
protected:
  constexpr post_increment() noexcept {}
  friend T;

public:
  constexpr auto operator++(int) noexcept -> T {
    return T(this->underlying().value()++);
  }
};

/**
 * @brief Defines the increment feature.
 * @tparam T The type of the underlying value.
 */
template <typename T>
struct increment : public pre_increment<T>, public post_increment<T> {
  using post_increment<T>::operator++;
  using pre_increment<T>::operator++;
};

/**
 * @brief Defines the pre_decrement feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct pre_decrement : crtp<T, pre_decrement> {
protected:
  constexpr pre_decrement() noexcept {}
  friend T;

public:
  constexpr auto operator--() noexcept -> T & {
    --this->underlying().value();
    return this->underlying();
  }
};

/**
 * @brief Defines the post_decrement feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct post_decrement : crtp<T, post_decrement> {
protected:
  constexpr post_decrement() noexcept {}
  friend T;

public:
  constexpr auto operator--(int) -> T {
    return T(this->underlying().value()--);
  }
};

/**
 * @brief Defines the decrement feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct decrement : pre_decrement<T>, post_decrement<T> {
  using post_decrement<T>::operator--;
  using pre_decrement<T>::operator--;
};

/**
 * @brief Defines the unary_plus (promotion) feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct unary_plus : crtp<T, unary_plus> {
protected:
  constexpr unary_plus() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator+() const noexcept -> T {
    return T(+this->underlying().value());
  }
};

/**
 * @brief Defines the binary_addition feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct binary_addition : crtp<T, binary_addition> {
protected:
  constexpr binary_addition() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator+(T const &other) const noexcept -> T {
    return T(this->underlying().value() + other.value());
  }
  constexpr auto operator+=(T const &other) noexcept -> T & {
    this->underlying().value() += other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the addition feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct addition : unary_plus<T>, binary_addition<T> {
  using unary_plus<T>::operator+;
  using binary_addition<T>::operator+;
};

/**
 * @brief Defines the unary_minus (negation) feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct unary_minus : crtp<T, unary_minus> {
protected:
  constexpr unary_minus() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator-() const noexcept -> T {
    return T(-this->underlying().value());
  }
};

/**
 * @brief Defines the binary_subtraction feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct binary_subtraction : crtp<T, binary_subtraction> {
protected:
  constexpr binary_subtraction() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator-(T const &other) const noexcept -> T {
    return T(this->underlying().value() - other.value());
  }
  constexpr auto operator-=(T const &other) noexcept -> T & {
    this->underlying().value() -= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the subtraction feature.
 * @tparam T The type of the underlying value.
 */
template <typename T>
struct subtraction : unary_minus<T>, binary_subtraction<T> {
  using unary_minus<T>::operator-;
  using binary_subtraction<T>::operator-;
};

/**
 * @brief Defines the multiplication feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct multiplication : crtp<T, multiplication> {
protected:
  constexpr multiplication() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator*(T const &other) const noexcept -> T {
    return T(this->underlying().value() * other.value());
  }
  constexpr auto operator*=(T const &other) noexcept -> T & {
    this->underlying().value() *= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the divisible feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct divisible : crtp<T, divisible> {
protected:
  constexpr divisible() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator/(T const &other) const noexcept -> T {
    return T(this->underlying().value() / other.value());
  }
  constexpr auto operator/=(T const &other) noexcept -> T & {
    this->underlying().value() /= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the modulo feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct modulo : crtp<T, modulo> {
protected:
  constexpr modulo() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator%(T const &other) const noexcept -> T {
    return T(this->underlying().value() % other.value());
  }
  constexpr auto operator%=(T const &other) noexcept -> T & {
    this->underlying().value() %= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the bitwise inversion feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct bitwise_not : crtp<T, bitwise_not> {
protected:
  constexpr bitwise_not() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator~() const noexcept -> T {
    return T(~this->underlying().value());
  }
};

/**
 * @brief Defines the bitwise "logical and" feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct bitwise_and : crtp<T, bitwise_and> {
protected:
  constexpr bitwise_and() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator&(T const &other) const noexcept -> T {
    return T(this->underlying().value() & other.value());
  }
  constexpr auto operator&=(T const &other) noexcept -> T & {
    this->underlying().value() &= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the bitwise "logical or" feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct bitwise_or : crtp<T, bitwise_or> {
protected:
  constexpr bitwise_or() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator|(T const &other) const noexcept -> T {
    return T(this->underlying().value() | other.value());
  }
  constexpr auto operator|=(T const &other) noexcept -> T & {
    this->underlying().value() |= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the bitwise "logical xor" feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct bitwise_xor : crtp<T, bitwise_xor> {
protected:
  constexpr bitwise_xor() noexcept {}
  friend T;

public:
  [[nodiscard]] constexpr auto operator^(T const &other) const noexcept -> T {
    return T(this->underlying().value() ^ other.value());
  }
  constexpr auto operator^=(T const &other) noexcept -> T & {
    this->underlying().value() ^= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the bitwise left shift feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct bitwise_shl : crtp<T, bitwise_shl> {
protected:
  constexpr bitwise_shl() noexcept {}
  friend T;

public:
  constexpr T operator<<(T const &other) const {
    return T(this->underlying().value() << other.value());
  }
  constexpr T &operator<<=(T const &other) {
    this->underlying().value() <<= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the bitwise right shift feature.
 * @tparam T The type of the underlying value.
 */
template <typename T> struct bitwise_shr : crtp<T, bitwise_shr> {
protected:
  constexpr bitwise_shr() noexcept {}
  friend T;

public:
  constexpr auto operator>>(T const &other) const noexcept -> T {
    return T(this->underlying().value() >> other.value());
  }
  constexpr auto operator>>=(T const &other) noexcept -> T & {
    this->underlying().value() >>= other.value();
    return this->underlying();
  }
};

/**
 * @brief Defines the complete set of bitwise features.
 * @tparam T The type of the underlying value.
 */
template <typename T>
struct bitwise : bitwise_not<T>,
                 bitwise_and<T>,
                 bitwise_or<T>,
                 bitwise_xor<T>,
                 bitwise_shl<T>,
                 bitwise_shr<T> {};

/**
 * @brief Defines the complete set of arithmetic features.
 * @tparam T The type of the underlying value.
 */
template <typename T>
struct complete : increment<T>,
                  decrement<T>,
                  addition<T>,
                  subtraction<T>,
                  multiplication<T>,
                  divisible<T>,
                  modulo<T>,
                  bitwise<T>,
                  relational<T> {};

} // namespace arithmetic

} // namespace st::traits

#endif // LIB_ST_INCLUDE_ST_TRAITS_HPP_
