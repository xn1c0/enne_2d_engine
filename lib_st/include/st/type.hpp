#ifndef LIB_ST_INCLUDE_ST_TYPE_HPP_
#define LIB_ST_INCLUDE_ST_TYPE_HPP_

/**
 * @author Nicolò Plebani
 * @file type.hpp
 */

#include <type_traits>
#include <utility>

namespace st {

/**
 * @brief Templated class type.
 * This class represents a strong type.
 * @tparam T The type of the value to be stored by this wrapper.
 * @tparam Tag The phantom type specifieng which type this is.
 * @tparam ...Traits The parameter pack adding features to this type.
 * @note It uses CRTP to add new functionalities.
 */
template <typename T, typename Tag, template <typename> class... Traits>
class type : public Traits<type<T, Tag, Traits...>>... {
public:
  using value_type = T;

private:
  value_type mValue; ///< The value stored by this wrapper.

public:
  /**
   * @brief Default constructor.
   * @note trivial and non-throwing.
   */
  explicit constexpr type() = default;

  /**
   * @brief Secondary constructor.
   * @param value The value used to initialise the wrapper.
   */
  explicit constexpr type(const value_type &value) noexcept(
      std::is_nothrow_copy_constructible_v<value_type>)
      : mValue{value} {}

  /**
   * @brief Secondary constructor.
   * @param value The value used to initialise the wrapper.
   */
  explicit constexpr type(value_type &&value) noexcept(
      std::is_nothrow_move_constructible_v<value_type>)
      : mValue{std::move(value)} {}

  /**
   * @brief Gets the reference to the value stored.
   * @return The reference to the stored value (mValue).
   */
  constexpr value_type &value() noexcept { return mValue; }

  /**
   * @brief Gets the value stored.
   * @return The value stored (mValue).
   */
  constexpr value_type value() const noexcept { return mValue; }
};

} // namespace st

#endif // LIB_ST_INCLUDE_ST_TYPE_HPP_
