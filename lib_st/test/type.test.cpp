#include <doctest/doctest.h>
#include <sstream>
#include <st/st.hpp>

using sint = st::type<int, struct sint_tag>;

using meter =
    st::type<unsigned long long, struct meter_tag,
             st::traits::arithmetic::addition,
             st::traits::arithmetic::subtraction, st::traits::relational>;

constexpr meter operator"" _meter(unsigned long long value) {
  return meter(value);
}

using width = st::type<meter, struct width_tag>;
using height = st::type<meter, struct height_tag>;

class rectangle {
private:
  meter mWidth;
  meter mHeight;

public:
  rectangle(width width, height height)
      : mWidth{width.value()}, mHeight{height.value()} {}
  meter get_width() const { return mWidth; }
  meter get_height() const { return mHeight; }
};

template <typename T>
using coord_x =
    st::type<T, struct height_tag, st::traits::arithmetic::complete>;

template <typename T>
using coord_y = st::type<T, struct width_tag, st::traits::arithmetic::complete>;

using counter = st::type<unsigned long long, struct MeterParameter,
                         st::traits::arithmetic::increment,
                         st::traits::arithmetic::decrement>;

template <typename T> class vec2 {
public:
  using value_type = T;
  using cx = coord_x<value_type>;
  using cy = coord_y<value_type>;

private:
  cx mx;
  cy my;

public:
  constexpr vec2(cx x, cy y) noexcept : mx{x}, my{y} {}
  cx get_x() const { return mx; }
  cy get_y() const { return my; }
};

TEST_SUITE("Functional") {

  TEST_CASE("Basic usage") {
    SUBCASE("Simple") {
      rectangle rect{width{4_meter}, height{25_meter}};
      REQUIRE(rect.get_width().value() == 4);
      REQUIRE(rect.get_height().value() == 25);
    } // SUBCASE("Simple")

    SUBCASE("Template") {
      // vec2f v{0.f, 1.f}; not allowed!
      // vec2f v{coord_x<int>{}, coord_y<int>{}}; not allowed either!
      vec2<float> v{coord_x<float>{23}, coord_y<float>{11}};
      REQUIRE(v.get_x().value() == 23.f);
      REQUIRE(v.get_y().value() == 11.f);
    } // SUBCASE("Template")
  } // TEST_CASE("Basic usage")

  struct non_default_constructible {
    non_default_constructible() = delete;
    int &f;
    const int x;
  };

  struct user_created {
    user_created() noexcept;
  };

  struct may_throw {
    may_throw() {}
  };

  user_created::user_created() noexcept {}

  TEST_CASE("Construction") {
    SUBCASE("Default constructible") {
      static_assert(std::is_default_constructible<sint>::value,
                    "sint is not default constructible");
      using type_non_default_constructible =
          st::type<non_default_constructible, struct no_default_ctor_tag>;
      static_assert(
          !std::is_default_constructible<type_non_default_constructible>::value,
          "non_default_constructible is default constructible");
    } // SUBCASE("Default constructible")

    SUBCASE("Trivially constructible") {
      static_assert(std::is_trivially_constructible<sint>::value,
                    "sint is not trivially constructible");
      using type_user_created = st::type<user_created, struct user_created_tag>;
      static_assert(!std::is_trivially_constructible<user_created>::value,
                    "user_created is trivially constructible");
    } // SUBCASE("Trivially constructible")

    SUBCASE("Nothrow constructible") {
      static_assert(std::is_nothrow_constructible<sint>::value,
                    "sint is not nothrow constructible");
      using type_may_throw = st::type<may_throw, struct may_throw_tag>;
      static_assert(!std::is_nothrow_constructible<type_may_throw>::value,
                    "type_may_throw is nothrow constructible");
    } // SUBCASE("Nothrow constructible")
  } // TEST_CASE("Construction")

  struct throw_on_construction {
    throw_on_construction() { throw 1; }
    throw_on_construction(int) { throw "exception"; }
  };

  TEST_CASE("Noexcept") {
    using ton = st::type<throw_on_construction, struct throw_tag>;
    REQUIRE(noexcept(sint{}));
    REQUIRE(!noexcept(ton{}));
    REQUIRE(noexcept(sint(3)));
    REQUIRE(!noexcept(ton{5}));
  } // TEST_CASE("Noexcept")

} // TEST_SUITE("Functional")

TEST_SUITE("Core traits") {
  TEST_CASE("Printable") {
    using printable =
        st::type<int, struct printable_tag, st::traits::printable>;
    std::ostringstream oss;
    oss << printable(777);
    REQUIRE(oss.str() == "777");
  } // TEST_CASE("Printable")

  TEST_CASE("Relational") {
    SUBCASE("Run-time") {
      REQUIRE(4_meter == 4_meter);
      REQUIRE_FALSE(8_meter == 5_meter);
      REQUIRE(6_meter != 9_meter);
      REQUIRE_FALSE(7_meter != 7_meter);
      REQUIRE(10_meter < 15_meter);
      REQUIRE_FALSE(10_meter < 10_meter);
      REQUIRE(10_meter <= 10_meter);
      REQUIRE_FALSE(10_meter <= 9_meter);
      REQUIRE(25_meter > 20_meter);
      REQUIRE_FALSE(25_meter > 29_meter);
      REQUIRE(25_meter >= 25_meter);
      REQUIRE_FALSE(25_meter >= 29_meter);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      static_assert(4_meter == 4_meter, "Relational is not constexpr");
      static_assert(!(8_meter == 5_meter), "Relational is not constexpr");
      static_assert(6_meter != 9_meter, "Relational is not constexpr");
      static_assert(!(7_meter != 7_meter), "Relational is not constexpr");
      static_assert(10_meter < 15_meter, "Relational is not constexpr");
      static_assert(!(10_meter < 10_meter), "Relational is not constexpr");
      static_assert(10_meter <= 10_meter, "Relational is not constexpr");
      static_assert(!(10_meter <= 9_meter), "Relational is not constexpr");
      static_assert(25_meter > 20_meter, "Relational is not constexpr");
      static_assert(!(25_meter > 29_meter), "Relational is not constexpr");
      static_assert(25_meter >= 25_meter, "Relational is not constexpr");
      static_assert(!(25_meter >= 29_meter), "Relational is not constexpr");
    } // SUBCASE("Compile_time")
  } // TEST_CASE("Relational")

} // TEST_SUITE("Core traits")

TEST_SUITE("Arithmetic traits") {

  TEST_CASE("Pre-increment") {
    using pre_increment = st::type<int, struct pre_increment_tag,
                                   st::traits::arithmetic::pre_increment>;

    SUBCASE("Run-time") {
      pre_increment a{1}, b{0};
      ++a;
      ++b;
      REQUIRE(a.value() == b.value() + 1);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pre_increment a{5};
      static_assert((++pre_increment{4}).value() == a.value(),
                    "pre_increment is not evaluated at compile-time");
    } // SUBCASE("Compile_time")
  } // TEST_CASE("Pre-increment")

  TEST_CASE("Post-increment") {
    using post_increment = st::type<int, struct post_increment_tag,
                                    st::traits::arithmetic::post_increment>;

    SUBCASE("Run-time") {
      post_increment a{1}, b{1};
      a++;
      b++;
      REQUIRE(a.value() == b.value());
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr post_increment a{8};
      static_assert((post_increment { 8 } ++).value() == a.value(),
                    "post_increment is not evaluated at compile-time");
    } // SUBCASE("Compile_time")
  } // TEST_CASE("Post-increment")

  TEST_CASE("Increment") {
    using increment =
        st::type<int, struct increment_tag, st::traits::arithmetic::increment>;

    SUBCASE("Run-time") {
      increment a{4}, b{5};
      a++;
      ++b;
      REQUIRE(++(a).value() == b.value());
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      static_assert((increment { 8 } ++).value() == (++increment{7}).value(),
                    "increment is not evaluated at compile-time");
    } // SUBCASE("Compile_time")
  } // TEST_CASE("Increment")

  TEST_CASE("Pre-decrement") {
    using pre_decrement = st::type<int, struct pre_decrement_tag,
                                   st::traits::arithmetic::pre_decrement>;

    SUBCASE("Run-time") {
      pre_decrement a{7}, b{6};
      --a;
      --b;
      REQUIRE(a.value() == b.value() + 1);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr pre_decrement a{9};
      static_assert((--pre_decrement{10}).value() == a.value(),
                    "pre_decrement is not evaluated at compile-time");
    } // SUBCASE("Compile_time")

  } // TEST_CASE("Pre-decrement")

  TEST_CASE("Post-decrement") {
    using post_decrement = st::type<int, struct post_decrement_tag,
                                    st::traits::arithmetic::post_decrement>;

    SUBCASE("Run-time") {
      post_decrement a{9}, b{8};
      a--;
      b--;
      REQUIRE(a.value() == b.value() + 1);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr post_decrement a{10};
      static_assert((post_decrement { 10 } --).value() == a.value(),
                    "pre_decrement is not evaluated at compile-time");
    }

  } // TEST_CASE("Post-decrement")

  TEST_CASE("Decrement") {
    using decrement =
        st::type<int, struct decrement_tag, st::traits::arithmetic::decrement>;

    SUBCASE("Run-time") {
      decrement a{4}, b{5};
      a--;
      --b;
      REQUIRE(++(a.value()) == b.value());
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      static_assert((decrement { 6 } --).value() == (--decrement{7}).value(),
                    "decrement is not evaluated at compile-time");
    } // SUBCASE("Compile_time")

  } // TEST_CASE("Decrement")

  TEST_CASE("Addition") {
    SUBCASE("Unary") {
      using unary_plus = st::type<int, struct addition_tag,
                                  st::traits::arithmetic::unary_plus>;

      SUBCASE("Run-time") {
        unary_plus a{29}, b{51};
        REQUIRE((+a).value() == 29);
        REQUIRE((+b).value() == 51);
      } // SUBCASE("Run-time")

      SUBCASE("Compile-time") {
        constexpr unary_plus a{11}, b{89};
        static_assert((+a).value() == 11,
                      "unary_plus is not evaluated at compile-time");
        static_assert((+b).value() == 89,
                      "unary_plus is not evaluated at compile-time");
      } // SUBCASE("Compile-time")
    } // SUBCASE("Unary")

    SUBCASE("Binary") {
      using binary_addition = st::type<int, struct addition_tag,
                                       st::traits::arithmetic::binary_addition>;

      SUBCASE("Run-time") {
        binary_addition a{44}, b{20};
        REQUIRE(((a + b).value() == (b + a).value()));
        REQUIRE((b + a).value() == 64);
        b += binary_addition{10};
        REQUIRE(b.value() == 30);
        REQUIRE((b + a).value() == 74);
      } // SUBCASE("Run-time")

      SUBCASE("Compile-time") {
        constexpr binary_addition a{77}, b{33};
        static_assert((a + b).value() == (b + a).value(),
                      "binary_addition is not evaluated at compile-time");
        static_assert((b + a).value() == 110,
                      "binary_addition is not evaluated at compile-time");
        static_assert(binary_addition{3}.operator+=(a + b).value() == 113,
                      "binary_addition is not evaluated at compile-time");
      } // SUBCASE("Compile-time")

    } // SUBCASE("Binary")

    SUBCASE("Complete") {
      using addition =
          st::type<int, struct addition_tag, st::traits::arithmetic::addition>;

      SUBCASE("Run-time") {
        addition a{6}, b{4};
        REQUIRE((+a).value() == 6);
        REQUIRE((+b).value() == 4);
        REQUIRE(((a + b).value() == (b + a).value()));
        REQUIRE((b + a).value() == 10);
      } // SUBCASE("Run-time")

      SUBCASE("Compile-time") {
        constexpr addition a{54}, b{33};
        static_assert((+a).value() == 54,
                      "addition is not evaluated at compile-time");
        static_assert((+b).value() == 33,
                      "addition is not evaluated at compile-time");
        static_assert((a + b).value() == (b + a).value(),
                      "addition is not evaluated at compile-time");
        static_assert((b + a).value() == 87,
                      "addition is not evaluated at compile-time");
        static_assert(addition{3}.operator+=(a + b).value() == 90,
                      "binary_addition is not evaluated at compile-time");
      } // SUBCASE("Compile-time")
    } // SUBCASE("Complete")
  } // SUBCASE("Addition")

  TEST_CASE("Subtraction") {
    SUBCASE("Unary") {
      using unary_minus = st::type<int, struct subtraction_tag,
                                   st::traits::arithmetic::unary_minus>;

      SUBCASE("Run-time") {
        unary_minus a{29}, b{51};
        REQUIRE((-a).value() == -29);
        REQUIRE((-b).value() == -51);
      } // SUBCASE("Run-time")

      SUBCASE("Compile-time") {
        constexpr unary_minus a{11}, b{89};
        static_assert((-a).value() == -11,
                      "unary_minus is not evaluated at compile-time");
        static_assert((-b).value() == -89,
                      "unary_minus is not evaluated at compile-time");
      } // SUBCASE("Compile-time")
    } // TEST_CASE("Unary")

    SUBCASE("Binary") {
      using binary_subtraction =
          st::type<int, struct subtraction_tag,
                   st::traits::arithmetic::binary_subtraction>;

      SUBCASE("Run-time") {
        binary_subtraction a{44}, b{20};
        REQUIRE(((a - b).value() == -(b - a).value()));
        REQUIRE((a - b).value() == 24);
        REQUIRE((b - a).value() == -24);
        b -= binary_subtraction{10};
        REQUIRE(b.value() == 10);
        REQUIRE((b - a).value() == -34);
      } // SUBCASE("Run-time")

      SUBCASE("Compile-time") {
        constexpr binary_subtraction a{77}, b{33};
        static_assert((a - b).value() == -(b - a).value(),
                      "binary_subtraction is not evaluated at compile-time");
        static_assert((a - b).value() == 44,
                      "binary_subtraction is not evaluated at compile-time");
        static_assert((b - a).value() == -44,
                      "binary_subtraction is not evaluated at compile-time");
        static_assert(binary_subtraction{3}.operator-=(a - b).value() == -41,
                      "binary_addition is not evaluated at compile-time");
      } // SUBCASE("Compile-time")
    } // SUBCASE("Binary")

    SUBCASE("Complete") {
      using subtraction = st::type<int, struct subtraction_tag,
                                   st::traits::arithmetic::subtraction>;

      SUBCASE("Run-time") {
        subtraction a{6}, b{4};
        REQUIRE((-a).value() == -6);
        REQUIRE((-b).value() == -4);
        REQUIRE(((a - b).value() == -(b - a).value()));
        REQUIRE((a - b).value() == 2);
        REQUIRE((b - a).value() == -2);
        b -= subtraction{1};
        REQUIRE(b.value() == 3);
        REQUIRE((b - a).value() == -3);
      } // SUBCASE("Run-time")

      SUBCASE("Compile-time") {
        constexpr subtraction a{54}, b{33};
        static_assert((-a).value() == -54,
                      "subtraction is not evaluated at compile-time");
        static_assert((-b).value() == -33,
                      "subtraction is not evaluated at compile-time");
        static_assert((a - b).value() == -(b - a).value(),
                      "subtraction is not evaluated at compile-time");
        static_assert((a - b).value() == 21,
                      "subtraction is not evaluated at compile-time");
        static_assert((b - a).value() == -21,
                      "subtraction is not evaluated at compile-time");
        static_assert(subtraction{3}.operator-=(a - b).value() == -18,
                      "binary_addition is not evaluated at compile-time");
      } // SUBCASE("Compile-time")
    } // SUBCASE("Complete")
  } // TEST_CASE("Subtraction")

  TEST_CASE("Multiplication") {
    using multiplication = st::type<int, struct multiplication_tag,
                                    st::traits::arithmetic::multiplication>;

    SUBCASE("Run-time") {
      multiplication a{12}, b{10};
      REQUIRE((a * b).value() == (b * a).value());
      a *= multiplication{2};
      REQUIRE((a).value() == 24);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr multiplication a{9}, b{15};
      static_assert((a * b).value() == (b * a).value(),
                    "multiplication is not evaluated at compile-time");
      static_assert((b * a).value() == 135,
                    "multiplication is not evaluated at compile-time");
      static_assert(multiplication{7}.operator*=(a).value() == 63,
                    "multiplication is not evaluated at compile-time");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Multiplication")

  TEST_CASE("Divisible") {
    using divisible =
        st::type<int, struct divisible_tag, st::traits::arithmetic::divisible>;

    SUBCASE("Run-time") {
      divisible a{66}, b{11};
      REQUIRE_FALSE((a / b).value() == (b / a).value());
      REQUIRE((a / b).value() == 6);
      a /= divisible{2};
      REQUIRE((a).value() == 33);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr divisible a{48}, b{6};
      static_assert((a / b).value() != (b / a).value(),
                    "divisible is not evaluated at compile-time");
      static_assert((a / b).value() == 8,
                    "divisible is not evaluated at compile-time");
      static_assert(divisible{98}.operator/=(a).value() == 2,
                    "divisible is not evaluated at compile-time");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Divisible")

  TEST_CASE("Modulo") {
    using modulo =
        st::type<int, struct modulo_tag, st::traits::arithmetic::modulo>;

    SUBCASE("Run-time") {
      modulo a{22}, b{9};
      REQUIRE_FALSE((a % b).value() == (b % a).value());
      REQUIRE((a % b).value() == 4);
      a %= modulo{2};
      REQUIRE((a).value() == 0);
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr modulo a{65}, b{5};
      static_assert((a % b).value() != (b % a).value(),
                    "modulo is not evaluated at compile-time");
      static_assert((a % b).value() == 0,
                    "modulo is not evaluated at compile-time");
      static_assert(modulo{-70}.operator%=(a).value() == -5,
                    "modulo is not evaluated at compile-time");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Modulo")

  TEST_CASE("Bitwise not") {
    using bitwise_not = st::type<int, struct bitwise_not_tag,
                                 st::traits::arithmetic::bitwise_not>;

    SUBCASE("Run-time") {
      bitwise_not a{55};
      REQUIRE((~a).value() == (~55));
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr bitwise_not a(13);
      static_assert((~a).value() == (~13), "bitwise_not is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Bitwise not")

  TEST_CASE("Bitwise and") {
    using bitwise_and = st::type<int, struct bitwise_and_tag,
                                 st::traits::arithmetic::bitwise_and>;

    SUBCASE("Run-time") {
      bitwise_and a(2);
      bitwise_and b(64);
      REQUIRE((a & b).value() == (2 & 64));
      a &= b;
      REQUIRE(a.value() == (2 & 64));
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr bitwise_and a(32);
      constexpr bitwise_and b(12);
      static_assert((a & b).value() == (32 & 12),
                    "bitwise_and is not constexpr");
      static_assert((b & a).value() == (32 & 12),
                    "bitwise_and is not constexpr");
      static_assert(bitwise_and{9}.operator&=(a).value() == (9 & 32),
                    "bitwise_and is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Bitwise and")

  TEST_CASE("Bitwise or") {
    using bitwise_or = st::type<int, struct bitwise_or_tag,
                                st::traits::arithmetic::bitwise_or>;

    SUBCASE("Run-time") {
      bitwise_or a{34}, b{11};
      REQUIRE((a | b).value() == (34 | 11));
      REQUIRE((a | b).value() == (11 | 34));
      a |= b;
      REQUIRE(a.value() == (34 | 11));
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr bitwise_or a(99);
      constexpr bitwise_or b(1);
      static_assert((a | b).value() == (99 | 1), "bitwise_or is not constexpr");
      static_assert((b | a).value() == (1 | 99), "bitwise_or is not constexpr");
      static_assert(bitwise_or{7}.operator|=(a).value() == (7 | 99),
                    "bitwise_or is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Bitwise or")

  TEST_CASE("Bitwise xor") {
    using bitwise_xor = st::type<int, struct bitwise_xor_tag,
                                 st::traits::arithmetic::bitwise_xor>;

    SUBCASE("Run-time") {
      bitwise_xor a{56}, b{27};
      REQUIRE((a ^ b).value() == (56 ^ 27));
      REQUIRE((a ^ b).value() == (27 ^ 56));
      a ^= b;
      REQUIRE(a.value() == (27 ^ 56));
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr bitwise_xor a(88);
      constexpr bitwise_xor b(76);
      static_assert((a ^ b).value() == (88 ^ 76),
                    "bitwise_xor is not constexpr");
      static_assert((b ^ a).value() == (76 ^ 88),
                    "bitwise_xor is not constexpr");
      static_assert(bitwise_xor{5}.operator^=(a).value() == (5 ^ 88),
                    "bitwise_xor is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Bitwise xor")

  TEST_CASE("Bitwise left shift") {
    using bitwise_shl = st::type<int, struct bitwise_shl_tag,
                                 st::traits::arithmetic::bitwise_shl>;

    SUBCASE("Run-time") {
      bitwise_shl a{2}, b{3};
      REQUIRE((a << b).value() == (2 << 3));
      REQUIRE((b << a).value() == (3 << 2));
      a <<= b;
      REQUIRE(a.value() == (2 << 3));
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr bitwise_shl a(2);
      constexpr bitwise_shl b(3);
      static_assert((a << b).value() == (2 << 3),
                    "bitwise_shl is not constexpr");
      static_assert((b << a).value() == (3 << 2),
                    "bitwise_shl is not constexpr");
      static_assert(bitwise_shl{2}.operator<<=(a).value() == (2 << 2),
                    "bitwise_shl is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Bitwise left shift")

  TEST_CASE("Bitwise left shift") {
    using bitwise_shr = st::type<int, struct bitwise_shr_tag,
                                 st::traits::arithmetic::bitwise_shr>;

    SUBCASE("Run-time") {
      bitwise_shr a{2}, b{3};
      REQUIRE((a >> b).value() == (2 >> 3));
      REQUIRE((b >> a).value() == (3 >> 2));
      a >>= b;
      REQUIRE(a.value() == (2 >> 3));
    } // SUBCASE("Run-time")

    SUBCASE("Compile-time") {
      constexpr bitwise_shr a(2);
      constexpr bitwise_shr b(3);
      static_assert((a >> b).value() == (2 >> 3),
                    "bitwise_shr is not constexpr");
      static_assert((b >> a).value() == (3 >> 2),
                    "bitwise_shr is not constexpr");
      static_assert(bitwise_shr{2}.operator>>=(a).value() == (2 >> 2),
                    "bitwise_shr is not constexpr");
    } // SUBCASE("Compile-time")
  } // TEST_CASE("Bitwise left shift")

} // TEST_SUITE("Arithmetic traits")
